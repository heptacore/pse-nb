/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import data.UserData;
import java.sql.SQLException;
import model.User;
import model.Person;
import java.util.ArrayList;

/**
 *
 * @author Oliver
 */
public class UsersTest {
    
    public UsersTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void getUsersWithPeopleData() {
        ArrayList<User> users = new ArrayList<>();
        UserData userData = new UserData();
        userData.readData();
        assertTrue("Users were retrieved successfully", userData.getUsers().size() > 0);
        if (userData.getUsers().size() > 0) {
            for (int i = 0; i < userData.getUsers().size(); i++) {
                //  User has a person assigned
                if (userData.getUsers().get(i).getPerson() != null) {
                    System.out.println("Person for this user is: " + userData.getUsers().get(i).getPerson().getFirstName() + "");
                }
            }
        }
    }
    
    @Test
    public void getUserWithPeopleData() throws SQLException {
        User user;
        UserData userData = new UserData();
        user = userData.readData("olimon");
        assertFalse(user == null);
        if (user != null) {
            System.out.println("Usuario: " + user.getNickname() + ", Nombre: " + user.getPerson().getFirstName());
        }
    }
}
