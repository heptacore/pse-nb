/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import data.ExperiencesData;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Experiences;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Oliver
 */
public class ExperiencesTest {
    
    public ExperiencesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void experienceTest() throws SQLException {
        ExperiencesData expData = new ExperiencesData();
        ArrayList<Experiences> exps = expData.getExperiences();
        System.out.println("Tamaño del Arreglo: " + exps.size());
        for (int i = 0; i < exps.size(); i++) {
            System.out.println("Experiencia: " + exps.get(i).getName().getName());
            System.out.println("Fecha: " + exps.get(i).getDate());
            System.out.println("Aprobado: " + exps.get(i).isApproved());
            for (int j = 0; j < exps.get(i).getContacts().size(); j++) {
                System.out.println("Contacto: " + exps.get(i).getContacts().get(j).getFirstName());
                System.out.println("Razón: " + exps.get(i).getContacts().get(j).getContactRole());
            }
            for (int j = 0; j < exps.get(i).getOrganizations().size(); j++) {
                System.out.println("Organización: " + exps.get(i).getOrganizations().get(j).getName());
                System.out.println("Participación: " + exps.get(i).getOrganizations().get(j).getExperienceRole());
                for (int k = 0; k < exps.get(i).getOrganizations().get(j).getJobs().size(); k++) {
                    System.out.println("Puesto: " + exps.get(i).getOrganizations().get(j).getJobs().get(k).getName());
                }
            }
            for (int j = 0; j < exps.get(i).getInterestGroups().size(); j++) {
                System.out.println("Grupo: " + exps.get(i).getInterestGroups().get(j).getName());
                System.out.println("Participación: " + exps.get(i).getInterestGroups().get(j).getExperienceRole());
                for (int k = 0; k < exps.get(i).getInterestGroups().get(j).getGroupRoles().size(); k++) {
                    System.out.println("Rol: " + exps.get(i).getInterestGroups().get(j).getGroupRoles().get(k).getName());
                }
            }
            for (int j = 0; j < exps.get(i).getSport().size(); j++) {
                System.out.println("Deporte: " + exps.get(i).getSport().get(j).getName());
                for (int k = 0; k < exps.get(i).getSport().get(j).getActivities().size(); k++) {
                    System.out.println("Actividad: " + exps.get(i).getSport().get(j).getActivities().get(k).getName());
                }
            }
            for (int j = 0; j < exps.get(i).getCulture().size(); j++) {
                System.out.println("Cultura: " + exps.get(i).getCulture().get(j).getName());
                for (int k = 0; k < exps.get(i).getCulture().get(j).getActivities().size(); k++) {
                    System.out.println("Actividad: " + exps.get(i).getCulture().get(j).getActivities().get(k).getName());
                }
            }
            for (int j = 0; j < exps.get(i).getUniverities().size(); j++) {
                System.out.println("Universidad: " + exps.get(i).getUniverities().get(j).getName());
                System.out.println("Razón: " + exps.get(i).getUniverities().get(j).getExperienceRole());
            }
            System.out.println("===============================");
            System.out.println("");
        }
    }
}
