/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import data.EmployeeData;
import data.PersonData;
import java.util.ArrayList;
import model.Career;
import model.Email;
import model.Job;
import model.Person;
import model.Phone;
import model.Stakeholder;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.sql.SQLException;
import model.Job;
import model.User;

/**
 *
 * @author Oliver
 */
public class CRUD {
    
    public CRUD() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Inserts data on tables from database related to an employee.
     */
    @Test
    public void createEmployee() throws SQLException {
        Stakeholder stakeholder = new Stakeholder();
        EmployeeData database = new EmployeeData();
        PersonData personData = new PersonData();
        
        //  Add personal information
        stakeholder.setFirstName("Armando");
        stakeholder.setLastName("José");
        stakeholder.setSurname("López");
        stakeholder.setLastSurname("López");
        stakeholder.setIdCardNumber("000");
        stakeholder.setGender("M");
        
        RandomString randomEmail = new RandomString(10);
        for (int i = 0; i < 2; i++) {
            Email email = new Email(randomEmail.nextString());
            email.setOwner(stakeholder.getID());
            stakeholder.getEmailAddresses().add(email);
        }
        RandomString randomPhone = new RandomString(8);
        for (int i = 0; i < 2; i++) {
            Phone phone = new Phone (randomPhone.nextString());
            phone.setOwner(stakeholder.getID());
            stakeholder.getPhones().add(new Phone(randomPhone.nextString()));
        }
        
        //  Add institutional information
        Job job = new Job();
        job.setID(1);
        stakeholder.getJobs().add(job);
        
        stakeholder.setCoordination(new Career(1));
        
        //  Add User
        stakeholder.setUser(new User("prueba", "clave"));
        
        //  Record object
        database.createEmployee(stakeholder);
        
        try {
            assertTrue("Prueba exitosa!", personData.existsPerson("idCardNumber", stakeholder.getIdCardNumber()));
        } catch (SQLException e) {
            System.out.println("Error: " + e);
        }
    }
}
