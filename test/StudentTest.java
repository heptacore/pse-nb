/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import data.StudentData;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Entity;
import utilities.Persistency;
import model.Person;
import model.Student;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Oliver
 */
public class StudentTest {
    
    public StudentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void readDataTest1() {
        StudentData students = StudentData.getInstance();
        try {
            students.readData();
            int studentsAmmount = students.getStudents().size();
            if (studentsAmmount > 0) {  //  Student list is not empty.
                assertFalse(students.getStudents().get(0) == null);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    @Test
    public void updateDataTest1() {
        //  FIXME Test fails.
        data.StudentData students = data.StudentData.getInstance();
        int posIndex = 0;
        Student localStudent = new Student();
        Student databaseStudent = new Student();
        try {
            students.readData();
            if (students.getStudents().size() > 0) {    //  There is at least a database result.
                databaseStudent = students.getStudents().get(posIndex);
                localStudent = databaseStudent;
                localStudent.setFirstName("Oliver");
                students.updateData(localStudent, posIndex);
                databaseStudent = students.readData(localStudent.getIdPerson());
                System.out.println("Unit Test Log: (local) " + localStudent.getFirstName() + " (database) " + databaseStudent.getFirstName());
                assertEquals(databaseStudent.getFirstName(), localStudent.getFirstName());
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    @Test
    public void deleteDataTest1() throws SQLException {
        int ID = 19;
        Student deletingStudent = new Student();
        StudentData data = StudentData.getInstance();
        deletingStudent = (Student) data.readData(ID);
        data.deleteData(deletingStudent);
        deletingStudent = (Student) data.readData(ID);
        assertEquals(deletingStudent.getIdPerson(), 0);
    }
    
    @Test
    public void createDataTest1() {
        Student newStudent = new Student();
        ArrayList<Student> students = new ArrayList<Student>();
        newStudent.setFirstName("Ashley");
        newStudent.setLastName("");
        newStudent.setSurname("Zeledón");
        newStudent.setLastSurname("");
        newStudent.setGender("F");
        students.add(newStudent);
        StudentData data = StudentData.getInstance();
        data.createData(students);
    }
    
    @Test
    public void paginatedTest() {
        StudentData studentData = new StudentData();
        ArrayList<Student> students = new ArrayList<>();
        
        try {
            students = studentData.getStudentsPage(1, 2);
            for (int i = 0; i < students.size(); i++) {
                System.out.println((i + 1) + ", " + students.get(i).getFirstName());
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e);
        }
    }
}
