/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataServlets;

import data.ExperienceData;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Experience;

/**
 *
 * @author Oliver
 */
@WebServlet(name = "getExperiences", urlPatterns = {"/getExperiences"})
public class getExperiences extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet getExperiences</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet getExperiences at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ExperienceData experiencesData = new ExperienceData();
        PrintWriter out = response.getWriter();
        
        try {
        ExperienceData experienceData = new ExperienceData();
        ArrayList<Experience> experiences = experienceData.getAllExperiences();
        
        out.println("<table class=\"selectableTable table table-striped table-bordered dataex-html5-selectors\">");
        out.println("   <thead>");
        out.println("       <tr>");
        out.println("           <th>#</th>");
        out.println("           <th>Experiencia</th>");
        out.println("       </tr>");
        out.println("   </thead>");
        out.println("   <tbody>");
        for (int i = 0; i < experiences.size(); i++) {
            out.println("       <tr onclick=\"loadForm(this.getAttribute('data'))\" class=\"table-row\" data=\"" + experiences.get(i).getID() + "\" data-dismiss=\"modal\">");
            out.println("           <td>" + (i + 1) + "</td>");
            out.println("           <td>" + experiences.get(i).getName() + "</td>");
            out.println("       </tr>");
        }
        out.println("   </tbody>");
        out.println("   <tfoot>");
        out.println("       <tr>");
        out.println("           <th>#</th>");
        out.println("           <th>Experiencia</th>");
        out.println("       </tr>");
        out.println("   </tfoot>");
        out.println("</table>");
        } catch (Exception e) {
            out.println("<p>Ha ocurrido un error interno en el servidor.</p>");
            System.out.println("[ERROR][/getExperiences]: " + e);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
