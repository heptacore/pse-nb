/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataServlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.StudentData;
import java.util.ArrayList;
import model.Student;
import data.StudentData;
import data.PersonData;
import data.UserData;
import data.EmailData;
import data.PhoneData;
import data.CareerData;
import java.sql.SQLException;
import model.Career;
import model.Email;
import model.Phone;
import model.User;

/**
 *
 * @author Oliver
 */
public class Add_Student extends HttpServlet {
    //  Attributes
    private final StudentData studentData = new StudentData();
    private final PersonData personData = new PersonData();
    private final UserData userData = new UserData();
    private final EmailData emailData = new EmailData();
    private final PhoneData phoneData = new PhoneData();
    private final CareerData careerData = new CareerData();
    private final Student newStudent = new Student();
    private String firstName;
    private String lastName = null;
    private String surname;
    private String lastSurname = null;
    private String gender;
    private String idCardNumber = null;
    private String idStudent = null;
    private String idExStudent = null;
    private String nickname = null;
    private String password = null;
    private ArrayList<String> emails = new ArrayList<>();
    private ArrayList<String> phones = new ArrayList<>();
    private ArrayList<String> careers = new ArrayList<>();
    private ArrayList<String> cohorts = new ArrayList<>();
    private PrintWriter out;

    //  Methods
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        out = response.getWriter();
        
        //  [Process][1]: Get data from http request.
        serializeForm(request);
        
        //  [Process][2]: Create Persistent Object with Gathered Information.
        createPersistentObject();
        
        //  [Process][3]:   Validate Object.
        try {
            if (!existsObject()) {
                //  [Process][4]:   Insert object into database.
                recordObject();
            }
        } catch (SQLException ex) {
            System.out.println("[HeptaCore]: " + ex);
            try {
                deleteObject();
                System.out.println("[HeptaCore]: Changes to database were undone for security purposes.");
            } catch (SQLException e) {
                System.out.println("[HeptaCore]: An error ocurred while undoing changes to database, " + e);
            }
        }
    }
    
    private void serializeForm(HttpServletRequest request) {
        int counter = 0;
        //  [Process][1.1]: Get Personal Information
        firstName = request.getParameter("firstName");
        if (request.getParameter("lastName").isEmpty() == false) {
            lastName = request.getParameter("lastName");
        }
        surname = request.getParameter("surname");
        if (request.getParameter("lastSurname").isEmpty() == false) {
            lastSurname = request.getParameter("lastSurname");
        }
        gender = request.getParameter("gender").substring(0, 1);
        if (request.getParameter("idCardNumber").isEmpty() == false) {
            idCardNumber = request.getParameter("idCardNumber");
        }
        //  if there were emails given
        while (null != request.getParameter("repeater-group[" + counter + "][email]")) {
            //  String is not empty
            if (request.getParameter("repeater-group[" + counter + "][email]").isEmpty() == false) {
                emails.add(request.getParameter("repeater-group[" + counter + "][email]"));
            }
            counter++;
        }
        counter = 0;
        while (null != request.getParameter("repeater-group[" + counter + "][phone]")) {
            if (request.getParameter("repeater-group[" + counter + "][phone]").length() >= 8) {
                phones.add(request.getParameter("repeater-group[" + counter + "][phone]"));
            }
            counter++;
        }
        counter = 0;
        
        //  [Process][1.2]: Get Academic Information.
        idStudent = request.getParameter("idStudent");
        //  ID Ex-Student was given
        if (null != request.getParameter("idExStudent")) {
            idExStudent = request.getParameter("idExStudent");
        }
        //  If there are given careers
        while (null != request.getParameter("repeater-group[" + counter + "][career]")) {
            //  Value is not null or empty
            if (request.getParameter("repeater-group[" + counter + "][career]").isEmpty() == false) {
                careers.add(request.getParameter("repeater-group[" + counter + "][career]"));
            }
            counter++;
        }
        counter = 0;
        while (null != request.getParameter("repeater-group[" + counter + "][cohort]")) {
            //  Value is not null or empty
            if (request.getParameter("repeater-group[" + counter + "][cohort]").isEmpty() == false) {
                cohorts.add(request.getParameter("repeater-group[" + counter + "][cohort]") + "-01");
            }
            counter++;
        }
        
        
        //  [Process][1.3]: Get Account Information
        nickname = request.getParameter("nickname");
        password = request.getParameter("password");
    }
    
    private boolean existsObject() throws SQLException {
        //  Person does not exist on database (if ID card was given)
        if (!personData.existsPerson("idCardNumber", newStudent.getIdCardNumber())) {
            //  Student does not exist on database
            if (!studentData.existStudent(newStudent)) {
                //  User does not exist on database
                if (!userData.existsUser("nickname", newStudent.getUser().getNickname())) {
                    return false;
                } else {
                    out.println("-3");
                }
            } else {
                out.println("-2");
            }
        } else {
            out.println("-1");
        }
        return true;
    }
    
    private void deleteObject() throws SQLException {
        //personData.deleteData(newStudent);
    }
    
    private void recordObject() throws SQLException {
        //  [Process][4.1]: Insert row in Person Table.
        personData.createData(newStudent);
        newStudent.setID(personData.getID("idCardNumber", newStudent.getIdCardNumber()));
        //  [Process][4.2]: Insert row in Student Table.
        studentData.createData(newStudent);
        newStudent.setIdStudent(studentData.getID("person", Integer.toString(newStudent.getID())));
        //  [Process][4.3]: Insert row in User Table
        userData.createData(newStudent.getUser());
        //  [Process][4.4]: Insert row in other tables
        //  [Process][4.4.1]:   Insert row(s) in Email Table
        for (int i = 0; i < emails.size(); i++) {
            emailData.createData(new Email(emails.get(i), newStudent.getID()));
        }
        //  [Process][4.4.2]:   Insert row(s) in Phone Table
        for (int i = 0; i < phones.size(); i++) {
            phoneData.createData(new Phone(phones.get(i), newStudent.getID()));
        }
        //  [Process][4.4.3]:   Insert row(s) in Student_Career Table
        for (int i = 0; i < careers.size(); i++) {
            careerData.suscribeStudent(newStudent, new Career(Integer.parseInt(careers.get(i)), cohorts.get(i)));
        }
        phones.clear();
        emails.clear();
        careers.clear();
        cohorts.clear();
    }
    
    private void createPersistentObject() {
        newStudent.setFirstName(firstName);
        //  LastName Field is not empty
        if (null != lastName) {
            newStudent.setLastName(lastName);
        }
        newStudent.setSurname(surname);
        //  LastSurname Field is not empty
        if (null != lastSurname) {
            newStudent.setLastSurname(lastSurname);
        }
        newStudent.setGender(gender);
        //  idCardNumber Field is not empty
        if (null != idCardNumber) {
            newStudent.setIdCardNumber(idCardNumber);
        }
        //  emails list is not empty
        if (!emails.isEmpty()) {
            if (emails.get(0).isEmpty() == false) {
                for (int i = 0; i < emails.size(); i++){
                    newStudent.getEmailAddresses().add(new Email(emails.get(i)));
                }
            }
        }
        //  phones list is not empty
        if (!phones.isEmpty()) {
            if (phones.get(0).isEmpty() == false) {
                for (int i = 0; i < phones.size(); i++){
                    newStudent.getPhones().add(new Phone(phones.get(i)));
                }
            }
        }
        newStudent.setStudentIdNumber(idStudent);
        //  idExStudent Field is not null/empty
        if (null != idExStudent) {
            if (idExStudent.isEmpty() == false) {
                newStudent.setExStudentIdNumber(idExStudent);
            }
        }
        //  careers and careers list is not empty and have the same size
        if ((careers.isEmpty() == false && cohorts.isEmpty() == false) && (careers.size() == cohorts.size())) {
            //  if default list value (0) is not empty
            if (!careers.get(0).isEmpty() && !careers.get(0).isEmpty()) {
                for (int i = 0; i < careers.size(); i++){
                    newStudent.getCareers().add(new Career(Integer.parseInt(careers.get(i)), cohorts.get(i)));
                }
            }
        }
        newStudent.setUser(new User(nickname, password));
        newStudent.getUser().setPerson(newStudent);
    }
}
