/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataServlets;

import data.ExperienceData;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Experience;

/**
 *
 * @author Oliver
 */
@WebServlet(name = "getExperienceForm", urlPatterns = {"/getExperienceForm"})
public class getExperienceForm extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("            <!DOCTYPE html>");
            out.println("            <html>");
            out.println("            <head>");
            out.println("            <title>Servlet getExperienceForm</title>");            
            out.println("            </head>");
            out.println("            <body>");
            out.println("            <h1>Servlet getExperienceForm at " + request.getContextPath() + "</h1>");
            out.println("            </body>");
            out.println("            </html>");
        }
    }

    // <editor-fold defaultstate=\"collapsed" desc=\"HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
        //PrintWriter out = response.getWriter();
        PrintWriter out = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), StandardCharsets.UTF_8), true);
        int experienceID = Integer.parseInt(request.getParameter("experience"));
        ExperienceData experienceData = new ExperienceData();
        Experience experience = experienceData.getExperience(experienceID);
        int counter = 1;
        
        //Experience Name
        out.println("<h4 title=\"" + experience.getDescription() + "\" style=\"text-align:center;\">" + experience.getName() + "</h4>");
        
        //  Experience Info
        out.println("<h4 class=\"form-section\"><i class=\"ft-user\"></i>Información General</h4>");
        //  Experience Title
        out.println("<div class=\"row\">");
        out.println("   <div class=\"col-md-12\">");
        out.println("       <div class=\"form-group\">");
        out.println("           <label for=\"input" + (counter) + "\">Título</label>");
        out.println("           <input type=\"text\" id=\"input" + (counter++) + "\" class=\"custom-form-control\" placeholder=\"Ej: Servicio Social en Pajarito Azul\" name=\"name\">");
        out.println("       </div>");
        out.println("   </div>");
        out.println("</div>");
        //  Experience Description
        out.println("<div class=\"row\">");
        out.println("   <div class=\"col-md-12\">");
        out.println("       <div class=\"form-group\">");
        out.println("           <label for=\"input" + (counter) + "\">Descripción</label>");
        out.println("           <input type=\"text\" id=\"input" + (counter++) + "\" class=\"custom-form-control\" placeholder=\"Ej: Servicio social tecnológico como parte de APS (...)\" name=\"description\">");
        out.println("       </div>");
        out.println("   </div>");
        out.println("</div>");
        //  Proyect Title
        if (experience.getIsProyect() == 1) {
            out.println("<h4 class=\"form-section\"><i class=\"ft-user\"></i>Información de Proyecto</h4>");
            out.println("<div class=\"row\">");
            out.println("   <div class=\"col-md-12\">");
            out.println("       <div class=\"form-group\">");
            out.println("           <label for=\"input" + (counter) + "\">Descripción</label>");
            out.println("           <input type=\"text\" id=\"input" + (counter++) + "\" class=\"custom-form-control\" placeholder=\"Ej: Servicio social tecnológico como parte de APS (...)\" name=\"proyectTitle\">");
            out.println("       </div>");
            out.println("   </div>");
            out.println("</div>");
        }
        //  Interest Groups
        if (experience.getHasInterestGroups() == 1) {
            out.println("<h4 class=\"form-section\"><i class=\"ft-user\"></i>Grupos de Interes</h4>");
            out.println("<div class=\"row\">");
            out.println("    <div class=\"col-md-6\">");
            out.println("       <label for=\"projectinput3\">Correo Electrónico</label>");
            out.println("       <div class=\"form-group\" data-ac-repeater=\"is\" data-ac-repeater=\"Add Who is\" data-ac-repeater-text=\"Agregar Cohorte\" buttonStyle=\"style='margin-top: 10px;' type='button' class='btn btn-primary'\">");
            out.println("           <label for=\"projectinput1\">Primer Nombre</label>");
            out.println("           <select id=\"projectinput1\" name=\"gender\" class=\"custom-form-control\">");
            out.println("               <option value=\"masculino\">Masculino</option>");
            out.println("               <option value=\"femenino\">Femenino</option>");
            out.println("           </select>");
            out.println("       </div>");
            out.println("    </div>");
            out.println("    <div class=\"col-md-6\">");
            out.println("        <label for=\"projectinput3\">Teléfono</label>");
            out.println("        <div class=\"form-group custom-col-12 mb-2 contact-repeater\">");

            out.println("            <div data-repeater-list=\"repeater-group\">");
            out.println("                <div class=\"input-group mb-1\" data-repeater-item=\"\">");
            out.println("                    <input name=\"repeater-group[0][phone]\" type=\"tel\" placeholder=\"Ingrese su Teléfono\" class=\"custom-form-control\" id=\"example-tel-input\">");
            out.println("                    <span class=\"input-group-append\" id=\"button-addon2\">");
            out.println("                        <button class=\"btn btn-danger\" type=\"button\" data-repeater-delete=\"\">");
            out.println("                            <i class=\"ft-x\"></i>");
            out.println("                        </button>");
            out.println("                    </span>");
            out.println("                </div>");
            out.println("            </div>");

            out.println("            <button type=\"button\" data-repeater-create=\"\" class=\"btn btn-primary\">");
            out.println("                <i class=\"ft-plus\"></i> Agregar Nuevo Teléfono");
            out.println("            </button>");
            out.println("        </div>");
            out.println("    </div>");
            out.println("</div>");
        }
        //  Universities
        if (experience.getHasUniversities() == 1) {
            out.println("<h4 class=\"form-section\"><i class=\"ft-user\"></i>Universidades</h4>");
        }
        //  Culture
        if (experience.getHasCultures() == 1) {
            out.println("<h4 class=\"form-section\"><i class=\"ft-user\"></i>Bellas Artes y Cultura</h4>");
        }
        //  Sports
        if (experience.getHasSports() == 1) {
            out.println("<h4 class=\"form-section\"><i class=\"ft-user\"></i>Deportes</h4>");
        }
        //  Organizations
        if (experience.getHasOrganizations() == 1) {
            out.println("<h4 class=\"form-section\"><i class=\"ft-user\"></i>Organizaciones</h4>");
        }
        } catch (Exception e) {
            System.out.println("[ERROR][getExperienceForm]: " + e);
            response.sendRedirect("add-experience.jsp");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
