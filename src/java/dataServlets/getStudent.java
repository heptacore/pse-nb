/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataServlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Student;
import data.StudentData;
import java.sql.SQLException;

/**
 *
 * @author Oliver
 */
@WebServlet(name = "getStudent", urlPatterns = {"/getStudent"})
public class getStudent extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        StudentData studentData = new StudentData();
        Student student = null;
        PrintWriter out = response.getWriter();

        //  passed data is valid
        try {
            if (null != request.getParameter("student")) {
                String studentID = request.getParameter("student");
                System.out.println("Request is: " + studentID);
                try {
                    student = studentData.getStudent(studentID);
                    out.println("<div class=\"modal-dialog\">");
                    out.println("   <div class=\"modal-content\">");
                    out.println("       <div class=\"modal-header\">");
                    out.println("           <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>");
                    out.println("           <h4 class=\"modal-title\">Modal Header</h4>");
                    out.println("       </div>");
                    out.println("       <div class=\"modal-body\">");
                    out.println("           <p>" + student.getName() + "</p>");
                    out.println("       </div>");
                    out.println("       <div class=\"modal-footer\">");
                    out.println("           <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>");
                    out.println("       </div>");
                    out.println("   </div>");
                    out.println("</div>");
                } catch (SQLException e) {
                    System.out.println("Error: " + e);
                    out.println("502");
                }
            } else {
                out.println("501");
            }
        } catch (NullPointerException e) {
            System.out.println("Error: " +e);
            out.println("Petición Errónea.");
        }
    }

}
