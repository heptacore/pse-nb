package utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public abstract class Connection {

    public Connection() {
    }

//	Atributos
    public static String bdHost = "localhost";
    public static String bdName = "PSE";
    public static String port = "1433";
    public static String UCABDUser = "aps";
    public static String UCABDPassword = "$qlS3rv3rAPS*";
    public static String localUser = "sa";
    public static String localPassword = "A51cfB10dc";
    public static String connectionUrl = "jdbc:sqlserver://" + bdHost + ":" + port + ";"
            + "databaseName=" + bdName + ";user=" + UCABDUser + ";password=" + UCABDPassword;
    com.microsoft.sqlserver.jdbc.SQLServerConnectionPoolDataSource dataSource;
    public static java.sql.Connection con = null;
    public static java.sql.Statement st = null;
    public static java.sql.PreparedStatement pst = null;
    public static java.sql.ResultSet rs = null;

    /**
     * Gets an instance for database connection.. if there are no instances, it
     * creates it; otherwise, it gets the existing one.
     *
     * @return
     * @throws ClassNotFoundException
     */
    public static java.sql.Connection getConnection() throws ClassNotFoundException {
        if (con == null) {
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                con = java.sql.DriverManager.getConnection(connectionUrl);
                if (con != null) {
                    System.out.println("PSE (DataBase): Connection to SQLServer was stablished successfully!");
                }
            } catch (SQLException exception) {
                System.out.println("PSE (DataBase) [ERROR]: " + exception);
            }
        }
        return con;
    }

    /**
     * Gets one or more rows from database in a specified SQL query.
     *
     * @param query consulta SQL a realizarse en la base de datos.
     * @return
     */
    public static java.sql.ResultSet doQuery(String query) {
        //  There are no instances of database.
        if (con == null) {
            try {
                con = getConnection();
                pst = con.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                rs = pst.executeQuery();
            } catch (ClassNotFoundException | SQLException e) {
                System.out.println("PSE (database) [ERROR]: " + e);
            }
        } else {
            try {
                pst = con.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                rs = pst.executeQuery();
            } catch (SQLException e) {
                System.out.println("PSE (database) [ERROR]: " + e + ", attempted query was " + query);
            }
        }
        return rs;
    }
    
    public Page getStudentsPage(int page, int pageSize, String query) throws SQLException, ClassNotFoundException {
        int first = Page.getStartItemByPage(page, pageSize);
        java.sql.Connection c = null;
        java.sql.PreparedStatement statement = null;
        ResultSet resultSet = null;
        Collection results = new ArrayList();
        try {
            c = getConnection();
            statement = c.prepareStatement(
                query,ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY);

            resultSet = statement.executeQuery();
            int countAdds = 0;
            boolean more = resultSet.absolute(first);
            while (more) {
                // Sustituir por nuestra creacion de Beans
                // MyBean m = new MyBean();
                // m.setDato(rs.getInt(1));
                // results.add(m);
                if (++ countAdds == pageSize) {
                    break;
                }
                more = resultSet.next();
            }
            resultSet.last();
            int totalElements = resultSet.getRow(); // El ultimo elemento

            return new Page(results,  page, pageSize, totalElements);

        } finally {
            try { if (resultSet != null) resultSet.close(); } catch (SQLException e) {}
            //try { if (statement != null) statement.close(); } catch (SQLException e) {}
            try { if (c != null) c.close(); } catch (SQLException e) {}
        }
    }

    /**
     * Performs a DML Clause on DataBase.
     *
     * @param query SQL query to be performed. It has to be a DML Query
     * Structure.
     * @return
     */
    public static int doUpdate(String query) {
        int result = 1;
        //  Database connection has not been instanciated.
        if (con == null) {
            try {
                con = getConnection();
                //  There is no statement for this database connection.
                if (st == null) {
                    st = con.createStatement();
                }
                result = st.executeUpdate(query);
            } catch (ClassNotFoundException | SQLException e) {
                System.out.println("PSE (database) [ERROR]: " + e);
            }
        } else {
            try {
                //  There is no statement for this database connection.
                if (st == null) {
                    st = con.createStatement();
                }
                result = st.executeUpdate(query);
            } catch (SQLException e) {
                System.out.println("PSE (database) [ERROR]: " + e);
            }
        }
        return result;
    }

    public static void endQuery() {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                System.out.println("PSE (database) [ERROR]: " + e);
            }
        }
    }
}
