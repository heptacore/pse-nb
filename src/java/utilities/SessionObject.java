/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 *
 * @author Oliver
 */
public class SessionObject {
    private HttpSession httpSession;
    private Session webSocketSession;

    public SessionObject(HttpSession httpSession, Session webSocketSession) {
        this.httpSession = httpSession;
        this.webSocketSession = webSocketSession;
    }

    public HttpSession getHttpSession() {
        return httpSession;
    }

    public void setHttpSession(HttpSession httpSession) {
        this.httpSession = httpSession;
    }

    public Session getWebSocketSession() {
        return webSocketSession;
    }

    public void setWebSocketSession(Session webSocketSession) {
        this.webSocketSession = webSocketSession;
    }
}
