package utilities;

import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.ResultSet;
import model.Entity;
import utilities.Connection;

public interface Persistency {
	public void createData(ArrayList<?> objects);
	
	public void readData() throws SQLException;

	public void updateData(Entity object, int localArrayIndex);
	
	public void deleteData(Entity object);
}
