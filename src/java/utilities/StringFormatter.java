/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author Oliver
 */
public class StringFormatter {
    public static StringFormatter instance;
    
    private StringFormatter() {
    }
    
    public static StringFormatter getInstance() {
        if (instance == null) {
            instance = new StringFormatter();
        }
        return instance;
    }
    
    public String validateString(String value, String mask) {
        String returnValue = value;
        if (value == null | value == "" | value == "null") {
            returnValue = mask;
        }
        return returnValue;
    }
}
