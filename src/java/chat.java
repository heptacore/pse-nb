/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.MessageData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import model.Message;
import model.User;

/**
 *
 * @author Oliver
 */
@WebServlet(urlPatterns = {"/chat"})
public class chat extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        User myUser = (User) session.getAttribute("user");
        String user2 = request.getParameter("contact");
        MessageData messageData = new MessageData();
        PrintWriter out = response.getWriter();
        
        try {
            ArrayList<Message> messages = messageData.getMessages(myUser.getNickname(), user2, 0);
            System.out.println(myUser.getNickname() + ", " + user2 + ", length: " + messages.size());
            for (int i = 0; i < messages.size(); i++) {
                //  First interaction
                if ((i == 0) || (i > 0 && messages.get(i - 1).getSenderUser().equals(messages.get(i).getSenderUser())) == false) {
                    //  I am the sender of the message
                    if (messages.get(i).getSenderUser().getNickname().equals(myUser.getNickname())) {
                        out.println("<div class=\"chat\">");
                        out.println("   <div class=\"chat-avatar\">");
                        out.println("       <a class=\"avatar\" data-toggle=\"tooltip\" href=\"#\" data-placement=\"right\" title=\"\" data-original-title=\"\">");
                        out.println("           <img src=\"app-assets/images/portrait/small/avatar-s-1.png\" alt=\"avatar\" />");
                        out.println("       </a>");
                        out.println("   </div>");
                        out.println("   <div class=\"chat-body\">");
                        out.println("       <div class=\"chat-content\">");
                        out.println("           <p>" + messages.get(i).getValue() + "</p>");
                        out.println("       </div>");
                        out.println("   </div>");
                    } else {
                        out.println("<div class=\"chat chat-left\">");
                        out.println("   <div class=\"chat-avatar\">");
                        out.println("       <a class=\"avatar\" data-toggle=\"tooltip\" href=\"#\" data-placement=\"left\" title=\"\" data-original-title=\"\">");
                        out.println("           <img src=\"app-assets/images/portrait/small/avatar-s-7.png\" alt=\"avatar\" />");
                        out.println("       </a>");
                        out.println("   </div>");
                        out.println("   <div class=\"chat-body\">");
                        out.println("       <div class=\"chat-content\">");
                        out.println("           <p>" + messages.get(i).getValue() + "</p>");
                        out.println("       </div>");
                        out.println("   </div>");
                    }
                } else {
                    //  I am the sender of the message
                    if (messages.get(i).getSenderUser().getNickname().equals(myUser.getNickname())) {
                        out.println("       <div class=\"chat-content\">");
                        out.println("           <p>" + messages.get(i).getValue() + "</p>");
                        out.println("       </div>");
                        out.println("   </div>");
                        out.println("</div>");
                    } else {
                        out.println("       <div class=\"chat-content\">");
                        out.println("           <p>" + messages.get(i).getValue() + "</p>");
                        out.println("       </div>");
                        out.println("   </div>");
                        out.println("</div>");
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("Chat.java [Servlet] [ERROR]: " + e);
            out.println("<span>An error has occured. Contact this system's administrator.</span>");
        }
    }
}
