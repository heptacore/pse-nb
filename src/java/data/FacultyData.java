package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Faculty;
import model.Career;
import utilities.Connection;

/**
 *
 * @author Oliver
 */
public class FacultyData {
    private ResultSet resultSet = null;
    private CareerData careerData = new CareerData();
    
    public FacultyData(){}
    
    public ArrayList<Faculty> getFaculties() throws SQLException {
        ArrayList<Faculty> faculties = new ArrayList<>();
        resultSet = Connection.doQuery("select * from Faculty;");
        while (resultSet.next()){
            Faculty faculty = new Faculty();
            faculty.setID(resultSet.getInt("ID"));
            faculty.setName(resultSet.getString("name"));
            faculty.setCareers(careerData.getFacultyCareers(resultSet.getInt("ID")));
            faculties.add(faculty);
        }
        return faculties;
    }
    
    public Faculty getFaculty(int idFaculty) {
        return null;
    }
}
