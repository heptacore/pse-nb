/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import model.Country;
import utilities.Connection;
import java.util.ArrayList;

/**
 *
 * @author Oliver
 */
public class CountryData {
    private ResultSet resultSet = null;
    
    public CountryData() {}
    
    public String getCountryName(String idCountry) throws SQLException {
        String countryName = null;
        resultSet = Connection.doQuery("SELECT * FROM Country WHERE name = '" + idCountry + "';");
        if (resultSet.first()) {
            countryName = resultSet.getString("name");
        }
        return countryName;
    }
}
