/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import utilities.Connection;
import model.Culture;
import java.util.ArrayList;


/**
 *
 * @author Oliver
 */
public class CultureData {
    private ResultSet resultSet = null;
    private final CETypeData ceTypeData = new CETypeData();
    
    public CultureData(){}
    
    public ArrayList<Culture> getExperienceCultures(int idExperience) throws SQLException {
        ArrayList<Culture> cultures = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT "
                + "Culture.ID as 'cultureID', Culture.approved as 'cultureApproved', Culture.name as 'cultureName' "
                + "FROM Culture "
                + "INNER JOIN culturalActivity on Culture.ID = culturalActivity.culture "
                + "WHERE experience = '" + idExperience + "';");
        while (resultSet.next()) {
            Culture culture = new Culture(
                resultSet.getInt("cultureID"),
                resultSet.getString("cultureName")
            );
            culture.setApproved(resultSet.getInt("cultureApproved"));
            culture.setActivities(ceTypeData.getExperienceCETypes(idExperience));
            cultures.add(culture);
        }
        return cultures;
    }
}
