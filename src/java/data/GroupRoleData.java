/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.GroupRole;
import utilities.Connection;

/**
 *
 * @author Oliver
 */
public class GroupRoleData {
    private ResultSet resultSet = null;
    
    public GroupRoleData(){}
    
    public GroupRole getExperienceGroupRole(int idExperience) throws SQLException {
        GroupRole groupRole = null;
        resultSet = Connection.doQuery("SELECT * FROM Group_Role "
                + "INNER JOIN Exp_IG on Group_Role.ID = Exp_IG.role "
                + "WHERE Exp_IG.experience = '" + idExperience + "';");
        if (resultSet.first()) {
            groupRole = new GroupRole();
            groupRole.setID(resultSet.getInt("ID"));
            groupRole.setName(resultSet.getString("name"));
            groupRole.setApproved(resultSet.getInt("approved"));
        }
        return groupRole;
    }
}
