/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.*;
import utilities.Connection;

/**
 *
 * @author Oliver
 */
public class ExperienceData {
    private ResultSet resultSet = null;
    
    public ExperienceData(){};
    
    /**
     * Constructs a {@code Experience} object with database data
     * from a specific key given row.
     * @param ID ID Key of the row for Experience table.
     * @return Experience object if found, null if no row was found.
     * @throws SQLException 
     */
    public Experience getExperience(int ID) throws SQLException {
        Experience experience = null;
        resultSet = Connection.doQuery("SELECT * FROM Experience WHERE ID = '" + ID + "';");
        if (resultSet.first()){
            experience = new Experience();
            experience.setID(resultSet.getInt("ID"));
            experience.setName(resultSet.getString("name"));
            experience.setDescription(resultSet.getString("description"));
            experience.setHasInterestGroups(resultSet.getInt("hasInterestGroups"));
            experience.setHasUniversities(resultSet.getInt("hasUniversities"));
            experience.setHasCultures(resultSet.getInt("hasCulture"));
            experience.setHasSports(resultSet.getInt("hasSport"));
            experience.setHasOrganizations(resultSet.getInt("hasOrganization"));
            experience.setIsProyect(resultSet.getInt("isProyect"));
        }
        return experience;
    }
    
    /**
     * Return an {@code ArrayList} object that contains
     * {@code Experience} objects constructed with database data.
     * Collection is equal to the number of rows on Experience Table
     * on database.
     * @return Collection of Experience, Collection is empty if nothing was found.
     * @throws SQLException 
     */
    public ArrayList<Experience> getAllExperiences() throws SQLException {
        ArrayList<Experience> experiences = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT * FROM Experience;");
        while(resultSet.next()) {
            Experience experience = new Experience();
            experience.setID(resultSet.getInt("ID"));
            experience.setName(resultSet.getString("name"));
            experience.setDescription(resultSet.getString("description"));
            experience.setHasInterestGroups(resultSet.getInt("hasInterestGroups"));
            experience.setHasUniversities(resultSet.getInt("hasUniversities"));
            experience.setHasCultures(resultSet.getInt("hasCulture"));
            experience.setHasSports(resultSet.getInt("hasSport"));
            experience.setHasOrganizations(resultSet.getInt("hasOrganization"));
            experience.setIsProyect(resultSet.getInt("isProyect"));
            experiences.add(experience);
        }
        return experiences;
    }
}
