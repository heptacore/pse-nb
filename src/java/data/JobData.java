/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import model.Job;
import java.util.ArrayList;
import utilities.Connection;

/**
 *
 * @author Christian Garcia
 */
public class JobData {
    private ResultSet resultSet;
    private ArrayList<Job> jobs = new ArrayList<>();
    
    public JobData(){}
    
    /**
     * Gets a collection of {@code Job} objects related 
     * to an employee contructed with data fetched from database.
     * @param stakeholderID Key for a row in Stakeholder table on database.
     * @return A collection of the Jobs of an employee. Collection will be empty if nothing could be fetched.
     * @throws SQLException 
     */
    public ArrayList<Job> readData(int stakeholderID) throws SQLException {
        //  jobs list is full
        ArrayList<Job> jobs = new ArrayList<>();
        
        resultSet = Connection.doQuery("select * from Job inner join Employee on Job.ID = Employee.job where Employee.stakeholder = '" + stakeholderID + "';");
        while(resultSet.next()) {
            Job newJob = new Job();
            newJob.setID(resultSet.getInt("ID"));
            newJob.setName(resultSet.getString("name"));
            newJob.setDescription(resultSet.getString("description"));
            jobs.add(newJob);
        }
        return jobs;
    }
    
    /**
     * Creates a {@code Job} object with data get from database.
     * @param JobID Key of the row where data to construct the object to be returned.
     * @return Created object if found on database. NULL if not found.
     * @throws SQLException 
     */
    public Job fetchJob(int JobID) throws SQLException {
        Job job = null;
        resultSet = Connection.doQuery("SELECT * FROM Job WHERE ID = '" + JobID + "';");
        
        if (resultSet.first()) {
            job = new Job();
            job.setID(resultSet.getInt("ID"));
            job.setName(resultSet.getString("name"));
            job.setDescription(resultSet.getString("description"));
        }
        
        return job;
    }
}
