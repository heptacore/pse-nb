/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import utilities.Connection;
import model.Career;
import model.Student;

/**
 *
 * @author Oliver
 */
public class CareerData {
    //  Attributes
    private ResultSet resultSet = null;
    private ArrayList<Career> careers = new ArrayList<>();
    
    //  Methods
    public CareerData(){}
    
    /**
     * Gets all Careers saved on database.
     * @return Collection of {@code Career} that can be empty.
     * @throws SQLException 
     */
    public ArrayList<Career> getAllCareers() throws SQLException {
        ArrayList<Career> careers = new ArrayList<>();
        resultSet = Connection.doQuery("select * from Career;");
        while (resultSet.next()){
            Career career = new Career(
                resultSet.getInt("ID"),
                resultSet.getString("name")
            );
            careers.add(career);
        }
        return careers;
    }
    
    /**
     * Asociates a Student with a Career on Database.
     * @param student
     * @param career 
     */
    public void suscribeStudent(Student student, Career career) {
        Connection.doUpdate("INSERT INTO Student_Career ("
                + "student, "
                + "cohort, "
                + "career, "
                + "finished, "
                + "approved"
                + ") VALUES ("
                + "'" + student.getIdStudent() + "', "
                + "'" + career.getCohort() + "', "
                + "'" + career.getID() + "', "
                + "'" + career.isFinished() + "', "
                + "'" + career.isApproved() + "'"
                + ");");
    }
    
    /**
     * Get all information related to careers of a student
     * @param idStudent ID of the student
     * @return ArrayList Object with all careers
     * @throws SQLException 
     */
    public ArrayList<Career> getStudentCareers(int idStudent) throws SQLException {
        ArrayList<Career> careers = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT * FROM Student_Career "
                + "INNER JOIN Career on Student_Career.career = Career.ID "
                + "WHERE student = '" + idStudent + "';");
        while (resultSet.next()) {
            Career career = new Career(
                    resultSet.getInt("ID"),
                    resultSet.getString("name")
            );
            career.setApproved(resultSet.getBoolean("approved"));
            career.setFinished(resultSet.getBoolean("finished"));
            career.setCohort(resultSet.getString("cohort"));
            careers.add(career);
        }
        return careers;
    }
    
    public ArrayList<Career> getFacultyCareers(int facultyID) throws SQLException {
        ArrayList<Career> careers = new ArrayList<>();
        resultSet = Connection.doQuery("select * from Career where faculty = '" + facultyID + "';");
        while (resultSet.next()){
            Career career = new Career(
                resultSet.getInt("ID"),
                resultSet.getString("name")
            );
            careers.add(career);
        }
        return careers;
    }

    /**
     * Gets a career related to a Stakeholder.
     * @param stakeholderID ID_KEY of the Stakeholder
     * @return
     * {@code Career} object if the Stakeholder has one
     * {@code NULL} if the Stakeholder does not have a Coordination
     * @throws SQLException 
     */
    public Career getStakeholderCareer(int stakeholderID) throws SQLException {
        Career newCareer = null;
        resultSet = Connection.doQuery("SELECT * FROM Career where ID =  " + stakeholderID + ";");
        while (resultSet.next()) {
            newCareer = new Career(
                    resultSet.getInt("ID")
            );
            newCareer.setName(resultSet.getString("name"));
        }
        return newCareer;
    }
}
