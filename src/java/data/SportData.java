/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import utilities.Connection;
import model.Sport;
import java.util.ArrayList;


/**
 *
 * @author Oliver
 */
public class SportData {
    private ResultSet resultSet = null;
    private final SETypeData seTypeData = new SETypeData();
    
    public SportData(){}
    
    public ArrayList<Sport> getExperienceSports(int idExperience) throws SQLException {
        ArrayList<Sport> sports = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT "
                + "sport.ID as 'sportID', sport.approved as 'sportApproved', sport.name as 'sportName' "
                + "FROM Sport "
                + "INNER JOIN sportActivity on Sport.ID = sportActivity.sport "
                + "WHERE experience = '" + idExperience + "';");
        while (resultSet.next()) {
            Sport sport = new Sport();
            sport.setID(resultSet.getInt("sportID"));
            sport.setName(resultSet.getString("sportName"));
            sport.setApproved(resultSet.getInt("sportApproved"));
            sport.setActivities(seTypeData.getExperienceSETypes(idExperience));
            sports.add(sport);
        }
        return sports;
    }
}
