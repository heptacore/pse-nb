package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import utilities.Connection;
import model.Stakeholder;
import model.Person;
import model.Job;
import java.util.ArrayList;

public class StakeholderData {
    private ResultSet resultSet = null;
    
    public StakeholderData(){}
    
    /**
     * Gets a Stakeholder related to a Person.
     * @param idPerson ID_KEY value for the Stakeholder Table on database
     * @return A {@code Stakeholder} object that contains all info from the row
     * on Stakeholder Table.
     */
    public Stakeholder getStakeholderByPerson(int idPerson) throws SQLException {
        Stakeholder newStakeholder = null;
        PersonData personData = new PersonData();
        JobData jobData = new JobData();
        Person newPerson = personData.readData(idPerson);
        resultSet = Connection.doQuery("select * from Student where person = '" + idPerson + "';");
        while (resultSet.next()) {

            ArrayList<Job> jobs = jobData.readData(newPerson.getID());
            newStakeholder = new Stakeholder(
                    newPerson.getID(),
                    resultSet.getInt("ID"),
                    jobs,
                    newPerson.getFirstName(),
                    newPerson.getLastName()
            );
        }
        return newStakeholder;
    }
    
    /**
     * Gets all Stakeholders from database.
     * @return an object that contains all rows from database.
     */
    public ArrayList<Stakeholder> getAllStakeholders() throws SQLException {
        CareerData careerData = new CareerData();
        JobData jobData = new JobData();
        ArrayList<Stakeholder> stakeholders = new ArrayList<>();
        PersonData personData = new PersonData();
        resultSet = Connection.doQuery("SELECT * FROM Stakeholder;");
        boolean hasCoordination = false;

        while (resultSet.next()) {
            Person newPerson = personData.readData(resultSet.getInt("person"));
            Stakeholder newStakeholder = new Stakeholder(
                    newPerson.getID(),
                    resultSet.getInt("ID"),
                    jobData.readData(resultSet.getInt("ID")),
                    newPerson.getFirstName(),
                    newPerson.getSurname()
            );

            //get lastname and lastsurname from personData
            newStakeholder.setLastName(newPerson.getLastName());
            newStakeholder.setLastSurname(newPerson.getLastSurname());
            newStakeholder.setIdCardNumber(newPerson.getIdCardNumber());
            newStakeholder.setGender(newPerson.getGender());

            // stakeholder belongs to a coordination
            newStakeholder.setCoordination(careerData.getStakeholderCareer(resultSet.getInt("coordination")));
            stakeholders.add(newStakeholder);
        }
        return stakeholders;
    }
    
    /**
     * Returns data from row on Stakeholder Table where a coincidence is found
     * on database.
     * @param stakeholderID ID_KEY for the Table Stakeholder wich is going to be
     * searched for
     * @return returns all data from the found row in a {@code Stakehodler}
     * object.
     * @see model.Stakeholder
       *
     */
    public Stakeholder getStakeholderByID_KEY(String stakeholderID) throws SQLException {
        Stakeholder newStakeholder = null;
        PersonData personData = new PersonData();
        resultSet = Connection.doQuery("select * from Stakeholder where ID = '" + stakeholderID + "';");
        while (resultSet.next()) {
            newStakeholder = new Stakeholder(personData.readData(resultSet.getInt("person")));
            newStakeholder.setId(resultSet.getInt("ID"));
        }
        return newStakeholder;
    }
    
    /**
     * Deletes a row in Stakeholder Table on Database
     * @param object for get ID
     **/
    public void deleteStakeholder(Stakeholder object) {
        Stakeholder stakeholder = (Stakeholder) object;
        Connection.doUpdate("DELETE FROM Stakeholder WHERE ID = " + stakeholder.getID() + ";");
        Connection.endQuery();
    }

    
    /**
     * Gets the ID of a row on Stkaholder Table on database searching for
     * a column name and a passed value
     * @param column column on table Stakeholder to be inspected
     * @param value value of the column to be searched
     * @return returns the ID of the row if there was a coincidence otherwise it returns -1
     **/
    public int getID_KEY(String column, String value) throws SQLException {
        resultSet = Connection.doQuery("SELECT * FROM Stakeholder WHERE " + column + " = '" + value + "';");
        if (resultSet.first()){
            return resultSet.getInt("ID");
        }
        return -1;
    }
    
    /**
     * Inserts a row in Stakeholder Tabla on database with specified data.
     * @param stakeholder data to record on Stakeholder Table from database.
     */
    public void saveStakeholder(Stakeholder stakeholder) {
        String coordination = "NULL";
        
        if (stakeholder.getCoordination() != null) {
            coordination = Integer.toString(stakeholder.getCoordination().getID());
        }
        Connection.doUpdate("INSERT INTO Stakeholder ("
                + "person, "
                + "coordination"
                + ") VALUES ("
                + "'" + stakeholder.getID()+ "', "
                + "'" + coordination + "'"
                + ");");
    }
    
    /**
     * Looks on DataBase wether a Person
     * is a Stakeholder.
     * @param stakeholder Object to extract {@code Person Table ID_KEY}
     * @return true if Person is a Stakeholder, false if not.
     * @throws SQLException 
     */
    public boolean isOnDatabase(Stakeholder stakeholder) throws SQLException {
        resultSet = Connection.doQuery("SELECT * FROM Stakeholder WHERE person = '" + stakeholder.getID() + "';");
        if (resultSet.first()) {
            return true;
        }
        return false;
    }
}
