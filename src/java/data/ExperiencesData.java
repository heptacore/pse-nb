/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import model.Experiences;
import java.util.ArrayList;
import model.Achievement;
import model.Experience;
import model.InterestGroup;
import model.Organization;
import model.Sport;
import model.Culture;
import model.Person;
import model.University;
import utilities.Connection;

/**
 *
 * @author Oliver
 */
public class ExperiencesData {
    private ResultSet resultSet;
    private final ExperienceData experienceData = new ExperienceData();
    private final OrganizationData organizationData = new OrganizationData();
    private final AchievementData achievementData = new AchievementData();
    private final InterestGroupData interestGroupData = new InterestGroupData();
    private final StudentJobData studentJobData = new StudentJobData();
    private final SportData sportData = new SportData();
    private final CultureData cultureData = new CultureData();
    private final PersonData personData = new PersonData();
    private final UniversityData universityData = new UniversityData();
    
    public ExperiencesData(){};
    
    public ArrayList<Experiences> getExperiences() throws SQLException {
        ArrayList<Experiences> experiences = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT * FROM Experiences;");
        while (resultSet.next()) {
            Experience expName = experienceData.getExperience(resultSet.getInt("name"));
            Experiences experience = new Experiences(
                resultSet.getInt("ID"),
                resultSet.getString("date"),
                resultSet.getInt("approved"),
                expName
            );
            experience.setDescription(resultSet.getString("description"));
            //  Init achievements if linked.
            ArrayList<Achievement> achievements = achievementData.getExperienceAchievements(resultSet.getInt("ID"));
            if (achievements.isEmpty() == false) {
                experience.setAchievements(achievements);
            }
            //  Init organizations with it's jobs if linked and role for this experience.
            ArrayList<Organization> organizations = organizationData.getExperienceOrganizations(resultSet.getInt("ID"));
            if (organizations.isEmpty() == false) {
                experience.setOrganizations(organizations);
            }
            //  Init interest groups with it's roles and participations if linked for this experience.
            ArrayList<InterestGroup> interestGroups = interestGroupData.getExperienceInterestGroups(resultSet.getInt("ID"));
            if (interestGroups.isEmpty() == false) {
                experience.setInterestGroups(interestGroups);
            }
            //  Init sports if linked
            ArrayList<Sport> sports = sportData.getExperienceSports(resultSet.getInt("ID"));
            if (sports.isEmpty() == false) {
                experience.setSport(sports);
            }
            //  Init culture if linked
            ArrayList<Culture> cultures = cultureData.getExperienceCultures(resultSet.getInt("ID"));
            if (cultures.isEmpty() == false) {
                experience.setCulture(cultures);
            }
            //  Init univerities if linked
            ArrayList<University> universities = universityData.getExperienceUniversities(resultSet.getInt("ID"));
            if (universities.isEmpty() == false) {
                experience.setUniversities(universities);
            }
            //  Init contacts if linked
            //ArrayList<Person> contacts = personData.getExperienceContacts(resultSet.getInt("ID"));
            /*if (contacts.isEmpty() == false) {
                experience.setContacts(contacts);
            }*/
            experiences.add(experience);
        }
        return experiences;
    }
}
