/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import model.Person;
import model.Phone;
import model.Email;
import model.User;
import java.util.ArrayList;
import model.Entity;
import utilities.Connection;
import utilities.Persistency;

/**
 *
 * @author Christian Garcia
 */
public class PersonData {
    
    //  Variables
    private ResultSet resultSet = null;
    private final PhoneData phoneData = new PhoneData();
    private final EmailData emailData = new EmailData();
    private final UserData userData = new UserData();
    
    public PersonData(){};

    /* FIXME
     *public ArrayList<Person> getExperienceContacts(int idExperience) throws SQLException {
        ArrayList<Person> contacts = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT * FROM Person "
                + "INNER JOIN Contact on Person.ID = Contact.person "
                + "WHERE Contact.experience = '" + idExperience + " ';");
        while (resultSet.next()) {
            Person retrievedPerson = new Person(
                    resultSet.getInt("ID"),
                    resultSet.getString("firstName"),
                    resultSet.getString("surname")
            );
            retrievedPerson.setIdCardNumber(resultSet.getString("idCardNumber"));
            retrievedPerson.setLastName(resultSet.getString("lastName"));
            retrievedPerson.setLastSurname(resultSet.getString("lastSurname"));
            retrievedPerson.setGender(resultSet.getString("gender"));
            retrievedPerson.setContactRole(resultSet.getString("description"));
            phones.readData(retrievedPerson.getID());
            //  It has at least a phone
            if (phones.getPhones().size() > 0) {
                //  Assign all phones to it's owner
                for (int i = 0; i < phones.getPhones().size(); i++) {
                    retrievedPerson.getPhones().add(phones.getPhones().get(i));
                }
            }
            emails.readData(retrievedPerson.getID());
            //  It has at least an email
            if (emails.getEmails().size() > 0) {
                //  Assign all emails to it's owner
                for (int i = 0; i < emails.getEmails().size(); i++) {
                    retrievedPerson.getEmailAddresses().add(emails.getEmails().get(i));
                }
            }
            contacts.add(retrievedPerson);
        }
        return contacts;
    }*/
    
    /**
     * Looks for a row where specified parameters are found in database.
     * @param column column name in Person table
     * @param value value to look in specified column
     * @return 1 = found, 0 = not found
     */
    public  boolean existsPerson(String column, String value)   throws  SQLException   {
        resultSet = Connection.doQuery("SELECT * FROM Person WHERE " + column + " = '" + value + "';");
        //  If some row happens to exist
        if (resultSet.first()) {
            return true;
        }
        return false;
    }

    /**
     * Reads all information related to a Person Object from database on
     * all tables related to Person Table.
     * @param ID ID of the person on database.
     * @return a Person object with all it's information initialized according to database data.
     * @throws SQLException 
     */
    public Person readData(int ID) throws SQLException {
        resultSet = Connection.doQuery("SELECT * FROM Person WHERE ID = '" + ID + " ';");
        while (resultSet.next()) {
            User user = null;
            
            Person retrievedPerson = new Person(
                    resultSet.getInt("ID"),
                    resultSet.getString("firstName"),
                    resultSet.getString("surname")
            );
            retrievedPerson.setIdCardNumber(resultSet.getString("idCardNumber"));
            retrievedPerson.setLastName(resultSet.getString("lastName"));
            retrievedPerson.setLastSurname(resultSet.getString("lastSurname"));
            retrievedPerson.setGender(resultSet.getString("gender"));
            
            //  Get Emails
            retrievedPerson.setEmailAddresses(emailData.readData(retrievedPerson.getID()));
            
            //  Get Phones
            retrievedPerson.setPhones(phoneData.readData(retrievedPerson.getID()));
            
            //  Get User
            retrievedPerson.setUser(userData.getPersonUser(retrievedPerson.getID()));
            return retrievedPerson;
        }
        return null;
    }
    
    /**
     * Gets the ID column value from Person Table in DataBase
     * where row equals to specified values.
     * @param column column name in Person Table on database.
     * @param value value to be evaluated
     * @return ID Column Value if found, -1 if row not found.
     * @throws SQLException 
     */
    public int getID(String column, String value) throws SQLException {
        resultSet = Connection.doQuery("SELECT * FROM Person WHERE " + column + " = '" + value + "';");
        if (resultSet.first()) {
            return resultSet.getInt("ID");
        }
        return -1;
    }
    
    public void createData(Person object){
        Connection.doUpdate("INSERT INTO Person ("
            + "idCardNumber, "
            + "firstName, "
            + "lastName, "
            + "surname, "
            + "lastSurname, "
            + "gender"
            + ") values ("
            + "'" + object.getIdCardNumber() + "', "
            + "'" + object.getFirstName() + "', "
            + "'" + object.getLastName() + "', "
            + "'" + object.getSurname() + "', "
            + "'" + object.getLastSurname() + "', "
            + "'" + object.getGender() + "'"
            + ");");
    }
}
