/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.ResultSet;
import java.sql.SQLException;
import utilities.Connection;
import model.Email;
import java.util.ArrayList;

/**
 *
 * @author Christian Garcia
 */
public class EmailData {
    //  Attributes
    private ResultSet resultSet = null;
    
    //  Methods
    public EmailData(){}
    
    public void createData(Email object){
        Connection.doUpdate("INSERT INTO Email ("
                + "address, "
                + "owner, "
                + "tag"
                + ") VALUES ("
                + "'" + object.getAddress() + "', "
                + "'" + object.getOwner() + "', "
                + "'" + object.getTag() + "'"
                + ");");
    }
    
    public ArrayList<Email> readData(int ID) throws SQLException {
        ArrayList<Email> emails = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT * FROM Email WHERE owner = '" + ID + "';");
        while (resultSet.next()) {
            Email newEmail = new Email(resultSet.getString("address"));
            newEmail.setTag(resultSet.getString("tag"));
            emails.add(newEmail);
        }
        return emails;
    }
}
