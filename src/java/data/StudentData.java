package data;

import java.util.ArrayList;
import model.Student;
import utilities.Connection;
import utilities.Persistency;
import model.Phone;
import model.Email;
import model.Entity;
import model.Career;
import model.Person;
import java.sql.SQLException;
import java.sql.ResultSet;

public class StudentData {

    //	Attributes
    private Student singleStudent;
    private Phone singlePhone;
    private Email singleEmail;
    private PersonData personData = new PersonData();
    private EmailData emailData = new EmailData();
    private PhoneData phoneData = new PhoneData();
    private CareerData careerData = new CareerData();
    private ResultSet resultSet = null;
    private ArrayList<Student> students = new ArrayList<>();
    private ArrayList<Phone> phones = new ArrayList<>();
    private ArrayList<Email> emails = new ArrayList<>();
    private ArrayList<Career> careers = new ArrayList<>();

    public StudentData() {}
    
    public int getID(String column, String value) throws SQLException{
        resultSet = Connection.doQuery("SELECT * FROM Student WHERE " + column + " = '" + value + "';");
        if (resultSet.first()) {
            return resultSet.getInt("ID");
        }
        return -1;
    }

    public void createData(Student object) {
        Connection.doUpdate(""
                + "INSERT INTO Student ("
                + "studentId, "
                + "exStudentId, "
                + "person"
                + ") values ("
                + "'" + object.getStudentIdNumber() + "', "
                + "'" + object.getExStudentIdNumber() + "', "
                + "'" + object.getID() + "'"
                + ");");
    }
    
    /**
     * Gets an ammount of Student objects paginated in a defined set.
     * @param page page to be retrieved
     * @param size ammount of elements per page
     * @return
     * @throws SQLException
     */
    public ArrayList<Student> getStudentsPage(int page, int pageSize) throws SQLException {
        int first, countAdds, lastElement;
        
        first = getStartItemByPage(page, pageSize);
        ArrayList<Student> students = new ArrayList<>();
        countAdds = 0;
        resultSet = Connection.doQuery("SELECT * FROM Student;");
        boolean more = resultSet.absolute(first);
        while (more) {
            // Sustituir por nuestra creacion de Beans
            // MyBean m = new MyBean();
            // m.setDato(rs.getInt(1));
            // results.add(m);
            Person person = personData.readData(resultSet.getInt("person"));
            
            //  Person, Phone, Email and User Tables Information
            Student student = new Student(person);
            
            //  Student Table Information
            student.setIdStudent(resultSet.getInt("ID"));
            student.setStudentIdNumber(resultSet.getString("studentId"));
            student.setExStudentIdNumber(resultSet.getString("exStudentId"));
            
            //  Student_Career Table
            student.setCareers(careerData.getStudentCareers(student.getIdStudent()));
            
            students.add(student);
            if (++ countAdds == pageSize) {
                break;
            }
            more = resultSet.next();
        }
        lastElement = getLastElement(resultSet);
        return students;
    }
    
    public static int getStartItemByPage(int currentPage, int pageSize) {
        return Math.max((pageSize * (currentPage - 1)) + 1, 1);
    }
    
    public static int getLastPage(int totalElements, int pageSize) {
        int base = totalElements / pageSize;
        int mod = totalElements % pageSize;
        return base + (mod > 0?1:0);
    }
    
    /**
     * Gets the last row number of Student Table
     * @return -1 if an resultset has no last row, row number if all succeded.
     * @throws SQLException 
     */
    public int getLastElement(ResultSet resultSet) throws SQLException {
        if (resultSet.last()) {
            return resultSet.getRow();
        }
        return -1;
    }

    public void readData() throws SQLException {
        if (students.isEmpty() == false) {
            students.clear();
        }
        resultSet = Connection.doQuery("SELECT Student.ID as 'studentID', "
                + "studentId, exStudentId, person, idCardNumber, firstName, "
                + "lastName, surname, lastSurname, Person.ID as 'personID', gender "
                + "FROM Student LEFT JOIN Person on Student.person = Person.ID;"
        );
        while (resultSet.next()) {
            singleStudent = new Student(resultSet.getInt("studentID"),
                    resultSet.getInt("personID"), resultSet.getString("firstName"),
                    resultSet.getString("surname"));
            singleStudent.setStudentIdNumber(resultSet.getString("studentId"));
            singleStudent.setExStudentIdNumber(resultSet.getString("exStudentId"));
            singleStudent.setLastName(resultSet.getString("lastName"));
            singleStudent.setSurname(resultSet.getString("surname"));
            singleStudent.setLastSurname(resultSet.getString("lastSurname"));
            singleStudent.setGender(resultSet.getString("gender"));
            singleStudent.setIdCardNumber(resultSet.getString("idCardNumber"));
            students.add(singleStudent);
        }
        Connection.endQuery();

        for (int i = 0; i < students.size(); i++) {
            resultSet = Connection.doQuery("SELECT Career.ID as 'ID', faculty, cohort, name, finished, approved FROM Student_Career "
                    + "INNER JOIN Student on Student_Career.student = Student.ID "
                    + "INNER JOIN Career on Student_Career.career = Career.ID "
                    + "WHERE Student_Career.student = '"+students.get(i).getIdStudent()+"';");
            while (resultSet.next()) {
                Career singleCareer = new Career(resultSet.getInt("ID"), resultSet.getString("name"));
                singleCareer.setCohort(resultSet.getString("cohort"));
                singleCareer.setFinished(resultSet.getBoolean("finished"));
                singleCareer.setApproved(resultSet.getBoolean("approved"));
                students.get(i).getCareers().add(singleCareer);
                //careers.add(singleCareer);
            }
            /*if (careers.isEmpty()==false) {
                students.get(i).setCareers(careers);
                careers.clear();
            }*/
            Connection.endQuery();
        }
        
        System.out.println("====== STUDENT DEBBUGER  =============");
        System.out.println("Describes status for the Student arraylist.");
        for (int i = 0; i < students.size(); i++) {
            System.out.println("Student = " + students.get(i).getFirstName() + ", Careers = " + students.get(i).getCareers().size() + "");
        }
        System.out.println("====== END STUDENT DEBBUGER  =========");
    }

    public void updateData(Entity object, int localArrayIndex) {
//        Student receivedStudent = (Student) object;
//        //  update object at local array.
//        students.remove(localArrayIndex);
//        students.add(localArrayIndex, (Student) object);
//
//        //  update object at database.
//        Connection.doUpdate("UPDATE Person SET "
//                + "idCardNumber = '" + receivedStudent.getIdCardNumber() + "',"
//                + "firstName = '" + receivedStudent.getFirstName() + "',"
//                + "lastName = '" + receivedStudent.getLastName() + "',"
//                + "surname = '" + receivedStudent.getSurname() + "',"
//                + "lastSurname = '" + receivedStudent.getLastSurname() + "',"
//                + "gender = '" + receivedStudent.getGender() + "' "
//                + "WHERE Person.ID = " + receivedStudent.getIdPerson() + ";");
    }

    
    public void deleteData(Entity object) {
//        singleStudent = (Student) object;
//        Connection.doUpdate("DELETE FROM Person WHERE ID = " + singleStudent.getIdPerson() + ";");
//        Connection.endQuery();
    }

    public Student readData(int idPerson) throws SQLException {
        Student newStudent = null;
        Person newPerson = personData.readData(idPerson);
        resultSet = Connection.doQuery("select * from Student where person = '" + idPerson + "';");
        while (resultSet.next()) {
            newStudent = new Student(
                    resultSet.getInt("ID"),
                    resultSet.getInt("studentId"),
                    newPerson.getFirstName(),
                    newPerson.getSurname()
            );
        }
        return newStudent;
    }
    
    /**
     * Returns a student from database with specified Student ID
     * @param ID
     * @return
     * @throws SQLException 
     */
    public Student getStudent(String studentID) throws SQLException {
        Student newStudent = null;
        resultSet = Connection.doQuery("select * from Student where studentId = '" + studentID + "';");
        while (resultSet.next()) {
            newStudent = new Student(personData.readData(resultSet.getInt("person")));
            newStudent.setIdStudent(resultSet.getInt("ID"));
            newStudent.setStudentIdNumber(resultSet.getString("studentId"));
        }
        return newStudent;
    }
    
    /**
     * Verifies wether a student exists in database.
     * @param student
     * @return 0 = does not exist on database, 1 = exists on database
     * @throws SQLException 
     */
    public boolean existStudent(Student student) throws SQLException {
        resultSet = Connection.doQuery("SELECT * FROM Student WHERE studentId = '" + student.getStudentIdNumber() + "';");
        if (resultSet.first()) {
            return true;
        }
        return false;
    }
}
