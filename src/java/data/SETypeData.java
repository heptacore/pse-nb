/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import model.SEType;
import java.util.ArrayList;
import utilities.Connection;

/**
 *
 * @author Oliver
 */
public class SETypeData {
    private ResultSet resultSet = null;
    
    public SETypeData() {}
    
    public ArrayList<SEType> getExperienceSETypes(int idExperience) throws SQLException {
        ArrayList<SEType> SETypes = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT SE_Type.ID as 'ID', SE_Type.name as 'name', SE_Type.approved as 'approved' "
                + "FROM SE_Type INNER JOIN sportActivity on SE_Type.ID = sportActivity.type "
                + "WHERE sportActivity.experience = '" + idExperience + "';");
        while (resultSet.next()) {
            SEType object = new SEType();
            object.setID(resultSet.getInt("ID"));
            object.setName(resultSet.getString("name"));
            object.setApproved(resultSet.getInt("approved"));
            SETypes.add(object);
        }
        return SETypes;
    }
}
