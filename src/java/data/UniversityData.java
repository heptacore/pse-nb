/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import utilities.Connection;
import model.University;
import java.util.ArrayList;

/**
 *
 * @author Oliver
 */
public class UniversityData {
    private ResultSet resultSet = null;
    private final CountryData countryData = new CountryData();
    
    public UniversityData() {}
    
    public ArrayList<University> getExperienceUniversities(int idExperience) throws SQLException {
        ArrayList<University> universities = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT * FROM University "
                + "INNER JOIN Country on University.country = Country.name "
                + "INNER JOIN Experience_University on University.ID = Experience_University.university "
                + "WHERE Experience_University.experience = '" + idExperience + "';");
        while (resultSet.next()) {
            University university = new University();
            university.setID(resultSet.getInt("ID"));
            university.setName(resultSet.getString("name"));
            university.setJS(resultSet.getInt("isJs"));
            university.setApproved(resultSet.getInt("approved"));
            university.setCountry(countryData.getCountryName(resultSet.getString("country")));
            university.setExperienceRole(resultSet.getString("description"));
            universities.add(university);
        }
        return universities;
    }
}
