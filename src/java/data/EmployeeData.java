/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.util.ArrayList;
import model.*;
import utilities.Persistency;
import java.sql.ResultSet;
import utilities.Connection;

/**
 *
 * @author Christian Garcia
 */
public class EmployeeData {
    private ResultSet resultSet = null;
    
    /**
     * Gets an instance of this class.
     */
    public EmployeeData(){}
    
    /**
     *  Inserts a row in {@code Employee Table} in database
     *  @param stakeholder
     *      data will be extracted from this object.
     *  @see utilities.Connection
     */
    public void createEmployee(int stakeholderID, int jobID){
        Connection.doUpdate("INSERT INTO Employee ("
                + "stakeholder, "
                + "job"
                + ") VALUES ("
                + "'" + stakeholderID + "', "
                + "'" + jobID + "'"
                + ");");
    }
    
    /**
     * Records data related to an employee on necesary tables from database.
     * @param stakeholder
     * @param job 
     */
    public void createEmployee(Stakeholder stakeholder) throws SQLException {
        PersonData personData = new PersonData();
        PhoneData phoneData = new PhoneData();
        EmailData emailData = new EmailData();
        StakeholderData stakeholderData = new StakeholderData();
        UserData userData = new UserData();
        
        //  [PROCESS][1]: Insert into Person Table.
        System.out.println("Inserting into person table...");
        personData.createData(stakeholder);
        stakeholder.setID(personData.getID("idCardNumber", stakeholder.getIdCardNumber()));
        
        //  [PROCESS][1.1]: Insert into Phone Table
        System.out.println("Inserting into Phone table...");
        //  Employee has phones
        for (int i = 0; i < stakeholder.getPhones().size(); i++) {
            phoneData.createData(stakeholder.getPhones().get(i));
        }
        
        //  [PROCESS][1.2]: Insert into Email Table
        System.out.println("Inserting into Email table...");
        for (int i = 0; i < stakeholder.getEmailAddresses().size(); i++) {
            emailData.createData(stakeholder.getEmailAddresses().get(i));
        }
        
        //  [PROCESS][2]: Insert into Stakeholder Table.
        System.out.println("Inserting into Stakeholder table. . .");
        stakeholderData.createStakeholder(stakeholder);
        stakeholder.setId(stakeholderData.getID("person", Integer.toString(stakeholder.getID())));
        
        //  [PROCESS][3]: Insert into Employee Table.
        System.out.println("Inserting into Employee table. . .");
        for (int i = 0; i < stakeholder.getJobs().size(); i++) {
            this.createEmployee(stakeholder.getId(), stakeholder.getJobs().get(i).getID());
        }
        
        //  [PROCESS][4]: Insert into User Table.
        System.out.println("Inserting into User Table...");
        userData.createData(new User(stakeholder.getUser().getNickname(), stakeholder.getUser().getPassword()));
    }
}
