/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.ResultSet;
import java.util.ArrayList;
import utilities.Connection;
import java.sql.SQLException;
import model.Message;
import model.User;

/**
 *
 * @author Oliver
 */
public class MessageData {
    private ResultSet resultSet = null;
    private UserData userData = new UserData();
    
    public MessageData(){}
    
    public ArrayList<User> readConversations (User myUser) throws SQLException {
        ArrayList<User> conversations = new ArrayList<>();
        resultSet = Connection.doQuery("select [User].nickname from Message "
            + "inner join [User] on Message.receiverUser = [User].nickname or Message.senderUser = [User].nickname "
            + "where senderUser = '" + myUser.getNickname() + "' or receiverUser = '" + myUser.getNickname() + "' "
            + "group by nickname;"
        );
        while(resultSet.next()) {
            //  User is not myselft
            if (resultSet.getString("nickname").equals(myUser.getNickname()) == false) {
                User conversation = userData.readData(resultSet.getString("nickname"));
                conversations.add(conversation);
            }
        }
        return conversations;
    }
    
    public Message getLastMessage(User myUser, User user2) throws SQLException {
        Message lastMessage = null;
        resultSet = Connection.doQuery("select * from Message "
            + "right join [User] on Message.senderUser = [User].nickname "
            + "where (nickname = '" + myUser.getNickname() + "' or nickname = '" + user2.getNickname() + "') and "
            + "(senderUser = '" + myUser.getNickname() + "' or senderUser = '" + user2.getNickname() + "') and "
            + "(receiverUser = '" + myUser.getNickname() + "' or receiverUser = '" + user2.getNickname() + "') "
            + "order by date desc;"
        );
        //  There's at least a result
        if (resultSet.first() == true) {
            lastMessage = new Message(
                userData.readData(resultSet.getString("senderUser")),
                userData.readData(resultSet.getString("receiverUser")),
                resultSet.getString("value"),
                resultSet.getString("date")
            );
            lastMessage.setDeleted(resultSet.getInt("deleted"));
            lastMessage.setSeen(resultSet.getInt("seen"));
        }
        return lastMessage;
    }
    
    /**
     * 
     * @param myUser main user's nickname
     * @param user2 user2 nickname
     * @param messages Ammount of messages to be fetched.
     * @return
     * @throws SQLException 
     */
    public ArrayList<Message> getMessages(String myUser, String user2, int messagesAmmount) throws SQLException {
        //  TODO    Ammount of messages to be fetched.
        ArrayList<Message> messages = new ArrayList<>();
        resultSet = Connection.doQuery("select * from Message "
            + "right join [User] on Message.senderUser = [User].nickname "
            + "where (nickname = '" + myUser + "' or nickname = '" + user2 + "') and "
            + "(senderUser = '" + myUser + "' or senderUser = '" + user2 + "') and "
            + "(receiverUser = '" + myUser + "' or receiverUser = '" + user2 + "') "
            + "order by date asc;"
        );
        //  There's at least a result
        while (resultSet.next()) {
            Message message = new Message(
                userData.readData(resultSet.getString("senderUser")),
                userData.readData(resultSet.getString("receiverUser")),
                resultSet.getString("value"),
                resultSet.getString("date")
            );
            message.setDeleted(resultSet.getInt("deleted"));
            message.setSeen(resultSet.getInt("seen"));
            messages.add(message);
        }
        return messages;
    }
}
