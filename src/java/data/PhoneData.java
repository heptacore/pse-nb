/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Entity;
import model.Person;
import utilities.Persistency;
import utilities.Connection;
import model.Phone;

/**
 *
 * @author Christian Garcia
 */
public class PhoneData {
    
    private ResultSet resultSet = null;
    
    public PhoneData() {}
    
    public ArrayList<Phone> readData(int number) throws SQLException {
        ArrayList<Phone> phones = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT * FROM Phone WHERE owner = '" + number + "';");
        while (resultSet.next()) {
            Phone newPhone = new Phone(resultSet.getString("number"));
            newPhone.setTag(resultSet.getString("tag"));
            phones.add(newPhone);
        }
        return phones;
    }

    public void createData(Phone object) {
        Connection.doUpdate("INSERT INTO Phone ("
                + "number, "
                + "owner, "
                + "tag"
                + ") VALUES ("
                + "'" + object.getNumber() + "', "
                + "'" + object.getOwner() + "', "
                + "'" + object.getTag() + "'"
                + ");");
    }
    
}
