/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.InterestGroup;
import utilities.Connection;

/**
 *
 * @author Oliver
 */
public class InterestGroupData {
    private ResultSet resultSet = null;
    private final GroupRoleData groupRoleData = new GroupRoleData();
    
    public InterestGroupData(){}
    
    public ArrayList<InterestGroup> getExperienceInterestGroups(int experienceID) throws SQLException {
        ArrayList<InterestGroup> interestGroups = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT * FROM Interest_Group "
                + "INNER JOIN Exp_IG on Interest_Group.ID = Exp_IG.interestGroup "
                + "WHERE Exp_IG.experience = '" + experienceID + "';");
        while (resultSet.next()){
            InterestGroup interestGroup = new InterestGroup();
            //  There's a role group on the experience
            if (resultSet.getInt("role") != 0) {
                interestGroup.getGroupRoles().add(groupRoleData.getExperienceGroupRole(experienceID));
            }
            interestGroup.setID(resultSet.getInt("ID"));
            interestGroup.setName(resultSet.getString("name"));
            interestGroup.setDescription(resultSet.getString("description"));
            interestGroup.setApproved(resultSet.getInt("approved"));
            interestGroup.setExperienceRole(resultSet.getString("participation"));
            interestGroups.add(interestGroup);
        }
        return interestGroups;
    }
}
