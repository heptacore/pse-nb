/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Entity;
import model.Person;
import model.User;
import model.Stakeholder;
import model.Student;
import utilities.Persistency;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilities.Connection;

/**
 *
 * @author OLIMON
 */
public class UserData {
    private Person singlePerson;
    private User singleUser;
    private ResultSet resultSet = null;
    
    public UserData() {}

    public void createData(User object) {
        Connection.doUpdate(""
                + "INSERT INTO [User] ("
                + "nickname, "
                + "password, "
                + "avatar, "
                + "type, "
                + "person"
                + ") values ("
                + "'" + object.getNickname() + "', "
                + "'" + object.getPassword() + "', "
                + "'" + object.getAvatar() + "', "
                + "'" + object.getType() + "', "
                + "'" + object.getPerson().getID() + "')"
        + ";");
    }

    public ArrayList<User> readData() {
        ArrayList<User> users = new ArrayList<>();
        try {
            resultSet = Connection.doQuery("SELECT * FROM [User];");
            while (resultSet.next()) {
                User newUser = new User(
                        resultSet.getString("nickname"),
                        resultSet.getString("password")
                );
                newUser.setAvatar(resultSet.getString("avatar"));
                newUser.setType(resultSet.getInt("type"));
                
                //  user is a person
                if (resultSet.getInt("person") != 0) {
                    PersonData personData = new PersonData();
                    StakeholderData stakeholderData = new StakeholderData();
                    StudentData studentData = new StudentData();
                    Person userPerson = personData.readData(resultSet.getInt("person"));
                    Stakeholder stakeholder = stakeholderData.readData(userPerson.getID());
                    Student student = studentData.readData(userPerson.getID());
                    //  user is a stakeholder
                    if (stakeholder != null) {
                        newUser.setPerson(stakeholder);
                    } else {
                        //  user is a student
                        if (student != null) {
                            newUser.setPerson(student);
                        } else {
                            newUser.setPerson(userPerson);
                        }
                    }
                } else {
                    newUser.setPerson(null);
                }
                users.add(newUser);
            }
        } catch (SQLException ex) {
            System.out.println("An error has ocurred: " + ex);
        }
        return users;
    }

    /**
     * Looks for a row on User table from database wich has the specified value
     * on specified column.
     * @param column column on User table from database.
     * @param value value to be evaluated
     * @return 1 = found, 0 = not found
     * @throws SQLException 
     */
    public boolean existsUser(String column, String value) throws SQLException {
        resultSet = Connection.doQuery("SELECT * FROM [User] WHERE " + column + " = '" + value + "';");
        //  There was a row found
        if (resultSet.first()) {
            return true;
        }
        return false;
    }
    
    public User readData(String ID) throws SQLException {
        User newUser = null;
        try {
            resultSet = Connection.doQuery("SELECT * FROM [User] WHERE nickname = '" + ID + "';");
            while (resultSet.next()) {
                newUser = new User(resultSet.getString("nickname"), resultSet.getString("password"));
                System.out.println("ha entrado a la funcion");
                newUser.setAvatar(resultSet.getString("avatar"));
                newUser.setType(resultSet.getInt("type"));
                
                //  If the user belongs to a person.
                if (resultSet.getInt("person") != 0) {
                    PersonData personData = new PersonData();
                    Person userPerson = personData.readData(resultSet.getInt("person"));
                    newUser.setPerson(userPerson);
                } else {
                    newUser.setPerson(null);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return newUser;
    }
    
    /**
     * Gets a user related to a person.
     * @param idPerson id of the person wich the user is related to
     * @return null if no user was found.
     * @throws SQLException 
     */
    public User getPersonUser(int idPerson) throws SQLException {
        User user = null;
        resultSet = Connection.doQuery("SELECT * FROM [User] WHERE person = '" + idPerson + "';");
        while (resultSet.next()){
            user = new User(
                resultSet.getString("nickname"),
                resultSet.getString("password")
            );
            user.setAvatar(resultSet.getString("avatar"));
            user.setType(resultSet.getInt("type"));
        }
        return user;
    }
    
    public User readData(String nickname, String password) throws SQLException {
        User newUser = null;
        try {
            resultSet = Connection.doQuery("SELECT * FROM [User]"
                + " WHERE nickname = '" + nickname + "' AND password = '" + password + "';");
            //  User was found.
            if (resultSet.first()) {
                newUser = new User(
                        resultSet.getString("nickname"),
                        resultSet.getString("password")
                );
                newUser.setAvatar(resultSet.getString("avatar"));
                newUser.setType(resultSet.getInt("type"));
                
                //  If the user belongs to a person.
                if (resultSet.getInt("person") != 0) {
                    PersonData personData = new PersonData();
                    Person userPerson = personData.readData(resultSet.getInt("person"));
                    newUser.setPerson(userPerson);
                } else {
                    newUser.setPerson(null);
                }
            }
        } catch (SQLException ex) {
            System.out.println("An error has occured: " + ex);
        }
        return newUser;
    }
}
