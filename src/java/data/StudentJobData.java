/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import model.StudentJob;
import java.util.ArrayList;
import utilities.Connection;

/**
 *
 * @author Oliver
 */
public class StudentJobData {
    private ResultSet resultSet;
    
    public StudentJobData() {}
    
    public ArrayList<StudentJob> getStudentJobs(int idOrganization) throws SQLException {
        ArrayList<StudentJob> jobs = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT * FROM StudentJob "
                + "INNER JOIN OrganizationJob on StudentJob.ID = OrganizationJob.studentJob "
                + "WHERE OrganizationJob.organization = '" + idOrganization + "';");
        while (resultSet.next()) {
            StudentJob job = new StudentJob(
                resultSet.getInt("ID"),
                resultSet.getString("name")
            );
            job.setDescription(resultSet.getString("description"));
            job.setApproved(resultSet.getInt("approved"));
            jobs.add(job);
        }
        return jobs;
    }
    
    public StudentJob getExperienceJob(int idStudentJob) throws SQLException {
        StudentJob studentJob = null;
        resultSet = Connection.doQuery("SELECT * FROM StudentJob "
                + "INNER JOIN OrganizationJob on StudentJob.ID = OrganizationJob.studentJob "
                + "WHERE OrganizationJob.ID = '" + idStudentJob + "';");
        if (resultSet.first()) {
            studentJob = new StudentJob(
                resultSet.getInt("ID"),
                resultSet.getString("name")
            );
            studentJob.setApproved(resultSet.getInt("approved"));
            studentJob.setDescription(resultSet.getString("description"));
        }
        return studentJob;
    }
}
