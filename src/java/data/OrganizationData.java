/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;
import java.sql.SQLException;
import java.sql.ResultSet;
import utilities.Connection;
import model.Organization;
import java.util.ArrayList;

/**
 *
 * @author Oliver
 */
public class OrganizationData {
    private ResultSet resultSet = null;
    private final StudentJobData studentJobData = new StudentJobData();
    
    public OrganizationData(){}
    
    public ArrayList<Organization> getOrganizations() throws SQLException {
        ArrayList<Organization> organizations = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT * FROM Organization;");
        while (resultSet.next()){
            Organization organization = new Organization(
                resultSet.getInt("ID"),
                resultSet.getString("name")
            );
            organizations.add(organization);
        }
        return organizations;
    }
    
    public ArrayList<Organization> getExperienceOrganizations(int idExperience) throws SQLException {
        ArrayList<Organization> organizations = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT * FROM Organization "
                + "INNER JOIN Experiences_Organization on Organization.ID = Experiences_Organization.organization "
                + "WHERE Experiences_Organization.experience = '" + idExperience + "';");
        while (resultSet.next()) {
            Organization organization = new Organization(
                resultSet.getInt("ID"),
                resultSet.getString("name")
            );
            //  get jobs in organization if there are.
            if (resultSet.getInt("job") != 0) {
                organization.getJobs().add(studentJobData.getExperienceJob(resultSet.getInt("job")));
            }
            organization.setApproved(resultSet.getInt("approved"));
            organization.setExperienceRole(resultSet.getString("role"));
            organizations.add(organization);
        }
        return organizations;
    }
}