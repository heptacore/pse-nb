/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import model.CEType;
import java.util.ArrayList;
import utilities.Connection;

/**
 *
 * @author Oliver
 */
public class CETypeData {
    private ResultSet resultSet = null;
    
    public CETypeData() {}
    
    public ArrayList<CEType> getExperienceCETypes(int idExperience) throws SQLException {
        ArrayList<CEType> CETypes = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT CEType.ID as 'ID', CEType.name as 'name', CEType.approved as 'approved' "
                + "FROM CEType INNER JOIN culturalActivity on CEType.ID = culturalActivity.type "
                + "WHERE culturalActivity.experience = '" + idExperience + "';");
        while (resultSet.next()) {
            CEType object = new CEType();
            object.setID(resultSet.getInt("ID"));
            object.setName(resultSet.getString("name"));
            object.setApproved(resultSet.getInt("approved"));
            CETypes.add(object);
        }
        return CETypes;
    }
}
