/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Achievement;
import utilities.Connection;

/**
 *
 * @author Oliver
 */
public class AchievementData {
    private ResultSet resultSet = null;
    
    public AchievementData(){}
    
    public ArrayList<Achievement> getExperienceAchievements(int experience) throws SQLException {
        ArrayList<Achievement> achievements = new ArrayList<>();
        resultSet = Connection.doQuery("SELECT * FROM Achievement WHERE experience = '" + experience + "';");
        while (resultSet.next()){
            Achievement achievement = new Achievement(
                resultSet.getInt("ID"),
                resultSet.getString("date"),
                resultSet.getInt("approved")
            );
            achievement.setDescription(resultSet.getString("description"));
            achievement.setAttachmentUri(resultSet.getString("attachmentUri"));
            achievements.add(achievement);
        }
        return achievements;
    }
}
