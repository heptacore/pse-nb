/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pse.api;

import data.PersonData;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Person;

/**
 *
 * @author Oliver
 */
public class PersonApi extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Person</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Person at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /*@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            PersonData data = new PersonData();
            data.readData();
            int rows = data.getPeople().size();
            PrintWriter out = response.getWriter();
            out.write("[");
            for (int i = 0; i < rows; i++) {
                out.write("{");
                out.write("'ID': " + data.getPeople().get(i).getID() + ", ");
                out.write("'firstName': " + '"' + data.getPeople().get(i).getFirstName() + '"' + ", ");
                out.write("'lastName': " + '"' + data.getPeople().get(i).getLastName() + '"' + ", ");
                out.write("'surname': " + '"' + data.getPeople().get(i).getSurname() + '"' + ", ");
                out.write("'lastSurname': " + '"' + data.getPeople().get(i).getLastSurname() + '"' + ", ");
                out.write("'gender': " + '"' + data.getPeople().get(i).getGender() + '"' + ", ");
                out.write("'idCardNumber': " + '"' + data.getPeople().get(i).getIdCardNumber() + '"');
                out.write("}");
                if (i < (rows - 1)) {
                    out.write(", ");
                }
            }
            out.write("]");
        } catch (Exception e) {
            System.out.println("Ha ocurrido un error en com.pse.api.PersonApi.java: " + e);
        }
    }*/

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
