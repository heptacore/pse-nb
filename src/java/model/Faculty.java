/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Oliver
 */
public class Faculty {
    private int ID;
    private String name;
    private ArrayList<Career> careers = new ArrayList<Career>();

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Career> getCareers() {
        return careers;
    }

    public void setCareers(ArrayList<Career> careers) {
        this.careers = careers;
    }
    
    
    
}
