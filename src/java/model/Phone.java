package model;

public class Phone implements Entity {

    //	Attributes
    private String number;
    private String tag;
    private int owner;

    //	Methods
    public Phone(String number) {
        this.number = number;
    }

    public Phone(String number, int owner) {
        this.number = number;
        this.owner = owner;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }
    
    

}
