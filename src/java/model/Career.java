/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Oliver
 */
public class Career implements Entity {
    private int ID;
    private String name;
    private Person coordinator;
    private String cohort;
    private boolean finished;
    private boolean approved;
    
    public Career(int ID) {
        this.ID = ID;
    }

    public Career(String name) {
        this.name = name;
    }
    
    public Career(String name, String cohort) {
        this.name = name;
        this.cohort = cohort;
    }
    
    public Career(int ID, String cohort) {
        this.ID = ID;
        this.cohort = cohort;
    }
    
    

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person getCoordinator() {
        return coordinator;
    }

    public void setCoordinator(Person coordinator) {
        this.coordinator = coordinator;
    }

    public String getCohort() {
        return cohort;
    }

    public void setCohort(String cohort) {
        this.cohort = cohort;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
