/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.StringWriter;
import javax.json.Json;
import javax.json.JsonObject;
import model.User;

/**
 *
 * @author Oliver
 */
public class Message implements Entity {

    private String date;
    private User senderUser;
    private User receiverUser;
    private int deleted;
    private String value;
    private String attached;
    private JsonObject json;
    private int seen;

    public Message(User senderUser, User receiverUser, String value, String date) {
        this.senderUser = senderUser;
        this.receiverUser = receiverUser;
        this.value = value;
        this.date = date;
    }

    public int isSeen() {
        return seen;
    }

    public void setSeen(int seen) {
        this.seen = seen;
    }
    
    
    
    public Message(JsonObject json) {
        System.out.println("Ha entrado al constructor JSON");
        this.json = json;
        this.value = json.getString("value");
    }
    
    @Override
    public String toString() {
        StringWriter writer = new StringWriter();
        Json.createWriter(writer).write(json);
        return writer.toString();
    }

    public JsonObject getJson() {
        return json;
    }

    public void setJson(JsonObject json) {
        this.json = json;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int isDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAttached() {
        return attached;
    }

    public void setAttached(String attached) {
        this.attached = attached;
    }

    public User getSenderUser() {
        return senderUser;
    }

    public void setSenderUser(User senderUser) {
        this.senderUser = senderUser;
    }

    public User getReceiverUser() {
        return receiverUser;
    }

    public void setReceiverUser(User receiverUser) {
        this.receiverUser = receiverUser;
    }
}
