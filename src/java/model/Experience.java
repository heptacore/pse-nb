/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Oliver
 */
public class Experience {
    private int ID;
    private String name;
    private String description;
    private int hasInterestGroups;
    private int hasUniversities;
    private int hasCultures;
    private int hasSports;
    private int hasOrganizations;
    private int isProyect;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getHasInterestGroups() {
        return hasInterestGroups;
    }

    public void setHasInterestGroups(int hasInterestGroups) {
        this.hasInterestGroups = hasInterestGroups;
    }

    public int getHasUniversities() {
        return hasUniversities;
    }

    public void setHasUniversities(int hasUniversities) {
        this.hasUniversities = hasUniversities;
    }

    public int getHasCultures() {
        return hasCultures;
    }

    public void setHasCultures(int hasCultures) {
        this.hasCultures = hasCultures;
    }

    public int getHasSports() {
        return hasSports;
    }

    public void setHasSports(int hasSports) {
        this.hasSports = hasSports;
    }

    public int getHasOrganizations() {
        return hasOrganizations;
    }

    public void setHasOrganizations(int hasOrganizations) {
        this.hasOrganizations = hasOrganizations;
    }

    public int getIsProyect() {
        return isProyect;
    }

    public void setIsProyect(int isProyect) {
        this.isProyect = isProyect;
    }
    
    
}
