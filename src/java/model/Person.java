package model;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * This class represents a Person in the ERD Diagram. It's methods are not
 * abstract for they define an implementation which is not going to be
 * overwritten.
 *
 * @author Oliver
 * @version 1.0
 */
public class Person implements Entity {
    //	Attributes
    protected int ID;
    protected String idCardNumber;
    protected String firstName;
    protected String lastName;
    protected String surname;
    protected String lastSurname;
    protected String gender;
    private String contactRole;
    protected ArrayList<Phone> phones = new ArrayList<>();
    protected ArrayList<Email> emailAddresses = new ArrayList<>();
    protected User user;
    
    public Person(){}

    public Person(int IDPerson, String firstName, String surname) {
        this.ID = IDPerson;
        this.firstName = firstName;
        this.surname = surname;
    }

    public String getContactRole() {
        return contactRole;
    }

    public void setContactRole(String contactRole) {
        this.contactRole = contactRole;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLastSurname() {
        return lastSurname;
    }
    
    public String getName() throws NullPointerException {
        String name = this.firstName;
        //  [PROCESS][1]: Validate fields
        //  Fields are not null, empty or "null"
        if (!this.lastName.isEmpty()){
            name = name + " " + this.lastName;
        }
        if (!this.surname.isEmpty()){
            name = name + " " + this.surname;
        }
        if (!this.lastSurname.isEmpty()){
            name = name + " " + this.lastSurname;
        }
        return name;
    }

    public void setLastSurname(String lastSurname) {
        this.lastSurname = lastSurname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ArrayList<Phone> getPhones() {
        return phones;
    }

    public void setPhones(ArrayList<Phone> phones) {
        this.phones = phones;
    }

    public ArrayList<Email> getEmailAddresses() {
        return emailAddresses;
    }

    public void setEmailAddresses(ArrayList<Email> emailAddresses) {
        this.emailAddresses = emailAddresses;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    
}
