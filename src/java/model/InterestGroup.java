/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Oliver
 */
public class InterestGroup {
    private int ID;
    private String name;
    private int approved;
    private String description;
    private ArrayList<GroupRole> groupRoles = new ArrayList<>();
    private String experienceRole;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getExperienceRole() {
        return experienceRole;
    }

    public void setExperienceRole(String experienceRole) {
        this.experienceRole = experienceRole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int isApproved() {
        return approved;
    }

    public void setApproved(int approved) {
        this.approved = approved;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<GroupRole> getGroupRoles() {
        return groupRoles;
    }

    public void setGroupRoles(ArrayList<GroupRole> groupRoles) {
        this.groupRoles = groupRoles;
    }
}
