package model;

import java.util.ArrayList;

public class Student extends Person implements Entity {
    //	Attributes
    private int idStudent;	//	Unique DataBase value.
    private String studentIdNumber;
    private String exStudentIdNumber;
    private ArrayList<Career> careers = new ArrayList<>();

    //	Methods

    public Student(int idStudent, int idPerson, String firstName, String surname) {
        super(idPerson, firstName, surname);
        this.idStudent = idStudent;
    }
    
    public Student () {};
    
    public Student (Person person) {
        super.ID = person.ID;
        super.emailAddresses = person.emailAddresses;
        super.firstName = person.firstName;
        super.lastName = person.lastName;
        super.surname = person.surname;
        super.lastSurname = person.lastSurname;
        super.idCardNumber = person.idCardNumber;
        super.gender = person.gender;
    }
    
    
    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public String getStudentIdNumber() {
        return studentIdNumber;
    }

    public void setStudentIdNumber(String studentIdNumber) {
        this.studentIdNumber = studentIdNumber;
    }

    public String getExStudentIdNumber() {
        return exStudentIdNumber;
    }

    public void setExStudentIdNumber(String exStudentIdNumber) {
        this.exStudentIdNumber = exStudentIdNumber;
    }

    public ArrayList<Career> getCareers() {
        return careers;
    }

    public void setCareers(ArrayList<Career> careers) {
        this.careers = careers;
    }

}
