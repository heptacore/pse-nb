package model;

public class Email implements Entity {
    //	Attributes

    private String address;
    private String tag;
    private int owner;

    //	Methods
    public Email(String address) {
        this.address = address;
    }

    public Email(String address, int owner) {
        this.address = address;
        this.owner = owner;
    }

    public Email(String address, String tag, int owner) {
        this.address = address;
        this.tag = tag;
        this.owner = owner;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }
    
    
}
