/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Oliver
 */
public class University {
    private int ID;
    private String name;
    private int isJS;
    private int approved;
    private String country;
    private String experienceRole;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExperienceRole() {
        return experienceRole;
    }

    public void setExperienceRole(String experienceRole) {
        this.experienceRole = experienceRole;
    }

    public int isJS() {
        return isJS;
    }

    public void setJS(int isJS) {
        this.isJS = isJS;
    }

    public int isApproved() {
        return approved;
    }

    public void setApproved(int approved) {
        this.approved = approved;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    
}
