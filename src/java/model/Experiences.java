/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Oliver
 */
public class Experiences implements Entity {

    private int ID;
    private ArrayList<Achievement> achievements = new ArrayList<>();
    private String date;
    private int approved;
    private Experience name;
    private String description;
    private ArrayList<Organization> organizations = new ArrayList<>();
    private ArrayList<InterestGroup> interestGroups = new ArrayList<>();
    private GroupRole groupRole;
    private ArrayList<Sport> sport = new ArrayList<>();
    private ArrayList<Culture> culture = new ArrayList<>();
    private StudentJob studentJob;
    private ArrayList<University> university;
    private String proyectTitle;
    private ArrayList<Person> contacts = new ArrayList<>();

    public Experiences(int ID, String date, int approved, Experience name) {
        this.ID = ID;
        this.date = date;
        this.approved = approved;
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public ArrayList<Achievement> getAchievements() {
        return achievements;
    }

    public void setAchievements(ArrayList<Achievement> achievements) {
        this.achievements = achievements;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int isApproved() {
        return approved;
    }

    public void setApproved(int approved) {
        this.approved = approved;
    }

    public Experience getName() {
        return name;
    }

    public void setName(Experience name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Organization> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(ArrayList<Organization> organizations) {
        this.organizations = organizations;
    }

    public ArrayList<InterestGroup> getInterestGroups() {
        return interestGroups;
    }

    public void setInterestGroups(ArrayList<InterestGroup> interestGroups) {
        this.interestGroups = interestGroups;
    }

    public GroupRole getGroupRole() {
        return groupRole;
    }

    public void setGroupRole(GroupRole groupRole) {
        this.groupRole = groupRole;
    }

    public ArrayList<Sport> getSport() {
        return sport;
    }

    public void setSport(ArrayList<Sport> sport) {
        this.sport = sport;
    }

    public ArrayList<Culture> getCulture() {
        return culture;
    }

    public void setCulture(ArrayList<Culture> culture) {
        this.culture = culture;
    }

    public StudentJob getStudentJob() {
        return studentJob;
    }

    public void setStudentJob(StudentJob studentJob) {
        this.studentJob = studentJob;
    }

    public ArrayList<University> getUniverities() {
        return university;
    }

    public void setUniversities(ArrayList<University> university) {
        this.university = university;
    }

    public String getProyectTitle() {
        return proyectTitle;
    }

    public void setProyectTitle(String proyectTitle) {
        this.proyectTitle = proyectTitle;
    }

    public ArrayList<Person> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Person> contacts) {
        this.contacts = contacts;
    }
}
