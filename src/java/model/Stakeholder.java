/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Oliver
 */
public class Stakeholder extends Person implements Entity {

    private int id;
    private Career coordination = null;
    private ArrayList<Job> jobs = new ArrayList<>();

    public Stakeholder(int idPerson, int id, ArrayList<Job> jobs, String firstName, String surname) {
        super(idPerson, firstName, surname);
        this.id = id;
        this.jobs = jobs;
    }
    
    public Stakeholder(){};
    
    public Stakeholder(Person person) {
        super.ID = person.ID;
        super.emailAddresses = person.emailAddresses;
        super.firstName = person.firstName;
        super.lastName = person.lastName;
        super.surname = person.surname;
        super.lastSurname = person.lastSurname;
        super.idCardNumber = person.idCardNumber;
        super.gender = person.gender;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Career getCoordination() {
        return coordination;
    }

    public void setCoordination(Career coordination) {
        this.coordination = coordination;
    }

    public ArrayList<Job> getJobs() {
        return jobs;
    }

    public void setJobs(ArrayList<Job> jobs) {
        this.jobs = jobs;
    }

    
}
