/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Oliver
 */
public class Sport {
    private int ID;
    private int approved;
    private String name;
    private ArrayList<SEType> activities = new ArrayList<>();

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int isApproved() {
        return approved;
    }

    public void setApproved(int approved) {
        this.approved = approved;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<SEType> getActivities() {
        return activities;
    }

    public void setActivities(ArrayList<SEType> activities) {
        this.activities = activities;
    }
}
