import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import data.UserData;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import model.User;

/**
 *
 * @author Oliver
 */
@WebServlet(urlPatterns = {"/login"})
public class login extends HttpServlet {
    
    //  Attributes
    private UserData userData;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserData userData = new UserData();
        String userName = request.getParameter("user");
        String password = request.getParameter("password");
        PrintWriter out = response.getWriter();
        
        //  Form fields were filled.
        if (userName != null && password != null) {
            User user = null;

            try {
                user = userData.readData(userName, password);
            } catch (SQLException ex) {
                System.out.println("An error has ocurred: " + ex);
            }
            
            //  User was found.
            if (null != user) {
                HttpSession session = request.getSession();
                session.setAttribute("user", user);
                out.write("1");
            } else {
                out.write("0");
            }
        } else {
            out.write("-1");
        }
    }
}
