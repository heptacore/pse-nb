/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import data.MessageData;
import data.UserData;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Message;

/**
 *
 * @author Oliver
 */
@WebServlet(urlPatterns = {"/conversation"})
public class conversation extends HttpServlet {

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String myUserNick, user2Nick = null;
        PrintWriter out = response.getWriter();
        MessageData messageData = new MessageData();
        ArrayList<Message> messages = new ArrayList<>();
        
        myUserNick = request.getAttribute("myUser").toString();
        user2Nick = request.getAttribute("user2").toString();
        
        try {
            messages = messageData.getMessages(myUserNick, user2Nick, 0);
            for (int i = 0; i < messages.size(); i++) {
                out.println("<p>" + messages.get(i).getSenderUser() + ": " + messages.get(i).getValue() + "</p>");
            }
        } catch (SQLException e) {
            System.out.println("Conversation-Servlet [ERROR]: " + e);
            out.println("<p>Ha occurrido un error.</p>");
        }
    }


}
