<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <!DOCTYPE html>
    <html class="loading" lang="en" data-textdirection="ltr">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template.">
        <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
        <meta name="author" content="PIXINVENT">
        <title>Entorno de Seguimiento Académico</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/logo/logo.ico">
        <link href="app-assets/fonts/font-awesome-4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
        <!-- END VENDOR CSS-->
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="app-assets/fonts/icomoon.css">
        <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
        <!-- BEGIN ROBUST CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
        <!-- END ROBUST CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <!-- END Custom CSS-->
    </head>

    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu"
        data-col="2-columns">

        <!-- fixed-top-->
        <!-- fixed-top-->
        <nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-semi-dark navbar-shadow">
            <div class="navbar-wrapper">
                <div class="navbar-header">
                    <ul class="nav navbar-nav flex-row">
                        <li class="nav-item mobile-menu d-md-none mr-auto">
                            <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                                <i class="ft-menu font-large-1"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="navbar-brand" href="dashboard.jsp">
                                <img class="brand-logo" alt="pse admin logo" src="app-assets/images/logo/robust-logo-light.png">
                            </a>
                        </li>
                        <li class="nav-item d-md-none">
                            <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile">
                                <i class="fa fa-ellipsis-v"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="navbar-container content">
                    <div class="collapse navbar-collapse" id="navbar-mobile">
                        <ul class="nav navbar-nav mr-auto float-left">
                            <li class="nav-item d-none d-md-block">
                                <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                                    <i class="ft-menu"> </i>
                                </a>
                            </li>
                            <li class="nav-item d-none d-md-block">
                                <a class="nav-link nav-link-expand" href="#">
                                    <i class="ficon ft-maximize"></i>
                                </a>
                            </li>
                            <li class="nav-item nav-search">
                                <a class="nav-link nav-link-search" href="#">
                                    <i class="ficon ft-search"></i>
                                </a>
                                <div class="search-input">
                                    <input class="input" type="text" placeholder="Buscar...">
                                </div>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav float-right">
                            <li class="dropdown dropdown-notification nav-item">
                                <a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                                    <i class="ficon ft-bell"></i>
                                    <span class="badge badge-pill badge-default badge-danger badge-default badge-up">5</span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                    <li class="dropdown-menu-header">
                                        <h6 class="dropdown-header m-0">
                                            <span class="grey darken-2">Notificaciones</span>
                                        </h6>
                                        <span class="notification-tag badge badge-default badge-danger float-right m-0">5 Nuevas</span>
                                    </li>
                                    <li class="scrollable-container media-list w-100">
                                        <a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left align-self-center">
                                                    <i class="ft-plus-square icon-bg-circle bg-cyan"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">You have new order!</h6>
                                                    <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hace 30 minutos</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left align-self-center">
                                                    <i class="ft-download-cloud icon-bg-circle bg-red bg-darken-1"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading red darken-1">99% Server load</h6>
                                                    <p class="notification-text font-small-3 text-muted">Aliquam tincidunt mauris eu risus.</p>
                                                    <small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hace 5 horas</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left align-self-center">
                                                    <i class="ft-alert-triangle icon-bg-circle bg-yellow bg-darken-3"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading yellow darken-3">Warning notifixation</h6>
                                                    <p class="notification-text font-small-3 text-muted">Vestibulum auctor dapibus neque.</p>
                                                    <small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hoy</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left align-self-center">
                                                    <i class="ft-check-circle icon-bg-circle bg-cyan"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Complete the task</h6>
                                                    <small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hace una semana</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left align-self-center">
                                                    <i class="ft-file icon-bg-circle bg-teal"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Generate monthly report</h6>
                                                    <small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hace un mes</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="dropdown-menu-footer">
                                        <a class="dropdown-item text-muted text-center" href="javascript:void(0)">Ver Todas</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown dropdown-notification nav-item">
                                <a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                                    <i class="ficon ft-mail"></i>
                                    <span class="badge badge-pill badge-default badge-info badge-default badge-up">4 </span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                    <li class="dropdown-menu-header">
                                        <h6 class="dropdown-header m-0">
                                            <span class="grey darken-2">Mensajes</span>
                                        </h6>
                                        <span class="notification-tag badge badge-default badge-warning float-right m-0">4 Nuevos</span>
                                    </li>
                                    <li class="scrollable-container media-list w-100">
                                        <a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left">
                                                    <span class="avatar avatar-sm avatar-online rounded-circle">
                                                        <img src="app-assets/images/portrait/small/avatar-s-19.png" alt="avatar">
                                                        <i></i>
                                                    </span>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Margaret Govan</h6>
                                                    <p class="notification-text font-small-3 text-muted">I like your portfolio, let's start.</p>
                                                    <small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hoy</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left">
                                                    <span class="avatar avatar-sm avatar-busy rounded-circle">
                                                        <img src="app-assets/images/portrait/small/avatar-s-2.png" alt="avatar">
                                                        <i></i>
                                                    </span>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Bret Lezama</h6>
                                                    <p class="notification-text font-small-3 text-muted">I have seen your work, there is</p>
                                                    <small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Martes</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left">
                                                    <span class="avatar avatar-sm avatar-online rounded-circle">
                                                        <img src="app-assets/images/portrait/small/avatar-s-3.png" alt="avatar">
                                                        <i></i>
                                                    </span>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Carie Berra</h6>
                                                    <p class="notification-text font-small-3 text-muted">Can we have call in this week ?</p>
                                                    <small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Viernes</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="javascript:void(0)">
                                            <div class="media">
                                                <div class="media-left">
                                                    <span class="avatar avatar-sm avatar-away rounded-circle">
                                                        <img src="app-assets/images/portrait/small/avatar-s-6.png" alt="avatar">
                                                        <i></i>
                                                    </span>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">Eric Alsobrook</h6>
                                                    <p class="notification-text font-small-3 text-muted">We have project party this saturday.</p>
                                                    <small>
                                                        <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hace un mes</time>
                                                    </small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="dropdown-menu-footer">
                                        <a class="dropdown-item text-muted text-center" href="chat.jsp">Ver todos</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown dropdown-user nav-item">
                                <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                    <span class="avatar avatar-online">
                                        <img src="app-assets/images/portrait/small/avatar-s-1.png" alt="avatar">
                                        <i></i>
                                    </span>
                                    <span class="user-name">Armando López</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="user-profile.jsp">
                                        <i class="ft-user"></i> Perfil</a>
                                    <a class="dropdown-item" href="chat.jsp">
                                        <i class="ft-mail"></i> Inbox</a>
                                    <a class="dropdown-item" href="#">
                                        <i class="ft-check-square"></i> Tareas</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="index.jsp">
                                        <i class="ft-power"></i> Cerrar Sesión</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <!-- main menu-->
        <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
            <!-- main menu header-->
            <div class="main-menu-header">
                <input type="text" placeholder="Buscar" class="menu-search form-control round" />
            </div>
            <!-- / main menu header-->
            <!-- main menu content-->
            <div class="main-menu-content">
                <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
                    <li class=" nav-item">
                        <a href="dashboard.jsp">
                            <i class="icon-home3"></i>
                            <span data-i18n="nav.dash.main" class="menu-title">Inicio</span>
                        </a>
                    </li>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-user-tie"></i>
                            <span data-i18n="nav.project.main" class="menu-title">Colaboradores</span>
                        </a>
                        <ul class="menu-content">
                            <li>
                                <a href="add-employee.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Colaborador</a>
                            </li>
                            <li>
                                <a href="employee.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Colaboradores</a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-group"></i>
                            <span data-i18n="nav.project.main" class="menu-title">Estudiantes</span>
                        </a>
                        <ul class="menu-content">
                            <li>
                                <a href="add-student.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Estudiante</a>
                            </li>
                            <li>
                                <a href="student.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Estudiantes</a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-ios-albums-outline"></i>
                            <span data-i18n="nav.cards.main" class="menu-title">Carreras</span>
                        </a>
                        <ul class="menu-content">
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Carrera</a>
                            </li>
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Carreras</a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-university2"></i>
                            <span data-i18n="nav.advance_cards.main" class="menu-title">Facultades</span>
                        </a>
                        <ul class="menu-content">
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Facultad</a>
                            </li>
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Facultades</a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-star6"></i>
                            <span data-i18n="nav.content.main" class="menu-title">Experiencias</span>
                        </a>
                        <ul class="menu-content">
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Experiencia</a>
                            </li>
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Experiencias</a>
                            </li>
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Ver Solicitudes</a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-list-alt"></i>
                            <span data-i18n="nav.components.main" class="menu-title">Encuestas</span>
                        </a>
                        <ul class="menu-content">
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Crear Encuesta</a>
                            </li>
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Encuestas</a>
                            </li>
                        </ul>
                    </li>
                    <li class="navigation-header">
                        <span data-i18n="nav.category.support">Ajustes</span>
                        <i data-toggle="tooltip" data-placement="right" data-original-title="Ajustes"></i>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-cog2"></i>
                            <span data-i18n="nav.components.main" class="menu-title">Seguridad</span>
                        </a>
                        <ul class="menu-content">
                            <li class=" nav-item">
                                <a href="#">
                                    <span data-i18n="nav.components.main" class="menu-title">Usuarios</span>
                                </a>
                                <ul class="menu-content">
                                    <li>
                                        <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Registrar Usuario</a>
                                    </li>
                                    <li>
                                        <a href="user.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Usuarios</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="jobs.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Cargos</a>
                            </li>
                            <li>
                                <a href="permissions.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Permisos</a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-file-text3"></i>
                            <span data-i18n="nav.support_documentation.main" class="menu-title">Documentación</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /main menu content-->
            <!-- main menu footer-->
            <!-- include includes/menu-footer-->
            <!-- main menu footer-->
        </div>
        <!-- / main menu-->

        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row">
                    <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                        <h3 class="content-header-title mb-0 d-inline-block">Colaboradores</h3>
                        <div class="row breadcrumbs-top d-inline-block">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="dashboard.jsp">Inicio</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="employee.jsp">Gestionar Colaboradores</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="content-header-right col-md-4 col-12">
                        <div class="btn-group float-md-right">
                            <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="icon-settings mr-1"></i>Opciones</button>
                            <div class="dropdown-menu arrow">
                                <a class="dropdown-item" href="#">
                                    <i class="icon-calendar-plus-o mr-1"></i> Calendario</a>
                                <a class="dropdown-item" href="#">
                                    <i class="icon-wrench4 mr-1"></i> Soporte</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">
                                    <i class="icon-file-text3 mr-1"></i> Documentación</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-body">
                    <!-- Column selectors table -->
                    <section id="column-selectors">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Todos los Colaboradores</h3>
                                        <a class="heading-elements-toggle">
                                            <i class="fa fa-ellipsis-v font-medium-3"></i>
                                        </a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li>
                                                    <a data-action="collapse">
                                                        <i class="ft-minus"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-action="reload">
                                                        <i class="ft-rotate-cw"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-action="expand">
                                                        <i class="ft-maximize"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-selectors">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Position</th>
                                                        <th>Office</th>
                                                        <th>Age</th>
                                                        <th>Start date</th>
                                                        <th>Salary</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Tiger Nixon</td>
                                                        <td>System Architect</td>
                                                        <td>Edinburgh</td>
                                                        <td>61</td>
                                                        <td>2011/04/25</td>
                                                        <td>$320,800</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Garrett Winters</td>
                                                        <td>Accountant</td>
                                                        <td>Tokyo</td>
                                                        <td>63</td>
                                                        <td>2011/07/25</td>
                                                        <td>$170,750</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ashton Cox</td>
                                                        <td>Junior Technical Author</td>
                                                        <td>San Francisco</td>
                                                        <td>66</td>
                                                        <td>2009/01/12</td>
                                                        <td>$86,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Cedric Kelly</td>
                                                        <td>Senior Javascript Developer</td>
                                                        <td>Edinburgh</td>
                                                        <td>22</td>
                                                        <td>2012/03/29</td>
                                                        <td>$433,060</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Airi Satou</td>
                                                        <td>Accountant</td>
                                                        <td>Tokyo</td>
                                                        <td>33</td>
                                                        <td>2008/11/28</td>
                                                        <td>$162,700</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Brielle Williamson</td>
                                                        <td>Integration Specialist</td>
                                                        <td>New York</td>
                                                        <td>61</td>
                                                        <td>2012/12/02</td>
                                                        <td>$372,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Herrod Chandler</td>
                                                        <td>Sales Assistant</td>
                                                        <td>San Francisco</td>
                                                        <td>59</td>
                                                        <td>2012/08/06</td>
                                                        <td>$137,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Rhona Davidson</td>
                                                        <td>Integration Specialist</td>
                                                        <td>Tokyo</td>
                                                        <td>55</td>
                                                        <td>2010/10/14</td>
                                                        <td>$327,900</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Colleen Hurst</td>
                                                        <td>Javascript Developer</td>
                                                        <td>San Francisco</td>
                                                        <td>39</td>
                                                        <td>2009/09/15</td>
                                                        <td>$205,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sonya Frost</td>
                                                        <td>Software Engineer</td>
                                                        <td>Edinburgh</td>
                                                        <td>23</td>
                                                        <td>2008/12/13</td>
                                                        <td>$103,600</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jena Gaines</td>
                                                        <td>Office Manager</td>
                                                        <td>London</td>
                                                        <td>30</td>
                                                        <td>2008/12/19</td>
                                                        <td>$90,560</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Quinn Flynn</td>
                                                        <td>Support Lead</td>
                                                        <td>Edinburgh</td>
                                                        <td>22</td>
                                                        <td>2013/03/03</td>
                                                        <td>$342,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Charde Marshall</td>
                                                        <td>Regional Director</td>
                                                        <td>San Francisco</td>
                                                        <td>36</td>
                                                        <td>2008/10/16</td>
                                                        <td>$470,600</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Haley Kennedy</td>
                                                        <td>Senior Marketing Designer</td>
                                                        <td>London</td>
                                                        <td>43</td>
                                                        <td>2012/12/18</td>
                                                        <td>$313,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tatyana Fitzpatrick</td>
                                                        <td>Regional Director</td>
                                                        <td>London</td>
                                                        <td>19</td>
                                                        <td>2010/03/17</td>
                                                        <td>$385,750</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Michael Silva</td>
                                                        <td>Marketing Designer</td>
                                                        <td>London</td>
                                                        <td>66</td>
                                                        <td>2012/11/27</td>
                                                        <td>$198,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Paul Byrd</td>
                                                        <td>Chief Financial Officer (CFO)</td>
                                                        <td>New York</td>
                                                        <td>64</td>
                                                        <td>2010/06/09</td>
                                                        <td>$725,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gloria Little</td>
                                                        <td>Systems Administrator</td>
                                                        <td>New York</td>
                                                        <td>59</td>
                                                        <td>2009/04/10</td>
                                                        <td>$237,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bradley Greer</td>
                                                        <td>Software Engineer</td>
                                                        <td>London</td>
                                                        <td>41</td>
                                                        <td>2012/10/13</td>
                                                        <td>$132,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Dai Rios</td>
                                                        <td>Personnel Lead</td>
                                                        <td>Edinburgh</td>
                                                        <td>35</td>
                                                        <td>2012/09/26</td>
                                                        <td>$217,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jenette Caldwell</td>
                                                        <td>Development Lead</td>
                                                        <td>New York</td>
                                                        <td>30</td>
                                                        <td>2011/09/03</td>
                                                        <td>$345,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Yuri Berry</td>
                                                        <td>Chief Marketing Officer (CMO)</td>
                                                        <td>New York</td>
                                                        <td>40</td>
                                                        <td>2009/06/25</td>
                                                        <td>$675,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Caesar Vance</td>
                                                        <td>Pre-Sales Support</td>
                                                        <td>New York</td>
                                                        <td>21</td>
                                                        <td>2011/12/12</td>
                                                        <td>$106,450</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Doris Wilder</td>
                                                        <td>Sales Assistant</td>
                                                        <td>Sidney</td>
                                                        <td>23</td>
                                                        <td>2010/09/20</td>
                                                        <td>$85,600</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Angelica Ramos</td>
                                                        <td>Chief Executive Officer (CEO)</td>
                                                        <td>London</td>
                                                        <td>47</td>
                                                        <td>2009/10/09</td>
                                                        <td>$1,200,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gavin Joyce</td>
                                                        <td>Developer</td>
                                                        <td>Edinburgh</td>
                                                        <td>42</td>
                                                        <td>2010/12/22</td>
                                                        <td>$92,575</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jennifer Chang</td>
                                                        <td>Regional Director</td>
                                                        <td>Singapore</td>
                                                        <td>28</td>
                                                        <td>2010/11/14</td>
                                                        <td>$357,650</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Brenden Wagner</td>
                                                        <td>Software Engineer</td>
                                                        <td>San Francisco</td>
                                                        <td>28</td>
                                                        <td>2011/06/07</td>
                                                        <td>$206,850</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fiona Green</td>
                                                        <td>Chief Operating Officer (COO)</td>
                                                        <td>San Francisco</td>
                                                        <td>48</td>
                                                        <td>2010/03/11</td>
                                                        <td>$850,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shou Itou</td>
                                                        <td>Regional Marketing</td>
                                                        <td>Tokyo</td>
                                                        <td>20</td>
                                                        <td>2011/08/14</td>
                                                        <td>$163,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Michelle House</td>
                                                        <td>Integration Specialist</td>
                                                        <td>Sidney</td>
                                                        <td>37</td>
                                                        <td>2011/06/02</td>
                                                        <td>$95,400</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Suki Burks</td>
                                                        <td>Developer</td>
                                                        <td>London</td>
                                                        <td>53</td>
                                                        <td>2009/10/22</td>
                                                        <td>$114,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Prescott Bartlett</td>
                                                        <td>Technical Author</td>
                                                        <td>London</td>
                                                        <td>27</td>
                                                        <td>2011/05/07</td>
                                                        <td>$145,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gavin Cortez</td>
                                                        <td>Team Leader</td>
                                                        <td>San Francisco</td>
                                                        <td>22</td>
                                                        <td>2008/10/26</td>
                                                        <td>$235,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Martena Mccray</td>
                                                        <td>Post-Sales support</td>
                                                        <td>Edinburgh</td>
                                                        <td>46</td>
                                                        <td>2011/03/09</td>
                                                        <td>$324,050</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Unity Butler</td>
                                                        <td>Marketing Designer</td>
                                                        <td>San Francisco</td>
                                                        <td>47</td>
                                                        <td>2009/12/09</td>
                                                        <td>$85,675</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Howard Hatfield</td>
                                                        <td>Office Manager</td>
                                                        <td>San Francisco</td>
                                                        <td>51</td>
                                                        <td>2008/12/16</td>
                                                        <td>$164,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Hope Fuentes</td>
                                                        <td>Secretary</td>
                                                        <td>San Francisco</td>
                                                        <td>41</td>
                                                        <td>2010/02/12</td>
                                                        <td>$109,850</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Vivian Harrell</td>
                                                        <td>Financial Controller</td>
                                                        <td>San Francisco</td>
                                                        <td>62</td>
                                                        <td>2009/02/14</td>
                                                        <td>$452,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Timothy Mooney</td>
                                                        <td>Office Manager</td>
                                                        <td>London</td>
                                                        <td>37</td>
                                                        <td>2008/12/11</td>
                                                        <td>$136,200</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jackson Bradshaw</td>
                                                        <td>Director</td>
                                                        <td>New York</td>
                                                        <td>65</td>
                                                        <td>2008/09/26</td>
                                                        <td>$645,750</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Olivia Liang</td>
                                                        <td>Support Engineer</td>
                                                        <td>Singapore</td>
                                                        <td>64</td>
                                                        <td>2011/02/03</td>
                                                        <td>$234,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bruno Nash</td>
                                                        <td>Software Engineer</td>
                                                        <td>London</td>
                                                        <td>38</td>
                                                        <td>2011/05/03</td>
                                                        <td>$163,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sakura Yamamoto</td>
                                                        <td>Support Engineer</td>
                                                        <td>Tokyo</td>
                                                        <td>37</td>
                                                        <td>2009/08/19</td>
                                                        <td>$139,575</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Thor Walton</td>
                                                        <td>Developer</td>
                                                        <td>New York</td>
                                                        <td>61</td>
                                                        <td>2013/08/11</td>
                                                        <td>$98,540</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Finn Camacho</td>
                                                        <td>Support Engineer</td>
                                                        <td>San Francisco</td>
                                                        <td>47</td>
                                                        <td>2009/07/07</td>
                                                        <td>$87,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Serge Baldwin</td>
                                                        <td>Data Coordinator</td>
                                                        <td>Singapore</td>
                                                        <td>64</td>
                                                        <td>2012/04/09</td>
                                                        <td>$138,575</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Zenaida Frank</td>
                                                        <td>Software Engineer</td>
                                                        <td>New York</td>
                                                        <td>63</td>
                                                        <td>2010/01/04</td>
                                                        <td>$125,250</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Zorita Serrano</td>
                                                        <td>Software Engineer</td>
                                                        <td>San Francisco</td>
                                                        <td>56</td>
                                                        <td>2012/06/01</td>
                                                        <td>$115,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jennifer Acosta</td>
                                                        <td>Junior Javascript Developer</td>
                                                        <td>Edinburgh</td>
                                                        <td>43</td>
                                                        <td>2013/02/01</td>
                                                        <td>$75,650</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Cara Stevens</td>
                                                        <td>Sales Assistant</td>
                                                        <td>New York</td>
                                                        <td>46</td>
                                                        <td>2011/12/06</td>
                                                        <td>$145,600</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Hermione Butler</td>
                                                        <td>Regional Director</td>
                                                        <td>London</td>
                                                        <td>47</td>
                                                        <td>2011/03/21</td>
                                                        <td>$356,250</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lael Greer</td>
                                                        <td>Systems Administrator</td>
                                                        <td>London</td>
                                                        <td>21</td>
                                                        <td>2009/02/27</td>
                                                        <td>$103,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jonas Alexander</td>
                                                        <td>Developer</td>
                                                        <td>San Francisco</td>
                                                        <td>30</td>
                                                        <td>2010/07/14</td>
                                                        <td>$86,500</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shad Decker</td>
                                                        <td>Regional Director</td>
                                                        <td>Edinburgh</td>
                                                        <td>51</td>
                                                        <td>2008/11/13</td>
                                                        <td>$183,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Michael Bruce</td>
                                                        <td>Javascript Developer</td>
                                                        <td>Singapore</td>
                                                        <td>29</td>
                                                        <td>2011/06/27</td>
                                                        <td>$183,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Donna Snider</td>
                                                        <td>Customer Support</td>
                                                        <td>New York</td>
                                                        <td>27</td>
                                                        <td>2011/01/25</td>
                                                        <td>$112,000</td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Position</th>
                                                        <th>Office</th>
                                                        <th>Age</th>
                                                        <th>Start date</th>
                                                        <th>Salary</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--/ Column selectors table -->
                </div>
            </div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <div class="customizer border-left-blue-grey border-left-lighten-4 d-none d-xl-block">
            <a class="customizer-close" href="#">
                <i class="ft-x font-medium-3"></i>
            </a>
            <a class="customizer-toggle bg-danger box-shadow-3" href="#">
                <i class="ft-settings font-medium-3 spinner white"></i>
            </a>
            <div class="customizer-content p-2">
                <h4 class="text-uppercase mb-0">Theme Customizer</h4>
                <hr>
                <p>Customize & Preview in Real Time</p>
                <h5 class="mt-1 mb-1 text-bold-500">Menu Color Options</h5>
                <div class="form-group">
                    <!-- Outline Button group -->
                    <div class="btn-group customizer-sidebar-options" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-outline-info" data-sidebar="menu-light">Light Menu</button>
                        <button type="button" class="btn btn-outline-info" data-sidebar="menu-dark">Dark Menu</button>
                    </div>
                </div>
                <hr>
                <h5 class="mt-1 text-bold-500">Layout Options</h5>
                <ul class="nav nav-tabs nav-underline nav-justified layout-options">
                    <li class="nav-item">
                        <a class="nav-link layouts active" id="baseIcon-tab21" data-toggle="tab" aria-controls="tabIcon21" href="#tabIcon21" aria-expanded="true">Layouts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navigation" id="baseIcon-tab22" data-toggle="tab" aria-controls="tabIcon22" href="#tabIcon22" aria-expanded="false">Navigation</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navbar" id="baseIcon-tab23" data-toggle="tab" aria-controls="tabIcon23" href="#tabIcon23" aria-expanded="false">Navbar</a>
                    </li>
                </ul>
                <div class="tab-content px-1 pt-1">
                    <div role="tabpanel" class="tab-pane active" id="tabIcon21" aria-expanded="true" aria-labelledby="baseIcon-tab21">
                        <p>
                            <div class="d-inline-block custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="collapsed-sidebar" id="collapsed-sidebar">
                                <label class="custom-control-label" for="collapsed-sidebar">Collapsed Menu</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="fixed-layout" id="fixed-layout">
                                <label class="custom-control-label" for="fixed-layout">Fixed Layout</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="boxed-layout" id="boxed-layout">
                                <label class="custom-control-label" for="boxed-layout">Boxed Layout</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="static-layout" id="static-layout">
                                <label class="custom-control-label" for="static-layout">Static Layout</label>
                            </div>
                        </p>
                    </div>
                    <div class="tab-pane" id="tabIcon22" aria-labelledby="baseIcon-tab22">
                        <p>
                            <div class="d-inline-block custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="native-scroll" id="native-scroll">
                                <label class="custom-control-label" for="native-scroll">Native Scroll</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="right-side-icons" id="right-side-icons">
                                <label class="custom-control-label" for="right-side-icons">Right Side Icons</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="bordered-navigation" id="bordered-navigation">
                                <label class="custom-control-label" for="bordered-navigation">Bordered Navigation</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="flipped-navigation" id="flipped-navigation">
                                <label class="custom-control-label" for="flipped-navigation">Flipped Navigation</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="collapsible-navigation" id="collapsible-navigation">
                                <label class="custom-control-label" for="collapsible-navigation">Collapsible Navigation</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="static-navigation" id="static-navigation">
                                <label class="custom-control-label" for="static-navigation">Static Navigation</label>
                            </div>
                        </p>
                    </div>
                    <div class="tab-pane" id="tabIcon23" aria-labelledby="baseIcon-tab23">
                        <p>
                            <div class="d-inline-block custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="brand-center" id="brand-center">
                                <label class="custom-control-label" for="brand-center">Brand Center</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="navbar-static-top" id="navbar-static-top">
                                <label class="custom-control-label" for="navbar-static-top">Static Top</label>
                            </div>
                        </p>
                    </div>
                </div>
                <hr>
                <h5 class="mt-1 text-bold-500">Navigation Color Options</h5>
                <ul class="nav nav-tabs nav-underline nav-justified color-options">
                    <li class="nav-item">
                        <a class="nav-link nav-semi-light active" id="color-opt-1" data-toggle="tab" aria-controls="clrOpt1" href="#clrOpt1" aria-expanded="false">Semi Light</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-semi-dark" id="color-opt-2" data-toggle="tab" aria-controls="clrOpt2" href="#clrOpt2" aria-expanded="false">Semi Dark</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-dark" id="color-opt-3" data-toggle="tab" aria-controls="clrOpt3" href="#clrOpt3" aria-expanded="false">Dark</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-light" id="color-opt-4" data-toggle="tab" aria-controls="clrOpt4" href="#clrOpt4" aria-expanded="true">Light</a>
                    </li>
                </ul>
                <div class="tab-content px-1 pt-1">
                    <div role="tabpanel" class="tab-pane active" id="clrOpt1" aria-expanded="true" aria-labelledby="color-opt-1">
                        <div class="row">
                            <div class="col-6">
                                <h6>Solid</h6>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" class="custom-control-input bg-blue-grey" data-bg="bg-blue-grey" id="default">
                                        <label class="custom-control-label" for="default">Default</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" class="custom-control-input bg-primary" data-bg="bg-primary" id="primary">
                                        <label class="custom-control-label" for="primary">Primary</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" class="custom-control-input bg-danger" data-bg="bg-danger" id="danger">
                                        <label class="custom-control-label" for="danger">Danger</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" class="custom-control-input bg-success" data-bg="bg-success" id="success">
                                        <label class="custom-control-label" for="success">Success</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" class="custom-control-input bg-blue" data-bg="bg-blue" id="blue">
                                        <label class="custom-control-label" for="blue">Blue</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan" id="cyan">
                                        <label class="custom-control-label" for="cyan">Cyan</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" class="custom-control-input bg-pink" data-bg="bg-pink" id="pink">
                                        <label class="custom-control-label" for="pink">Pink</label>
                                    </div>
                                </p>
                            </div>
                            <div class="col-6">
                                <h6>Gradient</h6>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" checked class="custom-control-input bg-blue-grey" data-bg="bg-gradient-x-grey-blue"
                                            id="bg-gradient-x-grey-blue">
                                        <label class="custom-control-label" for="bg-gradient-x-grey-blue">Default</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" class="custom-control-input bg-primary" data-bg="bg-gradient-x-primary" id="bg-gradient-x-primary">
                                        <label class="custom-control-label" for="bg-gradient-x-primary">Primary</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" class="custom-control-input bg-danger" data-bg="bg-gradient-x-danger" id="bg-gradient-x-danger">
                                        <label class="custom-control-label" for="bg-gradient-x-danger">Danger</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" class="custom-control-input bg-success" data-bg="bg-gradient-x-success" id="bg-gradient-x-success">
                                        <label class="custom-control-label" for="bg-gradient-x-success">Success</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" class="custom-control-input bg-blue" data-bg="bg-gradient-x-blue" id="bg-gradient-x-blue">
                                        <label class="custom-control-label" for="bg-gradient-x-blue">Blue</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" class="custom-control-input bg-cyan" data-bg="bg-gradient-x-cyan" id="bg-gradient-x-cyan">
                                        <label class="custom-control-label" for="bg-gradient-x-cyan">Cyan</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-slight-clr" class="custom-control-input bg-pink" data-bg="bg-gradient-x-pink" id="bg-gradient-x-pink">
                                        <label class="custom-control-label" for="bg-gradient-x-pink">Pink</label>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="clrOpt2" aria-labelledby="color-opt-2">
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-sdark-clr" checked class="custom-control-input bg-default" data-bg="bg-default" id="opt-default">
                                <label class="custom-control-label" for="opt-default">Default</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-primary" data-bg="bg-primary" id="opt-primary">
                                <label class="custom-control-label" for="opt-primary">Primary</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-danger" data-bg="bg-danger" id="opt-danger">
                                <label class="custom-control-label" for="opt-danger">Danger</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-success" data-bg="bg-success" id="opt-success">
                                <label class="custom-control-label" for="opt-success">Success</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-blue" data-bg="bg-blue" id="opt-blue">
                                <label class="custom-control-label" for="opt-blue">Blue</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan" id="opt-cyan">
                                <label class="custom-control-label" for="opt-cyan">Cyan</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-pink" data-bg="bg-pink" id="opt-pink">
                                <label class="custom-control-label" for="opt-pink">Pink</label>
                            </div>
                        </p>
                    </div>
                    <div class="tab-pane" id="clrOpt3" aria-labelledby="color-opt-3">
                        <div class="row">
                            <div class="col-6">
                                <h3>Solid</h3>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" class="custom-control-input bg-blue-grey" data-bg="bg-blue-grey" id="solid-blue-grey">
                                        <label class="custom-control-label" for="solid-blue-grey">Default</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" class="custom-control-input bg-primary" data-bg="bg-primary" id="solid-primary">
                                        <label class="custom-control-label" for="solid-primary">Primary</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" class="custom-control-input bg-danger" data-bg="bg-danger" id="solid-danger">
                                        <label class="custom-control-label" for="solid-danger">Danger</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" class="custom-control-input bg-success" data-bg="bg-success" id="solid-success">
                                        <label class="custom-control-label" for="solid-success">Success</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" class="custom-control-input bg-blue" data-bg="bg-blue" id="solid-blue">
                                        <label class="custom-control-label" for="solid-blue">Blue</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan" id="solid-cyan">
                                        <label class="custom-control-label" for="solid-cyan">Cyan</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" class="custom-control-input bg-pink" data-bg="bg-pink" id="solid-pink">
                                        <label class="custom-control-label" for="solid-pink">Pink</label>
                                    </div>
                                </p>
                            </div>

                            <div class="col-6">
                                <h3>Gradient</h3>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" class="custom-control-input bg-blue-grey" data-bg="bg-gradient-x-grey-blue" id="bg-gradient-x-grey-blue1">
                                        <label class="custom-control-label" for="bg-gradient-x-grey-blue1">Default</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" checked class="custom-control-input bg-primary" data-bg="bg-gradient-x-primary" id="bg-gradient-x-primary1">
                                        <label class="custom-control-label" for="bg-gradient-x-primary1">Primary</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" checked class="custom-control-input bg-danger" data-bg="bg-gradient-x-danger" id="bg-gradient-x-danger1">
                                        <label class="custom-control-label" for="bg-gradient-x-danger1">Danger</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" checked class="custom-control-input bg-success" data-bg="bg-gradient-x-success" id="bg-gradient-x-success1">
                                        <label class="custom-control-label" for="bg-gradient-x-success1">Success</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" checked class="custom-control-input bg-blue" data-bg="bg-gradient-x-blue" id="bg-gradient-x-blue1">
                                        <label class="custom-control-label" for="bg-gradient-x-blue1">Blue</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" checked class="custom-control-input bg-cyan" data-bg="bg-gradient-x-cyan" id="bg-gradient-x-cyan1">
                                        <label class="custom-control-label" for="bg-gradient-x-cyan1">Cyan</label>
                                    </div>
                                </p>
                                <p>
                                    <div class="d-inline-block custom-control custom-radio">
                                        <input type="radio" name="nav-dark-clr" checked class="custom-control-input bg-pink" data-bg="bg-gradient-x-pink" id="bg-gradient-x-pink1">
                                        <label class="custom-control-label" for="bg-gradient-x-pink1">Pink</label>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="clrOpt4" aria-labelledby="color-opt-4">
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-light-clr" class="custom-control-input bg-blue-grey" data-bg="bg-blue-grey bg-lighten-4" id="light-blue-grey">
                                <label class="custom-control-label" for="light-blue-grey">Default</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-light-clr" class="custom-control-input bg-primary" data-bg="bg-primary bg-lighten-4" id="light-primary">
                                <label class="custom-control-label" for="light-primary">Primary</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-light-clr" class="custom-control-input bg-danger" data-bg="bg-danger bg-lighten-4" id="light-danger">
                                <label class="custom-control-label" for="light-danger">Danger</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-light-clr" class="custom-control-input bg-success" data-bg="bg-success bg-lighten-4" id="light-success">
                                <label class="custom-control-label" for="light-success">Success</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-light-clr" class="custom-control-input bg-blue" data-bg="bg-blue bg-lighten-4" id="light-blue">
                                <label class="custom-control-label" for="light-blue">Blue</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-light-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan bg-lighten-4" id="light-cyan">
                                <label class="custom-control-label" for="light-cyan">Cyan</label>
                            </div>
                        </p>
                        <p>
                            <div class="d-inline-block custom-control custom-radio">
                                <input type="radio" name="nav-light-clr" class="custom-control-input bg-pink" data-bg="bg-pink bg-lighten-4" id="light-pink">
                                <label class="custom-control-label" for="light-pink">Pink</label>
                            </div>
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer footer-static footer-light navbar-border">
            <p class="clearfix text-muted text-sm-center mb-0 px-2">
                <span class="float-md-left d-xs-block d-md-inline-block">Copyright &copy; 2018
                    <a href="http://www.uca.edu.ni/" target="_blank" class="text-bold-800 grey darken-2">Universidad Centroamericana</a>, Todos los derechos reservados. </span>
                <span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with
                    <i class="icon-heart5 pink"></i>
                </span>
            </p>
        </footer>

        <!-- BEGIN VENDOR JS-->
        <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/tables/jszip.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/tables/pdfmake.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/tables/vfs_fonts.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/tables/buttons.html5.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/tables/buttons.print.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/tables/buttons.colVis.min.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN ROBUST JS-->
        <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="app-assets/js/core/app.js" type="text/javascript"></script>
        <script src="app-assets/js/scripts/customizer.js" type="text/javascript"></script>
        <!-- END ROBUST JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
    </body>

    </html>