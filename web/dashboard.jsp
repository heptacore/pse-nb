<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.User" %>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template.">
        <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
        <meta name="author" content="PIXINVENT">
        <title>Entorno de Seguimiento Acad?mico</title>
        <!-- BEGIN STYLES -->
        <style type="text/css">
            .gm-style .gm-style-mtc label,.gm-style .gm-style-mtc div{
                font-weight:400
            }
        </style>
        <style type="text/css">
            .gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{
                font-size:10px
            }
        </style>
        <style type="text/css">
            @media print {
                .gm-style .gmnoprint, .gmnoprint {
                    display:none
                }
            }
            @media screen {
                .gm-style .gmnoscreen, .gmnoscreen {
                    display:none
                }
            }
        </style>
        <style type="text/css">
            .gm-style-pbc{
                transition:opacity ease-in-out;
                background-color:rgba(0,0,0,0.45);
                text-align:center
            }
            .gm-style-pbt{
                    font-size:22px;
                    color:white;
                    font-family:Roboto,Arial,sans-serif;
                    position:relative;
                    margin:0;
                    top:50%;
                    -webkit-transform:translateY(-50%);
                    -ms-transform:translateY(-50%);
                    transform:translateY(-50%);
            }
        </style>
        <style type="text/css">
            .gm-style {
                font: 400 11px Roboto, Arial, sans-serif;
                text-decoration: none;
            }
            .gm-style img {
                max-width: none;
            }
        </style>
        <!-- END STYLES -->
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/logo/logo.ico">
        <link href="app-assets/fonts/font-awesome-4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/icheck/icheck.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/icheck/custom.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/js/gallery/photo-swipe/photoswipe.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/js/gallery/photo-swipe/default-skin/default-skin.css">
        <!-- END VENDOR CSS-->
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="app-assets/fonts/icomoon.css">
        <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
        <!-- BEGIN ROBUST CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
        <!-- END ROBUST CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/users.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/timeline.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/calendars/clndr.css">
        <link rel="stylesheet" type="text/css" href="app-assets/fonts/meteocons/style.min.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="lp-assets/css/style.css">
        <!-- END Custom CSS-->
        <script type="text/javascript" charset="UTF-8" src="http://maps.googleapis.com/maps-api-v3/api/js/32/11/intl/es_ALL/common.js"></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.googleapis.com/maps-api-v3/api/js/32/11/intl/es_ALL/util.js"></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.googleapis.com/maps-api-v3/api/js/32/11/intl/es_ALL/map.js"></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.googleapis.com/maps-api-v3/api/js/32/11/intl/es_ALL/marker.js"></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.googleapis.com/maps-api-v3/api/js/32/11/intl/es_ALL/infowindow.js"></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.googleapis.com/maps-api-v3/api/js/32/11/intl/es_ALL/onion.js"></script>
        <script data-require-id="echarts/chart/bar" src="./../../../app-assets/vendors/js/charts/echarts/chart/bar.js" async=""></script>
        <script data-require-id="echarts/chart/line" src="./../../../app-assets/vendors/js/charts/echarts/chart/line.js" async=""></script>
        <script data-require-id="echarts/chart/radar" src="./../../../app-assets/vendors/js/charts/echarts/chart/radar.js" async=""></script>
        <script data-require-id="echarts/chart/chord" src="./../../../app-assets/vendors/js/charts/echarts/chart/chord.js" async=""></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.googleapis.com/maps-api-v3/api/js/32/11/intl/es_ALL/controls.js"></script>
        <script type="text/javascript" charset="UTF-8" src="http://maps.googleapis.com/maps-api-v3/api/js/32/11/intl/es_ALL/stats.js"></script>
        <script data-require-id="echarts/chart/bar" src="./../../../app-assets/vendors/js/charts/echarts/chart/bar.js" async=""></script>
        <script data-require-id="echarts/chart/line" src="./../../../app-assets/vendors/js/charts/echarts/chart/line.js" async=""></script>
        <script data-require-id="echarts/chart/radar" src="./../../../app-assets/vendors/js/charts/echarts/chart/radar.js" async=""></script>
        <script data-require-id="echarts/chart/chord" src="./../../../app-assets/vendors/js/charts/echarts/chart/chord.js" async=""></script>
        <script data-require-id="echarts/chart/bar" src="./../../../app-assets/vendors/js/charts/echarts/chart/bar.js" async=""></script>
        <script data-require-id="echarts/chart/line" src="./../../../app-assets/vendors/js/charts/echarts/chart/line.js" async=""></script>
        <script data-require-id="echarts/chart/radar" src="./../../../app-assets/vendors/js/charts/echarts/chart/radar.js" async=""></script>
        <script data-require-id="echarts/chart/chord" src="./../../../app-assets/vendors/js/charts/echarts/chart/chord.js" async=""></script>
        <script data-require-id="echarts/chart/bar" src="./../../../app-assets/vendors/js/charts/echarts/chart/bar.js" async=""></script>
        <script data-require-id="echarts/chart/line" src="./../../../app-assets/vendors/js/charts/echarts/chart/line.js" async=""></script>
        <script data-require-id="echarts/chart/radar" src="./../../../app-assets/vendors/js/charts/echarts/chart/radar.js" async=""></script>
        <script data-require-id="echarts/chart/chord" src="./../../../app-assets/vendors/js/charts/echarts/chart/chord.js" async=""></script>
        <script data-require-id="echarts/chart/bar" src="./../../../app-assets/vendors/js/charts/echarts/chart/bar.js" async=""></script>
        <script data-require-id="echarts/chart/line" src="./../../../app-assets/vendors/js/charts/echarts/chart/line.js" async=""></script>
        <script data-require-id="echarts/chart/radar" src="./../../../app-assets/vendors/js/charts/echarts/chart/radar.js" async=""></script>
        <script data-require-id="echarts/chart/chord" src="./../../../app-assets/vendors/js/charts/echarts/chart/chord.js" async=""></script>
    </head>

    <%
        //  There is an active session.
        try {
            if (session.getAttribute("user").equals(null) == false) {
    %>

    <body class="vertical-layout 2-columns fixed-navbar pace-done menu-expanded vertical-menu" data-open="click" data-menu="vertical-menu" data-col="2-columns"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div>

        <!-- fixed-top-->
        <div id="navigation-container">
        </div>
        <!-- fixed-top-->
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <div id="user-profile">
                        <div class="row">
                            <div class="col-12">
                                <div class="card profile-with-cover">


                                    <nav class="navbar navbar-light navbar-profile align-self-end">
                                        <button class="navbar-toggler d-sm-none" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation"></button>
                                        <nav class="navbar navbar-expand-lg">
                                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                                <ul class="navbar-nav mr-auto">
                                                    <li class="nav-item active">
                                                        <a class="nav-link" href="#">
                                                            <i class="fa fa-line-chart"></i> Timeline
                                                            <span class="sr-only">(current)</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#">
                                                            <i class="fa fa-user"></i> Profile</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#">
                                                            <i class="fa fa-briefcase"></i> Projects</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#">
                                                            <i class="fa fa-heart-o"></i> Favourites</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#">
                                                            <i class="fa fa-bell-o"></i> Notifications</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </nav></nav></div>

                            </div>
                        </div>
                    </div>
                    <section id="timeline" class="timeline-center timeline-wrapper">
                        <h3 class="page-title text-center">Timeline</h3>
                        <ul class="timeline">
                            <li class="timeline-line"></li>
                            <li class="timeline-group">
                                <a href="#" class="btn btn-primary">
                                    <i class="fa fa-calendar-o"></i> Today</a>
                            </li>
                        </ul>
                        <ul class="timeline">
                            <li class="timeline-line"></li>


                            <li class="timeline-item">

                                <div class="timeline-card card border-grey border-lighten-2">
                                    <div class="card-header">
                                        <div class="media">
                                            <div class="media-left mr-1">
                                                <a href="#">
                                                    <span class="avatar avatar-md avatar-busy">
                                                        <img src="app-assets/images/portrait/small/avatar-s-18.png" alt="avatar">
                                                    </span>
                                                    <i></i>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="card-title">
                                                    <a href="#">Application API Support</a>
                                                </h4>
                                                <p class="card-subtitle text-muted mb-0 pt-1">
                                                    <span class="font-small-3">15 Oct, 2015 at 8.00 A.M</span>
                                                    <span class="badge badge-pill badge-default badge-warning float-right">High</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <img class="img-fluid" src="app-assets/images/portfolio/width-1200/portfolio-3.jpg" alt="Timeline Image 1">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <p class="card-text">Nullam facilisis fermentum aliquam. Suspendisse ornare dolor vitae libero hendrerit auctor lacinia
                                                    a ligula. Curabitur elit tellus, porta ut orci sed, fermentum bibendum nisi.</p>

                                                <ul class="list-inline">
                                                    <li class="pr-1">
                                                        <a href="#" class="">
                                                            <span class="fa fa-thumbs-o-up"></span> Like</a>
                                                    </li>
                                                    <li class="pr-1">
                                                        <a href="#" class="">
                                                            <span class="fa fa-commenting-o"></span> Comment</a>
                                                    </li>

                                                </ul></div>
                                        </div>
                                        <div class="card-footer px-0 py-0">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-left mr-1">
                                                        <a href="#">
                                                            <span class="avatar avatar-online">
                                                                <img src="app-assets/images/portrait/small/avatar-s-4.png" alt="avatar">
                                                            </span>
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="text-bold-600 mb-0">
                                                            <a href="#">Crystal Lawson</a>
                                                        </p>
                                                        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.</p>
                                                        <div class="media">
                                                            <div class="media-left mr-1">
                                                                <a href="#">
                                                                    <span class="avatar avatar-online">
                                                                        <img src="app-assets/images/portrait/small/avatar-s-6.png" alt="avatar">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <p class="text-bold-600 mb-0">
                                                                    <a href="#">Rafila Gaitan</a>
                                                                </p>
                                                                <p>Gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                                                <div class="media">
                                                                    <div class="media-left mr-1">
                                                                        <a href="#">
                                                                            <span class="avatar avatar-online">
                                                                                <img src="app-assets/images/portrait/small/avatar-s-6.png" alt="avatar">
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <p class="text-bold-600 mb-0">
                                                                            <a href="#">Rafila Gaitan</a>
                                                                        </p>
                                                                        <p>Gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                                                    </div>
                                                                </div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <fieldset class="form-group position-relative has-icon-left mb-0">
                                                    <input type="text" class="form-control" placeholder="Write comments...">
                                                    <div class="form-control-position">
                                                        <i class="fa fa-dashcube"></i>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="timeline-item">

                                <div class="timeline-card card border-grey border-lighten-2">
                                    <div class="card-header">
                                        <div class="media">
                                            <div class="media-left mr-1">
                                                <a href="#">
                                                    <span class="avatar avatar-md avatar-busy">
                                                        <img src="app-assets/images/portrait/small/avatar-s-18.png" alt="avatar">
                                                    </span>
                                                    <i></i>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="card-title">
                                                    <a href="#">Application API Support</a>
                                                </h4>
                                                <p class="card-subtitle text-muted mb-0 pt-1">
                                                    <span class="font-small-3">15 Oct, 2015 at 8.00 A.M</span>
                                                    <span class="badge badge-pill badge-default badge-warning float-right">High</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <img class="img-fluid" src="app-assets/images/portfolio/width-1200/portfolio-3.jpg" alt="Timeline Image 1">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <p class="card-text">Nullam facilisis fermentum aliquam. Suspendisse ornare dolor vitae libero hendrerit auctor lacinia
                                                    a ligula. Curabitur elit tellus, porta ut orci sed, fermentum bibendum nisi.</p>

                                                <ul class="list-inline">
                                                    <li class="pr-1">
                                                        <a href="#" class="">
                                                            <span class="fa fa-thumbs-o-up"></span> Like</a>
                                                    </li>
                                                    <li class="pr-1">
                                                        <a href="#" class="">
                                                            <span class="fa fa-commenting-o"></span> Comment</a>
                                                    </li>

                                                </ul></div>
                                        </div>
                                        <div class="card-footer px-0 py-0">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-left mr-1">
                                                        <a href="#">
                                                            <span class="avatar avatar-online">
                                                                <img src="app-assets/images/portrait/small/avatar-s-4.png" alt="avatar">
                                                            </span>
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="text-bold-600 mb-0">
                                                            <a href="#">Crystal Lawson</a>
                                                        </p>
                                                        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.</p>
                                                        <div class="media">
                                                            <div class="media-left mr-1">
                                                                <a href="#">
                                                                    <span class="avatar avatar-online">
                                                                        <img src="app-assets/images/portrait/small/avatar-s-6.png" alt="avatar">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <p class="text-bold-600 mb-0">
                                                                    <a href="#">Rafila Gaitan</a>
                                                                </p>
                                                                <p>Gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                                                <div class="media">
                                                                    <div class="media-left mr-1">
                                                                        <a href="#">
                                                                            <span class="avatar avatar-online">
                                                                                <img src="app-assets/images/portrait/small/avatar-s-6.png" alt="avatar">
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <p class="text-bold-600 mb-0">
                                                                            <a href="#">Rafila Gaitan</a>
                                                                        </p>
                                                                        <p>Gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                                                    </div>
                                                                </div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <fieldset class="form-group position-relative has-icon-left mb-0">
                                                    <input type="text" class="form-control" placeholder="Write comments...">
                                                    <div class="form-control-position">
                                                        <i class="fa fa-dashcube"></i>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="timeline-item">

                                <div class="timeline-card card border-grey border-lighten-2">
                                    <div class="card-header">
                                        <div class="media">
                                            <div class="media-left mr-1">
                                                <a href="#">
                                                    <span class="avatar avatar-md avatar-busy">
                                                        <img src="app-assets/images/portrait/small/avatar-s-18.png" alt="avatar">
                                                    </span>
                                                    <i></i>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="card-title">
                                                    <a href="#">Application API Support</a>
                                                </h4>
                                                <p class="card-subtitle text-muted mb-0 pt-1">
                                                    <span class="font-small-3">15 Oct, 2015 at 8.00 A.M</span>
                                                    <span class="badge badge-pill badge-default badge-warning float-right">High</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <img class="img-fluid" src="app-assets/images/portfolio/width-1200/portfolio-3.jpg" alt="Timeline Image 1">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <p class="card-text">Nullam facilisis fermentum aliquam. Suspendisse ornare dolor vitae libero hendrerit auctor lacinia
                                                    a ligula. Curabitur elit tellus, porta ut orci sed, fermentum bibendum nisi.</p>

                                                <ul class="list-inline">
                                                    <li class="pr-1">
                                                        <a href="#" class="">
                                                            <span class="fa fa-thumbs-o-up"></span> Like</a>
                                                    </li>
                                                    <li class="pr-1">
                                                        <a href="#" class="">
                                                            <span class="fa fa-commenting-o"></span> Comment</a>
                                                    </li>

                                                </ul></div>
                                        </div>
                                        <div class="card-footer px-0 py-0">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-left mr-1">
                                                        <a href="#">
                                                            <span class="avatar avatar-online">
                                                                <img src="app-assets/images/portrait/small/avatar-s-4.png" alt="avatar">
                                                            </span>
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="text-bold-600 mb-0">
                                                            <a href="#">Crystal Lawson</a>
                                                        </p>
                                                        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.</p>
                                                        <div class="media">
                                                            <div class="media-left mr-1">
                                                                <a href="#">
                                                                    <span class="avatar avatar-online">
                                                                        <img src="app-assets/images/portrait/small/avatar-s-6.png" alt="avatar">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <p class="text-bold-600 mb-0">
                                                                    <a href="#">Rafila Gaitan</a>
                                                                </p>
                                                                <p>Gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                                                <div class="media">
                                                                    <div class="media-left mr-1">
                                                                        <a href="#">
                                                                            <span class="avatar avatar-online">
                                                                                <img src="app-assets/images/portrait/small/avatar-s-6.png" alt="avatar">
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <p class="text-bold-600 mb-0">
                                                                            <a href="#">Rafila Gaitan</a>
                                                                        </p>
                                                                        <p>Gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                                                    </div>
                                                                </div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <fieldset class="form-group position-relative has-icon-left mb-0">
                                                    <input type="text" class="form-control" placeholder="Write comments...">
                                                    <div class="form-control-position">
                                                        <i class="fa fa-dashcube"></i>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <!-- 2016 -->
                        <ul class="timeline">
                            <li class="timeline-line"></li>
                            <li class="timeline-group">
                                <a href="#" class="btn btn-primary">
                                    <i class="fa fa-calendar-o"></i> 2016</a>
                            </li>
                        </ul>
                        <ul class="timeline">
                            <li class="timeline-line"></li>
                            <!-- /.timeline-line -->
                            <li class="timeline-item">
                                <div class="timeline-badge">
                                    <span class="avatar avatar-online" data-toggle="tooltip" data-placement="right" title="" data-original-title="Eu pid nunc urna integer">
                                        <img src="app-assets/images/portrait/small/avatar-s-5.png" alt="avatar">
                                    </span>
                                </div>
                                <div class="timeline-card card text-white">
                                    <img class="card-img img-fluid" src="app-assets/images/portfolio/width-1200/portfolio-2.jpg" alt="Card image">
                                    <div class="card-img-overlay bg-overlay">
                                        <h4 class="card-title white">Good Morning</h4>
                                        <p class="card-text">
                                            <small>26 Aug, 2016 at 2.00 A.M</small>
                                        </p>
                                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content
                                            is a little bit longer.</p>
                                        <p class="card-text">Eu pid nunc urna integer, sed, cras tortor scelerisque penatibus facilisis a pulvinar, rhoncus sagittis
                                            ut nunc elit! Sociis in et? Rhoncus, vel dignissim in scelerisque. Dolor lacus pulvinar adipiscing adipiscing
                                            montes!
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="timeline-item block">
                                <div class="timeline-badge">
                                    <a title="" data-context="inverse" data-container="body" class="border-silc" href="#" data-original-title="block highlight"></a>
                                </div>
                                <div class="timeline-card card border-grey border-lighten-2">
                                    <div class="card-header">
                                        <div class="text-center">
                                            <p>
                                                <i class="fa fa-bar-chart font-medium-4"></i>
                                            </p>
                                            <h4>Campaign Report</h4>
                                            <p class="timeline-date">18 hours ago</p>
                                            <p>Rhoncus, vel dignissim in scelerisque. Dolor lacus pulvinar adipiscing adipiscing montes! Elementum risus
                                                adipiscing non, cras scelerisque risus penatibus? Massa vut, habitasse, tincidunt!</p>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="chart-container">
                                                <div id="stacked-column" class="height-400 overflow-hidden echart-container"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>
                        <!-- 2015 -->
                        <ul class="timeline">
                            <li class="timeline-line"></li>
                            <li class="timeline-group">
                                <a href="#" class="btn btn-primary">
                                    <i class="fa fa-calendar-o"></i> 2015</a>
                            </li>
                        </ul>
                        <ul class="timeline">
                            <li class="timeline-line"></li>
                            <!-- /.timeline-line -->
                            <li class="timeline-item block">
                                <div class="timeline-badge">
                                    <a title="" data-context="inverse" data-container="body" class="border-silc" href="#" data-original-title="block highlight"></a>
                                </div>
                                <div class="timeline-card card border-grey border-lighten-2">
                                    <div class="card-header">
                                        <div class="text-center">
                                            <p class="mt-1">
                                                <i class="fa fa-file-image-o font-medium-4"></i>
                                            </p>
                                            <h4>Media Gallery</h4>
                                            <p class="timeline-date">July 1, 2015</p>
                                            <p>Eu pid nunc urna integer, sed, cras tortor scelerisque penatibus facilisis a pulvinar, rhoncus sagittis
                                                ut nunc elit! Sociis in et?</p>
                                        </div>
                                    </div>
                                    <!-- Image grid -->
                                    <div class="card-content">
                                        <div class="card-body my-gallery" itemscope="" itemtype="http://schema.org/ImageGallery" data-pswp-uid="1">
                                            <div class="row">
                                                <figure class="col-md-3 col-sm-6 col-12" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                                                    <a href="app-assets/images/gallery/1.jpg" itemprop="contentUrl" data-size="480x360">
                                                        <img class="img-thumbnail img-fluid" src="app-assets/images/gallery/1.jpg" itemprop="thumbnail" alt="Image description">
                                                    </a>
                                                </figure>
                                                <figure class="col-md-3 col-sm-6 col-12" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                                                    <a href="app-assets/images/gallery/2.jpg" itemprop="contentUrl" data-size="480x360">
                                                        <img class="img-thumbnail img-fluid" src="app-assets/images/gallery/2.jpg" itemprop="thumbnail" alt="Image description">
                                                    </a>
                                                </figure>
                                                <figure class="col-md-3 col-sm-6 col-12" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                                                    <a href="app-assets/images/gallery/3.jpg" itemprop="contentUrl" data-size="480x360">
                                                        <img class="img-thumbnail img-fluid" src="app-assets/images/gallery/3.jpg" itemprop="thumbnail" alt="Image description">
                                                    </a>
                                                </figure>
                                                <figure class="col-md-3 col-sm-6 col-12" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                                                    <a href="app-assets/images/gallery/4.jpg" itemprop="contentUrl" data-size="480x360">
                                                        <img class="img-thumbnail img-fluid" src="app-assets/images/gallery/4.jpg" itemprop="thumbnail" alt="Image description">
                                                    </a>
                                                </figure>
                                            </div>
                                            <div class="row">
                                                <figure class="col-md-3 col-sm-6 col-12" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                                                    <a href="app-assets/images/gallery/5.jpg" itemprop="contentUrl" data-size="480x360">
                                                        <img class="img-thumbnail img-fluid" src="app-assets/images/gallery/5.jpg" itemprop="thumbnail" alt="Image description">
                                                    </a>
                                                </figure>
                                                <figure class="col-md-3 col-sm-6 col-12" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                                                    <a href="app-assets/images/gallery/6.jpg" itemprop="contentUrl" data-size="480x360">
                                                        <img class="img-thumbnail img-fluid" src="app-assets/images/gallery/6.jpg" itemprop="thumbnail" alt="Image description">
                                                    </a>
                                                </figure>
                                                <figure class="col-md-3 col-sm-6 col-12" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                                                    <a href="app-assets/images/gallery/7.jpg" itemprop="contentUrl" data-size="480x360">
                                                        <img class="img-thumbnail img-fluid" src="app-assets/images/gallery/7.jpg" itemprop="thumbnail" alt="Image description">
                                                    </a>
                                                </figure>
                                                <figure class="col-md-3 col-sm-6 col-12" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                                                    <a href="app-assets/images/gallery/8.jpg" itemprop="contentUrl" data-size="480x360">
                                                        <img class="img-thumbnail img-fluid" src="app-assets/images/gallery/8.jpg" itemprop="thumbnail" alt="Image description">
                                                    </a>
                                                </figure>
                                            </div>
                                            <div class="row">
                                                <figure class="col-md-3 col-sm-6 col-12" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                                                    <a href="app-assets/images/gallery/9.jpg" itemprop="contentUrl" data-size="480x360">
                                                        <img class="img-thumbnail img-fluid" src="app-assets/images/gallery/9.jpg" itemprop="thumbnail" alt="Image description">
                                                    </a>
                                                </figure>
                                                <figure class="col-md-3 col-sm-6 col-12" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                                                    <a href="app-assets/images/gallery/10.jpg" itemprop="contentUrl" data-size="480x360">
                                                        <img class="img-thumbnail img-fluid" src="app-assets/images/gallery/10.jpg" itemprop="thumbnail" alt="Image description">
                                                    </a>
                                                </figure>
                                                <figure class="col-md-3 col-sm-6 col-12" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                                                    <a href="app-assets/images/gallery/11.jpg" itemprop="contentUrl" data-size="480x360">
                                                        <img class="img-thumbnail img-fluid" src="app-assets/images/gallery/11.jpg" itemprop="thumbnail" alt="Image description">
                                                    </a>
                                                </figure>
                                                <figure class="col-md-3 col-sm-6 col-12" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
                                                    <a href="app-assets/images/gallery/12.jpg" itemprop="contentUrl" data-size="480x360">
                                                        <img class="img-thumbnail img-fluid" src="app-assets/images/gallery/12.jpg" itemprop="thumbnail" alt="Image description">
                                                    </a>
                                                </figure>
                                            </div>
                                        </div>
                                        <!--/ Image grid -->
                                        <!-- Root element of PhotoSwipe. Must have class pswp. -->
                                        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                                            <!-- Background of PhotoSwipe. 
                                             It's a separate element as animating opacity is faster than rgba(). -->
                                            <div class="pswp__bg"></div>
                                            <!-- Slides wrapper with overflow:hidden. -->
                                            <div class="pswp__scroll-wrap">
                                                <!-- Container that holds slides. 
                                                  PhotoSwipe keeps only 3 of them in the DOM to save memory.
                                                  Don't modify these 3 pswp__item elements, data is added later on. -->
                                                <div class="pswp__container" style="transform: translate3d(-3022px, 0px, 0px);">
                                                    <div class="pswp__item" style="display: block; transform: translate3d(3022px, 0px, 0px);"><div class="pswp__zoom-wrap" style="transform: translate3d(830.984px, 305.906px, 0px) scale(0.284312);"><img class="pswp__img" src="app-assets/images/gallery/3.jpg" style="opacity: 1; width: 765px; height: 574px;"></div></div>
                                                    <div class="pswp__item" style="transform: translate3d(4533px, 0px, 0px);"><div class="pswp__zoom-wrap" style="transform: translate3d(435px, 151px, 0px) scale(0.627178);"><img class="pswp__img" src="app-assets/images/gallery/4.jpg" style="opacity: 1; width: 765px; height: 574px;"></div></div>
                                                    <div class="pswp__item" style="display: block; transform: translate3d(1511px, 0px, 0px);"><div class="pswp__zoom-wrap" style="transform: translate3d(435px, 151px, 0px) scale(0.627178);"><img class="pswp__img" src="app-assets/images/gallery/2.jpg" style="opacity: 1; width: 765px; height: 574px;"></div></div>
                                                </div>
                                                <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                                                <div class="pswp__ui pswp__ui--fit pswp__ui--hidden">
                                                    <div class="pswp__top-bar">
                                                        <!--  Controls are self-explanatory. Order can be changed. -->
                                                        <div class="pswp__counter">3 / 4</div>
                                                        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                                                        <button class="pswp__button pswp__button--share" title="Share"></button>
                                                        <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                                                        <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                                                        <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                                                        <!-- element will get class pswp__preloader-active when preloader is running -->
                                                        <div class="pswp__preloader">
                                                            <div class="pswp__preloader__icn">
                                                                <div class="pswp__preloader__cut">
                                                                    <div class="pswp__preloader__donut"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                                        <div class="pswp__share-tooltip"></div>
                                                    </div>
                                                    <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                                                    </button>
                                                    <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                                                    </button>
                                                    <div class="pswp__caption">
                                                        <div class="pswp__caption__center"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ PhotoSwipe -->
                                </div>
                            </li>

                            <li class="timeline-item mt-3">

                                <div class="timeline-card card border-grey border-lighten-2">
                                    <div class="card-header">
                                        <h4 class="card-title">
                                            <a href="#">Quote of the day</a>
                                        </h4>
                                        <p class="card-subtitle text-muted mb-0 pt-1">
                                            <span class="font-small-3">03 March, 2015 at 5 P.M</span>
                                        </p>
                                    </div>
                                    <div class="card-content">
                                        <img class="img-fluid" src="app-assets/images/portfolio/width-600/portfolio-3.jpg" alt="Timeline Image 1">
                                        <div class="card-body">
                                            <blockquote class="card-bodyquote">
                                                <p class="card-text">Eu pid nunc urna integer, sed, cras tortor scelerisque penatibus facilisis a pulvinar, rhoncus sagittis
                                                    ut nunc elit! Sociis in et?</p>
                                                <footer>Someone famous in
                                                    <cite title="Source Title"> - Source Title</cite>
                                                </footer>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="timeline-item mt-3">

                                <div class="timeline-card card border-grey border-lighten-2">
                                    <div class="card-header">
                                        <h4 class="card-title">
                                            <a href="#">Quote of the day</a>
                                        </h4>
                                        <p class="card-subtitle text-muted mb-0 pt-1">
                                            <span class="font-small-3">03 March, 2015 at 5 P.M</span>
                                        </p>
                                    </div>
                                    <div class="card-content">
                                        <img class="img-fluid" src="app-assets/images/portfolio/width-600/portfolio-3.jpg" alt="Timeline Image 1">
                                        <div class="card-body">
                                            <blockquote class="card-bodyquote">
                                                <p class="card-text">Eu pid nunc urna integer, sed, cras tortor scelerisque penatibus facilisis a pulvinar, rhoncus sagittis
                                                    ut nunc elit! Sociis in et?</p>
                                                <footer>Someone famous in
                                                    <cite title="Source Title"> - Source Title</cite>
                                                </footer>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </li><li class="timeline-item">

                                <div class="timeline-card card border-grey border-lighten-2">
                                    <div class="card-header">
                                        <div class="media">
                                            <div class="media-left mr-1">
                                                <a href="#">
                                                    <span class="avatar avatar-md avatar-busy">
                                                        <img src="app-assets/images/portrait/small/avatar-s-18.png" alt="avatar">
                                                    </span>
                                                    <i></i>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="card-title">
                                                    <a href="#">Application API Support</a>
                                                </h4>
                                                <p class="card-subtitle text-muted mb-0 pt-1">
                                                    <span class="font-small-3">15 Oct, 2015 at 8.00 A.M</span>
                                                    <span class="badge badge-pill badge-default badge-warning float-right">High</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <img class="img-fluid" src="app-assets/images/portfolio/width-1200/portfolio-3.jpg" alt="Timeline Image 1">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <p class="card-text">Nullam facilisis fermentum aliquam. Suspendisse ornare dolor vitae libero hendrerit auctor lacinia
                                                    a ligula. Curabitur elit tellus, porta ut orci sed, fermentum bibendum nisi.</p>

                                                <ul class="list-inline">
                                                    <li class="pr-1">
                                                        <a href="#" class="">
                                                            <span class="fa fa-thumbs-o-up"></span> Like</a>
                                                    </li>
                                                    <li class="pr-1">
                                                        <a href="#" class="">
                                                            <span class="fa fa-commenting-o"></span> Comment</a>
                                                    </li>

                                                </ul></div>
                                        </div>
                                        <div class="card-footer px-0 py-0">
                                            <div class="card-body">
                                                <div class="media">
                                                    <div class="media-left mr-1">
                                                        <a href="#">
                                                            <span class="avatar avatar-online">
                                                                <img src="app-assets/images/portrait/small/avatar-s-4.png" alt="avatar">
                                                            </span>
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="text-bold-600 mb-0">
                                                            <a href="#">Crystal Lawson</a>
                                                        </p>
                                                        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.</p>
                                                        <div class="media">
                                                            <div class="media-left mr-1">
                                                                <a href="#">
                                                                    <span class="avatar avatar-online">
                                                                        <img src="app-assets/images/portrait/small/avatar-s-6.png" alt="avatar">
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <p class="text-bold-600 mb-0">
                                                                    <a href="#">Rafila Gaitan</a>
                                                                </p>
                                                                <p>Gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                                                <div class="media">
                                                                    <div class="media-left mr-1">
                                                                        <a href="#">
                                                                            <span class="avatar avatar-online">
                                                                                <img src="app-assets/images/portrait/small/avatar-s-6.png" alt="avatar">
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <p class="text-bold-600 mb-0">
                                                                            <a href="#">Rafila Gaitan</a>
                                                                        </p>
                                                                        <p>Gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                                                    </div>
                                                                </div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <fieldset class="form-group position-relative has-icon-left mb-0">
                                                    <input type="text" class="form-control" placeholder="Write comments...">
                                                    <div class="form-control-position">
                                                        <i class="fa fa-dashcube"></i>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li></ul>
                        <!-- 2014 -->
                        <ul class="timeline">
                            <li class="timeline-line"></li>
                            <li class="timeline-group">
                                <a href="#" class="btn btn-primary">
                                    <i class="fa fa-calendar-o"></i> 2014</a>
                            </li>
                        </ul>
                        <ul class="timeline">
                            <li class="timeline-line"></li>
                            <!-- /.timeline-line -->
                            <li class="timeline-item block">
                                <div class="timeline-badge">
                                    <a title="" data-context="inverse" data-container="body" class="border-silc" href="#" data-original-title="block highlight"></a>
                                </div>
                                <div class="timeline-card card border-grey border-lighten-2">
                                    <div class="card-header">
                                        <div class="text-center">
                                            <p>
                                                <i class="fa fa-map-marker font-medium-4"></i>
                                            </p>
                                            <h4>Moved to Brooklyn</h4>
                                            <p class="timeline-date">Jan 1, 2014</p>
                                            <p>Eu pid nunc urna integer, sed, cras tortor scelerisque penatibus facilisis a pulvinar, rhoncus sagittis
                                                ut nunc elit! Sociis in et? Rhoncus, vel dignissim in scelerisque. Dolor lacus pulvinar adipiscing
                                                adipiscing montes! Elementum risus adipiscing non, cras scelerisque risus penatibus? Massa vut, habitasse,
                                                tincidunt!
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div id="moved-brooklyn" class="height-450" style="position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div class="gm-style" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;"><div tabindex="0" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; cursor: url(&quot;http://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; touch-action: pan-x pan-y;"><div style="z-index: 1; position: absolute; left: 50%; top: 50%; transform: translate(0px, 0px);"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; z-index: 1; transform: matrix(1, 0, 0, 1, -232, -173);"><div style="position: absolute; left: 256px; top: 0px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 0px; top: -256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 256px; top: -256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 512px; top: -256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 512px; top: 0px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 512px; top: 256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 256px; top: 256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 0px; top: 256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: -256px; top: 256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: -256px; top: 0px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: -256px; top: -256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div style="position: absolute; z-index: 1; transform: matrix(1, 0, 0, 1, -232, -173);"><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: 0px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 0px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: -256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: -256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 512px; top: -256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 512px; top: 0px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 512px; top: 256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: 256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: 256px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: 0px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: -256px;"></div></div></div><div style="width: 27px; height: 43px; overflow: hidden; position: absolute; left: -14px; top: -43px; z-index: 0;"><img alt="" src="http://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 27px; height: 43px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; z-index: 1; transform: matrix(1, 0, 0, 1, -232, -173);"><div style="position: absolute; left: 256px; top: 0px; width: 256px; height: 256px;"><img draggable="false" alt="" src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i9653!3i12327!4i256!2m3!1e0!2sm!3i418118772!3m9!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&amp;token=65561" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px;"><img draggable="false" alt="" src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i9652!3i12327!4i256!2m3!1e0!2sm!3i418118689!3m9!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&amp;token=38758" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 0px; top: -256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i9652!3i12326!4i256!2m3!1e0!2sm!3i418118736!3m9!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&amp;token=779" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 256px; top: -256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i9653!3i12326!4i256!2m3!1e0!2sm!3i418118712!3m9!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&amp;token=78509" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 512px; top: -256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i9654!3i12326!4i256!2m3!1e0!2sm!3i418118772!3m9!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&amp;token=10187" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 512px; top: 0px; width: 256px; height: 256px;"><img draggable="false" alt="" src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i9654!3i12327!4i256!2m3!1e0!2sm!3i418118772!3m9!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&amp;token=15008" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 512px; top: 256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i9654!3i12328!4i256!2m3!1e0!2sm!3i418118772!3m9!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&amp;token=19829" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 256px; top: 256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i9653!3i12328!4i256!2m3!1e0!2sm!3i418118772!3m9!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&amp;token=70382" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 0px; top: 256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i9652!3i12328!4i256!2m3!1e0!2sm!3i418118772!3m9!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&amp;token=120935" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -256px; top: 256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i9651!3i12328!4i256!2m3!1e0!2sm!3i418118689!3m9!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&amp;token=94132" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -256px; top: 0px; width: 256px; height: 256px;"><img draggable="false" alt="" src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i9651!3i12327!4i256!2m3!1e0!2sm!3i418118689!3m9!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&amp;token=89311" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -256px; top: -256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i9651!3i12326!4i256!2m3!1e0!2sm!3i418118736!3m9!2ses-ES!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!23i1301875&amp;token=51332" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="z-index: 2; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; opacity: 0;"><p class="gm-style-pbt"></p></div><div style="z-index: 3; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; touch-action: pan-x pan-y;"><div style="z-index: 1; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px;"></div><div style="z-index: 4; position: absolute; left: 50%; top: 50%; transform: translate(0px, 0px);"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"><div class="gmnoprint" style="width: 27px; height: 43px; overflow: hidden; position: absolute; opacity: 0.01; left: -14px; top: -43px; z-index: 0;"><img alt="" src="http://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2.png" draggable="false" usemap="#gmimap0" style="position: absolute; left: 0px; top: 0px; width: 27px; height: 43px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"><map name="gmimap0" id="gmimap0"><area log="miw" coords="13.5,0,4,3.75,0,13.5,13.5,43,27,13.5,23,3.75" shape="poly" title="Moved Brooklyn" style="cursor: pointer; touch-action: none;"></map></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div></div><iframe frameborder="0" src="about:blank" style="z-index: -1; position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; border: none;"></iframe><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" href="https://maps.google.com/maps?ll=40.650002,-73.949997&amp;z=15&amp;t=m&amp;hl=es-ES&amp;gl=US&amp;mapclient=apiv3" title="Haz clic aqu? para visualizar esta zona en Google Maps" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img alt="" src="http://maps.gstatic.com/mapfiles/api-3/images/google4.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 334px; top: 135px;"><div style="padding: 0px 0px 10px; font-size: 16px;">Datos de mapas</div><div style="font-size: 13px;">Datos de mapas ?2018 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img alt="" src="http://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 328px; bottom: 0px; width: 151px;"><div draggable="false" class="gm-style-cc" style="user-select: none; height: 14px; line-height: 14px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="text-decoration: none; cursor: pointer; display: none;">Datos de mapas</a><span>Datos de mapas ?2018 Google</span></div></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Datos de mapas ?2018 Google</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; user-select: none; height: 14px; line-height: 14px; position: absolute; right: 141px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/es-ES_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">T?rminos de uso</a></div></div><button draggable="false" title="Cambiar a la vista en pantalla completa" aria-label="Cambiar a la vista en pantalla completa" type="button" style="background: none rgb(255, 255, 255); border: 0px; margin: 10px 14px; padding: 0px; position: absolute; cursor: pointer; user-select: none; width: 25px; height: 25px; overflow: hidden; top: 0px; right: 0px;"><img alt="" src="http://maps.gstatic.com/mapfiles/api-3/images/sv9.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 175px; user-select: none; border: 0px; padding: 0px; margin: 0px;"></button><div draggable="false" class="gm-style-cc" style="user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_blank" rel="noopener" title="Informar a Google acerca de errores en las im?genes o en el mapa de carreteras" href="https://www.google.com/maps/@40.650002,-73.949997,15z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Informar de un error de Maps</a></div></div><div class="gmnoprint gm-bundled-control" draggable="false" controlwidth="28" controlheight="55" style="margin: 10px; user-select: none; position: absolute; left: 0px; top: 0px;"><div class="gmnoprint" controlwidth="28" controlheight="55" style="position: absolute; left: 0px; top: 0px;"><div draggable="false" style="user-select: none; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 28px; height: 55px;"><button draggable="false" title="Acerca la imagen" aria-label="Acerca la imagen" type="button" style="background: none; display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; user-select: none; width: 28px; height: 27px; top: 0px; left: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img alt="" src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></button><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; background-color: rgb(230, 230, 230); top: 0px;"></div><button draggable="false" title="Aleja la imagen" aria-label="Aleja la imagen" type="button" style="background: none; display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; user-select: none; width: 28px; height: 27px; top: 0px; left: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img alt="" src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: -15px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></button></div></div></div><div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="28" controlheight="28" style="margin: 10px; user-select: none; position: absolute; bottom: 42px; right: 28px;"><div class="gm-svpc" controlwidth="28" controlheight="28" style="background-color: rgb(255, 255, 255); box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;http://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; touch-action: none; position: absolute; left: 0px; top: 0px;"><div style="position: absolute; left: 1px; top: 1px;"></div><div style="position: absolute; left: 1px; top: 1px;"><div aria-label="Control del hombrecito naranja de Street View" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img alt="" src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="El hombrecito naranja est? en la parte superior del mapa" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img alt="" src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Control del hombrecito naranja de Street View" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img alt="" src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;"><div title="Girar el mapa 90 grados" style="width: 28px; height: 28px; overflow: hidden; position: absolute; background-color: rgb(255, 255, 255); box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; display: none;"><img alt="" src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; background-color: rgb(255, 255, 255); box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; top: 0px; cursor: pointer;"><img alt="" src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 48px; top: 0px;"><div class="gm-style-mtc" style="float: left; position: relative;"><div role="button" tabindex="0" title="Muestra el callejero" aria-label="Muestra el callejero" aria-pressed="true" draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; min-width: 28px; font-weight: 500;">Mapa</div><div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; position: absolute; left: 0px; top: 28px; text-align: left; display: none;"><div draggable="false" title="Muestra el callejero con relieve" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap;"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img alt="" src="http://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Relieve</label></div></div></div><div class="gm-style-mtc" style="float: left; position: relative;"><div role="button" tabindex="0" title="Muestra las im?genes de sat?lite" aria-label="Muestra las im?genes de sat?lite" aria-pressed="false" draggable="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; min-width: 37px; border-left: 0px;">Sat?lite</div><div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; position: absolute; right: 0px; top: 29px; text-align: left; display: none;"><div draggable="false" title="Muestra las im?genes con los nombres de las calles" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap;"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img alt="" src="http://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Etiquetas</label></div></div></div></div><div draggable="false" class="gm-style-cc" style="position: absolute; user-select: none; height: 14px; line-height: 14px; right: 227px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><span>200 m&nbsp;</span><div style="position: relative; display: inline-block; height: 8px; bottom: -1px; width: 59px;"><div style="width: 100%; height: 4px; position: absolute; left: 0px; top: 0px;"></div><div style="width: 4px; height: 8px; left: 0px; top: 0px; background-color: rgb(255, 255, 255);"></div><div style="width: 4px; height: 8px; position: absolute; background-color: rgb(255, 255, 255); right: 0px; bottom: 0px;"></div><div style="position: absolute; background-color: rgb(102, 102, 102); height: 2px; left: 1px; bottom: 1px; right: 1px;"></div><div style="position: absolute; width: 2px; height: 6px; left: 1px; top: 1px; background-color: rgb(102, 102, 102);"></div><div style="width: 2px; height: 6px; position: absolute; background-color: rgb(102, 102, 102); bottom: 1px; right: 1px;"></div></div></div></div></div></div></div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <!-- 2014 -->
                        <ul class="timeline">
                            <li class="timeline-line"></li>
                            <li class="timeline-group">
                                <a href="#" class="btn btn-primary">
                                    <i class="fa fa-calendar-o"></i> Founded in 2012</a>
                            </li>
                        </ul>
                    </section>
                </div>

            </div>
        </div>

        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <div class="customizer border-left-blue-grey border-left-lighten-4 d-none d-xl-block">
            <a class="customizer-close" href="#">
                <i class="ft-x font-medium-3"></i>
            </a>
            <a class="customizer-toggle bg-danger box-shadow-3" href="#">
                <i class="ft-settings font-medium-3 spinner white"></i>
            </a>
            <div class="customizer-content p-2 ps-container ps-theme-dark ps-active-y" data-ps-id="c5e10834-482d-9ed0-ffa3-a2e423e34239">
                <h4 class="text-uppercase mb-0">Theme Customizer</h4>
                <hr>
                <p>Customize &amp; Preview in Real Time</p>
                <h5 class="mt-1 mb-1 text-bold-500">Menu Color Options</h5>
                <div class="form-group">
                    <!-- Outline Button group -->
                    <div class="btn-group customizer-sidebar-options" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-outline-info" data-sidebar="menu-light">Light Menu</button>
                        <button type="button" class="btn btn-outline-info active" data-sidebar="menu-dark">Dark Menu</button>
                    </div>
                </div>
                <hr>
                <h5 class="mt-1 text-bold-500">Layout Options</h5>
                <ul class="nav nav-tabs nav-underline nav-justified layout-options">
                    <li class="nav-item">
                        <a class="nav-link layouts active" id="baseIcon-tab21" data-toggle="tab" aria-controls="tabIcon21" href="#tabIcon21" aria-expanded="true">Layouts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navigation" id="baseIcon-tab22" data-toggle="tab" aria-controls="tabIcon22" href="#tabIcon22" aria-expanded="false">Navigation</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navbar" id="baseIcon-tab23" data-toggle="tab" aria-controls="tabIcon23" href="#tabIcon23" aria-expanded="false">Navbar</a>
                    </li>
                </ul>
                <div class="tab-content px-1 pt-1">
                    <div role="tabpanel" class="tab-pane active" id="tabIcon21" aria-expanded="true" aria-labelledby="baseIcon-tab21">
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="collapsed-sidebar" id="collapsed-sidebar">
                            <label class="custom-control-label" for="collapsed-sidebar">Collapsed Menu</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="fixed-layout" id="fixed-layout">
                            <label class="custom-control-label" for="fixed-layout">Fixed Layout</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="boxed-layout" id="boxed-layout">
                            <label class="custom-control-label" for="boxed-layout">Boxed Layout</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="static-layout" id="static-layout">
                            <label class="custom-control-label" for="static-layout">Static Layout</label>
                        </div>
                        <p></p>
                    </div>
                    <div class="tab-pane" id="tabIcon22" aria-labelledby="baseIcon-tab22">
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="native-scroll" id="native-scroll">
                            <label class="custom-control-label" for="native-scroll">Native Scroll</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="right-side-icons" id="right-side-icons">
                            <label class="custom-control-label" for="right-side-icons">Right Side Icons</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="bordered-navigation" id="bordered-navigation">
                            <label class="custom-control-label" for="bordered-navigation">Bordered Navigation</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="flipped-navigation" id="flipped-navigation">
                            <label class="custom-control-label" for="flipped-navigation">Flipped Navigation</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="collapsible-navigation" id="collapsible-navigation">
                            <label class="custom-control-label" for="collapsible-navigation">Collapsible Navigation</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="static-navigation" id="static-navigation">
                            <label class="custom-control-label" for="static-navigation">Static Navigation</label>
                        </div>
                        <p></p>
                    </div>
                    <div class="tab-pane" id="tabIcon23" aria-labelledby="baseIcon-tab23">
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="brand-center" id="brand-center">
                            <label class="custom-control-label" for="brand-center">Brand Center</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="navbar-static-top" id="navbar-static-top">
                            <label class="custom-control-label" for="navbar-static-top">Static Top</label>
                        </div>
                        <p></p>
                    </div>
                </div>
                <hr>
                <h5 class="mt-1 text-bold-500">Navigation Color Options</h5>
                <ul class="nav nav-tabs nav-underline nav-justified color-options">
                    <li class="nav-item">
                        <a class="nav-link nav-semi-light active" id="color-opt-1" data-toggle="tab" aria-controls="clrOpt1" href="#clrOpt1" aria-expanded="false">Semi Light</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-semi-dark" id="color-opt-2" data-toggle="tab" aria-controls="clrOpt2" href="#clrOpt2" aria-expanded="false">Semi Dark</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-dark" id="color-opt-3" data-toggle="tab" aria-controls="clrOpt3" href="#clrOpt3" aria-expanded="false">Dark</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-light" id="color-opt-4" data-toggle="tab" aria-controls="clrOpt4" href="#clrOpt4" aria-expanded="true">Light</a>
                    </li>
                </ul>
                <div class="tab-content px-1 pt-1">
                    <div role="tabpanel" class="tab-pane active" id="clrOpt1" aria-expanded="true" aria-labelledby="color-opt-1">
                        <div class="row">
                            <div class="col-6">
                                <h6>Solid</h6>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-blue-grey" data-bg="bg-blue-grey" id="default">
                                    <label class="custom-control-label" for="default">Default</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-primary" data-bg="bg-primary" id="primary">
                                    <label class="custom-control-label" for="primary">Primary</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-danger" data-bg="bg-danger" id="danger">
                                    <label class="custom-control-label" for="danger">Danger</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-success" data-bg="bg-success" id="success">
                                    <label class="custom-control-label" for="success">Success</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-blue" data-bg="bg-blue" id="blue">
                                    <label class="custom-control-label" for="blue">Blue</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan" id="cyan">
                                    <label class="custom-control-label" for="cyan">Cyan</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-pink" data-bg="bg-pink" id="pink">
                                    <label class="custom-control-label" for="pink">Pink</label>
                                </div>
                                <p></p>
                            </div>
                            <div class="col-6">
                                <h6>Gradient</h6>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" checked="" class="custom-control-input bg-blue-grey" data-bg="bg-gradient-x-grey-blue" id="bg-gradient-x-grey-blue">
                                    <label class="custom-control-label" for="bg-gradient-x-grey-blue">Default</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-primary" data-bg="bg-gradient-x-primary" id="bg-gradient-x-primary">
                                    <label class="custom-control-label" for="bg-gradient-x-primary">Primary</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-danger" data-bg="bg-gradient-x-danger" id="bg-gradient-x-danger">
                                    <label class="custom-control-label" for="bg-gradient-x-danger">Danger</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-success" data-bg="bg-gradient-x-success" id="bg-gradient-x-success">
                                    <label class="custom-control-label" for="bg-gradient-x-success">Success</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-blue" data-bg="bg-gradient-x-blue" id="bg-gradient-x-blue">
                                    <label class="custom-control-label" for="bg-gradient-x-blue">Blue</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-cyan" data-bg="bg-gradient-x-cyan" id="bg-gradient-x-cyan">
                                    <label class="custom-control-label" for="bg-gradient-x-cyan">Cyan</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-pink" data-bg="bg-gradient-x-pink" id="bg-gradient-x-pink">
                                    <label class="custom-control-label" for="bg-gradient-x-pink">Pink</label>
                                </div>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="clrOpt2" aria-labelledby="color-opt-2">
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" checked="" class="custom-control-input bg-default" data-bg="bg-default" id="opt-default">
                            <label class="custom-control-label" for="opt-default">Default</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-primary" data-bg="bg-primary" id="opt-primary">
                            <label class="custom-control-label" for="opt-primary">Primary</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-danger" data-bg="bg-danger" id="opt-danger">
                            <label class="custom-control-label" for="opt-danger">Danger</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-success" data-bg="bg-success" id="opt-success">
                            <label class="custom-control-label" for="opt-success">Success</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-blue" data-bg="bg-blue" id="opt-blue">
                            <label class="custom-control-label" for="opt-blue">Blue</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan" id="opt-cyan">
                            <label class="custom-control-label" for="opt-cyan">Cyan</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-pink" data-bg="bg-pink" id="opt-pink">
                            <label class="custom-control-label" for="opt-pink">Pink</label>
                        </div>
                        <p></p>
                    </div>
                    <div class="tab-pane" id="clrOpt3" aria-labelledby="color-opt-3">
                        <div class="row">
                            <div class="col-6">
                                <h3>Solid</h3>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-blue-grey" data-bg="bg-blue-grey" id="solid-blue-grey">
                                    <label class="custom-control-label" for="solid-blue-grey">Default</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-primary" data-bg="bg-primary" id="solid-primary">
                                    <label class="custom-control-label" for="solid-primary">Primary</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-danger" data-bg="bg-danger" id="solid-danger">
                                    <label class="custom-control-label" for="solid-danger">Danger</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-success" data-bg="bg-success" id="solid-success">
                                    <label class="custom-control-label" for="solid-success">Success</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-blue" data-bg="bg-blue" id="solid-blue">
                                    <label class="custom-control-label" for="solid-blue">Blue</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan" id="solid-cyan">
                                    <label class="custom-control-label" for="solid-cyan">Cyan</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-pink" data-bg="bg-pink" id="solid-pink">
                                    <label class="custom-control-label" for="solid-pink">Pink</label>
                                </div>
                                <p></p>
                            </div>

                            <div class="col-6">
                                <h3>Gradient</h3>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-blue-grey" data-bg="bg-gradient-x-grey-blue" id="bg-gradient-x-grey-blue1">
                                    <label class="custom-control-label" for="bg-gradient-x-grey-blue1">Default</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked="" class="custom-control-input bg-primary" data-bg="bg-gradient-x-primary" id="bg-gradient-x-primary1">
                                    <label class="custom-control-label" for="bg-gradient-x-primary1">Primary</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked="" class="custom-control-input bg-danger" data-bg="bg-gradient-x-danger" id="bg-gradient-x-danger1">
                                    <label class="custom-control-label" for="bg-gradient-x-danger1">Danger</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked="" class="custom-control-input bg-success" data-bg="bg-gradient-x-success" id="bg-gradient-x-success1">
                                    <label class="custom-control-label" for="bg-gradient-x-success1">Success</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked="" class="custom-control-input bg-blue" data-bg="bg-gradient-x-blue" id="bg-gradient-x-blue1">
                                    <label class="custom-control-label" for="bg-gradient-x-blue1">Blue</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked="" class="custom-control-input bg-cyan" data-bg="bg-gradient-x-cyan" id="bg-gradient-x-cyan1">
                                    <label class="custom-control-label" for="bg-gradient-x-cyan1">Cyan</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked="" class="custom-control-input bg-pink" data-bg="bg-gradient-x-pink" id="bg-gradient-x-pink1">
                                    <label class="custom-control-label" for="bg-gradient-x-pink1">Pink</label>
                                </div>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="clrOpt4" aria-labelledby="color-opt-4">
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-blue-grey" data-bg="bg-blue-grey bg-lighten-4" id="light-blue-grey">
                            <label class="custom-control-label" for="light-blue-grey">Default</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-primary" data-bg="bg-primary bg-lighten-4" id="light-primary">
                            <label class="custom-control-label" for="light-primary">Primary</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-danger" data-bg="bg-danger bg-lighten-4" id="light-danger">
                            <label class="custom-control-label" for="light-danger">Danger</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-success" data-bg="bg-success bg-lighten-4" id="light-success">
                            <label class="custom-control-label" for="light-success">Success</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-blue" data-bg="bg-blue bg-lighten-4" id="light-blue">
                            <label class="custom-control-label" for="light-blue">Blue</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan bg-lighten-4" id="light-cyan">
                            <label class="custom-control-label" for="light-cyan">Cyan</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-pink" data-bg="bg-pink bg-lighten-4" id="light-pink">
                            <label class="custom-control-label" for="light-pink">Pink</label>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 662px; right: 3px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 528px;"></div></div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 662px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 528px;"></div></div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 662px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 528px;"></div></div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 662px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 528px;"></div></div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 662px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 528px;"></div></div></div>
        </div>


        <footer class="footer footer-static footer-light navbar-border">
            <p class="clearfix text-muted text-sm-center mb-0 px-2">
                <span class="float-md-left d-xs-block d-md-inline-block">Copyright ? 2018
                    <a href="http://www.uca.edu.ni/" target="_blank" class="text-bold-800 grey darken-2">Universidad Centroamericana</a>, Todos los derechos reservados. </span>
                <span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted &amp; Made with
                    <i class="icon-heart5 pink"></i>
                </span>
            </p>
        </footer>

        <!-- BEGIN VENDOR JS-->
        <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="app-assets/vendors/js/forms/icheck/icheck.min.js" type="text/javascript"></script>
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBDkKetQwosod2SZ7ZGCpxuJdxY3kxo5Po" type="text/javascript"></script>
        <script src="app-assets/vendors/js/charts/gmaps.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/gallery/masonry/masonry.pkgd.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/gallery/photo-swipe/photoswipe.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/gallery/photo-swipe/photoswipe-ui-default.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/charts/echarts/echarts.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN ROBUST JS-->
        <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="app-assets/js/core/app.js" type="text/javascript"></script>
        <script src="app-assets/js/scripts/customizer.js" type="text/javascript"></script>
        <!-- END ROBUST JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="app-assets/js/scripts/charts/echarts/bar-column/stacked-column.js" type="text/javascript"></script>
        <script src="app-assets/js/scripts/charts/echarts/radar-chord/non-ribbon-chord.js" type="text/javascript"></script>
        <script src="app-assets/js/scripts/gallery/photo-swipe/photoswipe-script.js" type="text/javascript"></script>
        <script src="app-assets/js/scripts/pages/timeline.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
        <script>
            $(document).ready(function(){
                $("#navigation-container").load("nav.jsp");
            });
        </script>



        <div id="lumio-bubble-anchor"></div>
    </body>

    <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 17:41:38 GMT -->
    <%
            }
        } catch (Exception e) {
            request.setAttribute("message", "Su sesion ha finalizado.");
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }
    %>
</html>