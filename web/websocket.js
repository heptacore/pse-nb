var wsUri = "ws://localhost:8080/PSE/chatendpoint";
var websocket = new WebSocket(wsUri);

websocket.onerror = function(evt) {
    onError(evt);
};

websocket.onmessage = function(evt) { 
    onMessage(evt);
};

function onError(evt) {
    writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
}

function sendText(json){
    console.log("sending message...");
    websocket.send(json);
}

function onMessage(evt) {
    console.log("message received: " + evt.data);
    appendMessage(evt.data);
}