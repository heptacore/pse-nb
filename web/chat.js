var smsInput = document.getElementById("smsInput");
var sendButton = document.getElementById("sendButton");
sendButton.addEventListener("click", getMessage, false);

function clearChatSection() {
    while (document.getElementById("chatSection").hasChildNodes()){
        document.getElementById("chatSection").removeChild(document.getElementById("chatSection").firstChild);
    }
}

function getMessage(evt){
    var message = JSON.stringify({
        "senderUser": "olimon",
        "receiverUser": "jossy",
        "date": "18-04-2018 02:02:00.0",
        "deleted": 0,
        "seen": 1,
        "value": smsInput.value,
        "attached": ""
    });
    appendMessage(message);
    sendText(message);
}

function appendMessage(json) {
    var text = document.createTextNode(JSON.parse(json).value);
    var container = document.createElement("p");
    container.appendChild(text);
    document.getElementById("chatSection").appendChild(container);
    smsInput.value = "";
}