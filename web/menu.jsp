<%-- 
    Document   : menu
    Created on : 11-abr-2018, 2:39:11
    Author     : Oliver
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
  <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <!-- main menu header-->
    <div class="main-menu-header">
      <input type="text" placeholder="Buscar" class="menu-search form-control round" />
    </div>
    <!-- / main menu header-->
    <!-- main menu content-->
    <div class="main-menu-content">
      <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
        <li class=" nav-item">
          <a href="dashboard.jsp">
            <i class="icon-home3"></i>
            <span data-i18n="nav.dash.main" class="menu-title">Inicio</span>
          </a>
        </li>
        </li>
        <li class=" nav-item">
          <a href="#">
            <i class="icon-user-tie"></i>
            <span data-i18n="nav.project.main" class="menu-title">Colaboradores</span>
          </a>
          <ul class="menu-content">
            <li>
              <a href="add-employee.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Colaborador</a>
            </li>
            <li>
              <a href="employee.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Colaboradores</a>
            </li>
          </ul>
        </li>
        <li class=" nav-item">
          <a href="#">
            <i class="icon-group"></i>
            <span data-i18n="nav.project.main" class="menu-title">Estudiantes</span>
          </a>
          <ul class="menu-content">
            <li>
              <a href="add-student.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Estudiante</a>
            </li>
            <li>
              <a href="student.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Estudiantes</a>
            </li>
          </ul>
        </li>
        <li class=" nav-item">
          <a href="#">
            <i class="icon-ios-albums-outline"></i>
            <span data-i18n="nav.cards.main" class="menu-title">Carreras</span>
          </a>
          <ul class="menu-content">
            <li>
              <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Carrera</a>
            </li>
            <li>
              <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Carreras</a>
            </li>
          </ul>
        </li>
        <li class=" nav-item">
          <a href="#">
            <i class="icon-university2"></i>
            <span data-i18n="nav.advance_cards.main" class="menu-title">Facultades</span>
          </a>
          <ul class="menu-content">
            <li>
              <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Facultad</a>
            </li>
            <li>
              <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Facultades</a>
            </li>
          </ul>
        </li>
        <li class=" nav-item">
          <a href="#">
            <i class="icon-star6"></i>
            <span data-i18n="nav.content.main" class="menu-title">Experiencias</span>
          </a>
          <ul class="menu-content">
            <li>
              <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Experiencia</a>
            </li>
            <li>
              <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Experiencias</a>
            </li>
            <li>
              <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Ver Solicitudes</a>
            </li>
          </ul>
        </li>
        <li class=" nav-item">
          <a href="#">
            <i class="icon-list-alt"></i>
            <span data-i18n="nav.components.main" class="menu-title">Encuestas</span>
          </a>
          <ul class="menu-content">
            <li>
              <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Crear Encuesta</a>
            </li>
            <li>
              <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Encuestas</a>
            </li>
          </ul>
        </li>
        <li class="navigation-header">
          <span data-i18n="nav.category.support">Ajustes</span>
          <i data-toggle="tooltip" data-placement="right" data-original-title="Ajustes"></i>
        </li>
        <li class=" nav-item">
          <a href="#">
            <i class="icon-cog2"></i>
            <span data-i18n="nav.components.main" class="menu-title">Seguridad</span>
          </a>
          <ul class="menu-content">
            <li class=" nav-item">
              <a href="#">
                <span data-i18n="nav.components.main" class="menu-title">Usuarios</span>
              </a>
              <ul class="menu-content">
                <li>
                  <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Registrar Usuario</a>
                </li>
                <li>
                  <a href="user.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Usuarios</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="jobs.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Cargos</a>
            </li>
            <li>
              <a href="permissions.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Permisos</a>
            </li>
          </ul>
        </li>
        <li class=" nav-item">
          <a href="#">
            <i class="icon-file-text3"></i>
            <span data-i18n="nav.support_documentation.main" class="menu-title">Documentación</span>
          </a>
        </li>
      </ul>
    </div>
    <!-- /main menu content-->
    <!-- main menu footer-->
    <!-- include includes/menu-footer-->
    <!-- main menu footer-->
  </div>