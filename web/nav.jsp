<%-- 
    Document   : nav
    Created on : 11-abr-2018, 2:37:19
    Author     : Oliver
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.User" %>
<%@page import="model.Stakeholder" %>
<%@page import="model.Student" %>

<%
    User user;
    user = (User) session.getAttribute("user");
    Stakeholder stakeholder = null;
    Student student = null;
    try {
        //  user is a person
        if (null != user.getPerson()) {
            //  user is a Stakeholder
            if (user.getPerson().getClass().getTypeName().equals("model.Stakeholder")) {
                stakeholder = (Stakeholder) user.getPerson();
                System.out.println("Identificado como stakeholder");
            }
            //  user is a Student
            if (user.getPerson().getClass().getTypeName().equals("model.Student")) {
                student = (Student) user.getPerson();
                System.out.println("Identificado como estudiante");
            }
        }
    } catch (Exception e) {
        System.out.println("ERROR: " + e);
    }
%>
<!-- fixed-top-->
<!-- START NAV -->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto">
                    <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                        <i class="ft-menu font-large-1"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="navbar-brand" href="dashboard.jsp">
                        <img class="brand-logo" alt="pse admin logo" src="app-assets/images/logo/robust-logo-light.png">
                    </a>
                </li>
                <li class="nav-item d-md-none">
                    <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-md-block">
                        <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                            <i class="ft-menu"> </i>
                        </a>
                    </li>
                    <li class="nav-item d-none d-md-block">
                        <a class="nav-link nav-link-expand" href="#">
                            <i class="ficon ft-maximize"></i>
                        </a>
                    </li>
                    <li class="nav-item nav-search">
                        <a class="nav-link nav-link-search" href="#">
                            <i class="ficon ft-search"></i>
                        </a>
                        <div class="search-input">
                            <input class="input" type="text" placeholder="Buscar...">
                        </div>
                    </li>
                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-notification nav-item">
                        <a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                            <i class="ficon ft-bell"></i>
                            <span class="badge badge-pill badge-default badge-danger badge-default badge-up">5</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <h6 class="dropdown-header m-0">
                                    <span class="grey darken-2">Notificaciones</span>
                                </h6>
                                <span class="notification-tag badge badge-default badge-danger float-right m-0">5 Nuevas</span>
                            </li>
                            <li class="scrollable-container media-list w-100">
                                <a href="javascript:void(0)">
                                    <div class="media">
                                        <div class="media-left align-self-center">
                                            <i class="ft-plus-square icon-bg-circle bg-cyan"></i>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading">You have new order!</h6>
                                            <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                            <small>
                                                <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hace 30 minutos</time>
                                            </small>
                                        </div>
                                    </div>
                                </a>
                                <a href="javascript:void(0)">
                                    <div class="media">
                                        <div class="media-left align-self-center">
                                            <i class="ft-download-cloud icon-bg-circle bg-red bg-darken-1"></i>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading red darken-1">99% Server load</h6>
                                            <p class="notification-text font-small-3 text-muted">Aliquam tincidunt mauris eu risus.</p>
                                            <small>
                                                <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hace 5 horas</time>
                                            </small>
                                        </div>
                                    </div>
                                </a>
                                <a href="javascript:void(0)">
                                    <div class="media">
                                        <div class="media-left align-self-center">
                                            <i class="ft-alert-triangle icon-bg-circle bg-yellow bg-darken-3"></i>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading yellow darken-3">Warning notifixation</h6>
                                            <p class="notification-text font-small-3 text-muted">Vestibulum auctor dapibus neque.</p>
                                            <small>
                                                <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hoy</time>
                                            </small>
                                        </div>
                                    </div>
                                </a>
                                <a href="javascript:void(0)">
                                    <div class="media">
                                        <div class="media-left align-self-center">
                                            <i class="ft-check-circle icon-bg-circle bg-cyan"></i>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading">Complete the task</h6>
                                            <small>
                                                <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hace una semana</time>
                                            </small>
                                        </div>
                                    </div>
                                </a>
                                <a href="javascript:void(0)">
                                    <div class="media">
                                        <div class="media-left align-self-center">
                                            <i class="ft-file icon-bg-circle bg-teal"></i>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading">Generate monthly report</h6>
                                            <small>
                                                <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hace un mes</time>
                                            </small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-menu-footer">
                                <a class="dropdown-item text-muted text-center" href="javascript:void(0)">Ver Todas</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-notification nav-item">
                        <a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                            <i class="ficon ft-mail"></i>
                            <span class="badge badge-pill badge-default badge-info badge-default badge-up">4 </span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <h6 class="dropdown-header m-0">
                                    <span class="grey darken-2">Mensajes</span>
                                </h6>
                                <span class="notification-tag badge badge-default badge-warning float-right m-0">4 Nuevos</span>
                            </li>
                            <li class="scrollable-container media-list w-100">
                                <a href="javascript:void(0)">
                                    <div class="media">
                                        <div class="media-left">
                                            <span class="avatar avatar-sm avatar-online rounded-circle">
                                                <img src="app-assets/images/portrait/small/avatar-s-19.png" alt="avatar">
                                                <i></i>
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading">Margaret Govan</h6>
                                            <p class="notification-text font-small-3 text-muted">I like your portfolio, let's start.</p>
                                            <small>
                                                <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hoy</time>
                                            </small>
                                        </div>
                                    </div>
                                </a>
                                <a href="javascript:void(0)">
                                    <div class="media">
                                        <div class="media-left">
                                            <span class="avatar avatar-sm avatar-busy rounded-circle">
                                                <img src="app-assets/images/portrait/small/avatar-s-2.png" alt="avatar">
                                                <i></i>
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading">Bret Lezama</h6>
                                            <p class="notification-text font-small-3 text-muted">I have seen your work, there is</p>
                                            <small>
                                                <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Martes</time>
                                            </small>
                                        </div>
                                    </div>
                                </a>
                                <a href="javascript:void(0)">
                                    <div class="media">
                                        <div class="media-left">
                                            <span class="avatar avatar-sm avatar-online rounded-circle">
                                                <img src="app-assets/images/portrait/small/avatar-s-3.png" alt="avatar">
                                                <i></i>
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading">Carie Berra</h6>
                                            <p class="notification-text font-small-3 text-muted">Can we have call in this week ?</p>
                                            <small>
                                                <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Viernes</time>
                                            </small>
                                        </div>
                                    </div>
                                </a>
                                <a href="javascript:void(0)">
                                    <div class="media">
                                        <div class="media-left">
                                            <span class="avatar avatar-sm avatar-away rounded-circle">
                                                <img src="app-assets/images/portrait/small/avatar-s-6.png" alt="avatar">
                                                <i></i>
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading">Eric Alsobrook</h6>
                                            <p class="notification-text font-small-3 text-muted">We have project party this saturday.</p>
                                            <small>
                                                <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Hace un mes</time>
                                            </small>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-menu-footer">
                                <a class="dropdown-item text-muted text-center" href="chat.jsp">Ver todos</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            <span class="avatar avatar-online">
                                <img src="<%= user.getAvatar() != null ? user.getAvatar() : "img/avatar.png" %>" alt="avatar">
                                <i></i>
                            </span>
                            <span class="user-name"><%= user.getNickname()%></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <%
                                //  User is a person
                                if (null != user.getPerson()) {
                            %>
                            <a class="dropdown-item" href="profile?user=<%= user.getNickname()%>">
                                <i class="ft-user"></i> Mi Perfil</a>
                                <%
                                    }
                                %>
                            <a class="dropdown-item" href="chat.jsp">
                                <i class="ft-mail"></i> Inbox</a>
                            <a class="dropdown-item" href="#">
                                <i class="ft-check-square"></i> Tareas</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="./logout" id="logOutButton">
                                <i class="ft-power"></i> Cerrar Sesión</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- END NAV -->

<!-- START MENU -->
<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <!-- main menu header-->
    <div class="main-menu-header" style="">
        <input type="text" placeholder="Buscar" class="menu-search form-control round">
    </div>
    <!-- / main menu header-->
    <!-- main menu content-->
    <div class="main-menu-content ps-container ps-theme-light" data-ps-id="528e7933-a915-2ba9-a465-f1f9dcee9fbf">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
            <li class="nav-item">
                <a href="dashboard.jsp">
                    <i class="icon-home3"></i>
                    <span data-i18n="nav.dash.main" class="menu-title">Inicio</span>
                </a>
            </li>


            <li class="nav-item has-sub">
                <a href="#">
                    <i class="icon-group"></i>
                    <span data-i18n="nav.project.main" class="menu-title">Personas</span>
                </a>
                <ul class="menu-content">
                    <li>
                        <a href="add-student.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Estudiantes</a>
                    </li>
                    <li>
                        <a href="student.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Colaboradores</a>
                    </li>
                    <li>
                        <a href="student.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Contactos</a>
                    </li></ul>
            </li>

            <li class="nav-item has-sub">
                <a href="#">
                    <i class="icon-university2"></i>
                    <span data-i18n="nav.advance_cards.main" class="menu-title">Academia</span>
                </a>
                <ul class="menu-content">
                    <li>
                        <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Experiencias</a>
                    </li>
                    <li>
                        <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Facultades</a>
                    </li>
                    <li>
                        <a href="student.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Carreras</a>
                    </li></ul>
            </li>

            <li class="nav-item">
                <a href="#">
                    <i class="icon-list-alt"></i>
                    <span data-i18n="nav.components.main" class="menu-title">Encuestas</span>
                </a>

            </li>
            <li class="navigation-header">
                <span data-i18n="nav.category.support">Ajustes</span>
                <i data-toggle="tooltip" data-placement="right" data-original-title="Ajustes"></i>
            </li>
            <li class="nav-item has-sub">
                <a href="#">
                    <i class="icon-cog2"></i>
                    <span data-i18n="nav.components.main" class="menu-title">Seguridad</span>
                </a>
                <ul class="menu-content" style="">
                    <li class="is-shown">
                        <a href="#">
                            <span data-i18n="nav.components.main" class="menu-title">Usuarios</span>
                        </a>

                    </li>
                    <li class="is-shown">
                        <a href="jobs.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Cargos</a>
                    </li>
                    <li class="is-shown">
                        <a href="permissions.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Permisos</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="#">
                    <i class="icon-file-text3"></i>
                    <span data-i18n="nav.support_documentation.main" class="menu-title">Documentación</span>
                </a>
            </li>
        </ul>
        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
    <!-- /main menu content-->
    <!-- main menu footer-->
    <!-- include includes/menu-footer-->
    <!-- main menu footer-->
</div>
<!-- END MENU -->
