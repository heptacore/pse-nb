<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html lang="en-US">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Theme Starz">
    <link href="lp-assets/fonts/font-awesome-4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="lp-assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="lp-assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="lp-assets/css/magnific-popup.css" type="text/css">
    <!--<link rel="stylesheet" href="lp-assets/css/flexslider.css" type="text/css">-->
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/logo/logo.ico">
    <link rel="stylesheet" href="lp-assets/css/style.css" type="text/css">
    <title>Entorno de Seguimiento Académico</title>
</head>

<%
    try {
        //  There's no active session.
        if (null == session.getAttribute("user")) {
            //  There's a URI query
            if (null != request.getQueryString()) {
                String query = request.getParameter("message");
                request.setAttribute("message", query);
            }
%>
<body data-spy="scroll" data-target=".navigation" data-offset="90">
    <!-- Wrapper -->
    <section id="home"></section>
    <div class="wrapper">
        <div class="navigation fixed">
            <div class="container">
                <header class="navbar" id="top" role="banner">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="navbar-brand nav" id="brand">
                            <a href="index.jsp">
                                <img src="lp-assets/img/logo.png" alt="brand">
                            </a>
                        </div>
                    </div>
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="#home">Inicio</a>
                            </li>
                            <li class="custom">
                                &nbsp; | &nbsp;
                            </li>
                            <li>
                                <a href="#features">Seguimiento a Egresados</a>
                            </li>
                            <li class="custom">
                                &nbsp; | &nbsp;
                            </li>
                            <li>
                                <a href="#features-boxes">Academia</a>
                            </li>
                            <li class="custom">
                                &nbsp; | &nbsp;
                            </li>
                            <li>
                                <a href="#footer">Contacto</a>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.navbar collapse-->
                </header>
                <!-- /.navbar -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.navigation -->

        <section id="slider">
            <div class="flexslider loading">
                <div id="loading-icon">
                    <i class="fa fa-cog fa-spin"></i>
                </div>
                <ul class="slides">
                    <li class="slide">
                        <img src="lp-assets/img/slide-01.jpg" alt="">
                    </li>
                    <li class="slide">
                        <img src="lp-assets/img/slide-02.jpg" alt="">
                    </li>
                    <li class="slide">
                        <img src="lp-assets/img/slide-03.jpg" alt="">
                    </li>
                    <!-- <li class="slide">
                        <img src="lp-assets/img/slide-03.jpg" alt="">
                    </li> -->
                </ul>
                <!-- /.slides -->
            </div>
            <!-- /.flexslider -->
            <div class="slider-banner">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-8">
                            <div class="title">
                                <h2>¡Añade Nuevas Experiencias!</h2>
                                <figure class="subtitle">Ingresa ahora para registrar y dar seguimiento a tus experiencias.</figure>
                                <i class="fa fa-arrow-circle-o-right"></i>
                            </div>
                            <!-- /.title -->
                        </div>
                        <!-- /.col-md-8 -->
                        <div class="col-md-4 col-sm-4">
                            <div class="form-slider-wrapper">
                                <header>
                                    <h3>Bienvenido al Entorno de Seguimiento Académico</h3>
                                </header>
                                <hr>
                                <form role="form" id="form-slider">
                                    <div class="form-group">
                                        <label for="name">Usuario
                                            <em>*</em>
                                        </label>
                                        <input type="text" class="form-control" id="user" name="user" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="password">Contraseña
                                            <em>*</em>
                                        </label>
                                        <input type="password" class="form-control" id="password" name="password" required>
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <div id="form-slider-status"></div>
                                        <a type="submit" id="form-slider-submit" class="btn btn-default">Ingresar</a>
                                    </div>
                                    <!-- /.form-group -->
                                </form>
                                <!-- /#form-slider -->
                                <figure>*Campos Requeridos</figure>
                            </div>
                            <!-- /.form-slider-wrapper -->
                        </div>
                        <!-- /.form-slider-wrapper -->
                    </div>
                    <!-- /.col-md-4 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
    </div>
    <!-- /.slider-banner -->
    </section>
    <!-- /#slider -->
    <section id="features" class="block">
        <div class="container">
            <header>
                <h3>Seguimiento a Egresados</h3>
            </header>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <ul class="features-list">
                        <li>Posicionamiento Laboral</li>
                        <li>Empresas Asociadas</li>
                        <li>Orientación Académica</li>
                        <li>Seguimiento Post-Grado</li>
                        <li>Enfoque Profesional</li>
                    </ul>
                    <!-- /.features-list -->
                </div>
                <!-- /.col-md-8 -->
                <div class="col-md-8 col-sm-8">
                    <div class="owl-carousel testimonials-carousel"></div>
                    <div class="image-carousel">
                        <div class="image-carousel-slide">
                            <div class="row">
                                <div class="col-md-5 col-sm-5">
                                    <a href="lp-assets/img/features-slide-01.jpg" class="image-popup">
                                        <img src="lp-assets/img/features-slide-01.jpg" alt="">
                                    </a>
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <h4>Proyecto de Seguimiento a Egresados UCA</h4>
                                    <p align="justify">Desde hace varios años se ha venido fortaleciendo a nivel internacional la tendencia
                                        de evaluación de la actividad universitaria, como una forma de rendición de cuentas
                                        a la sociedad y a los gobiernos. En ese contexto de evaluación, el seguimiento de
                                        egresados es un asunto de vital importancia para las universidades, ya que el desempeño
                                        profesional y personal de los egresados permite establecer indicadores con respecto
                                        a la calidad y eficiencia de las instituciones de educación superior.
                                    </p>
                                    <a href="lp-assets/img/box-image-02.jpg" class="btn btn-framed image-popup">Ver Imagen
                                        <span class="fa fa-search-plus"></span>
                                    </a>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.image-carousel-slide -->
                        <div class="image-carousel-slide">
                            <div class="row">
                                <div class="col-md-5 col-sm-5">
                                    <a href="http://vimeo.com/24506451" class="video-popup">
                                        <img src="lp-assets/img/features-slide-02.jpg" alt="">
                                    </a>
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <h4>Plan Estratégico PSE-UCA</h4>
                                    <p align="justify">PSE fue desarrollado con el propósito de evaluar, de manera sistemática, la pertinencia
                                        de sus procesos formativos, así como el papel que juegan sus egresados en el desarrollo
                                        de su entorno. Esto permitirá contar con mejores indicadores para retroalimentar
                                        el currículo, en función de las nuevas exigencias que plantean los ámbitos social
                                        y productivo del mundo, el país y la región; así como fortalecer la formación de
                                        excelentes profesionales capaces de asimilar las transformaciones del entorno y responder
                                        de manera propositiva e innovadora.
                                    </p>
                                    <a href="http://vimeo.com/24506451" class="btn btn-framed video-popup">Reproducir Video
                                        <span class="fa fa-search-plus"></span>
                                    </a>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.image-carousel-slide -->
                        <div class="image-carousel-slide">
                            <a href="lp-assets/img/box-image-02.jpg" class="image-popup">
                                <img src="lp-assets/img/features-slide-03.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <!-- /.image-carousel -->
                </div>
                <!-- /.col-md-8 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /#features .block -->
    <section id="features-boxes" class="block background-color-grey-light">
        <div class="container">
            <header class="custom-header">
                <h3>Nuestra Academia</h3>
            </header>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="feature-box">
                        <header>
                            <img class="center" src="lp-assets/img/icon-01.png" alt="">
                            <h4>Actividades Extracurriculares</h4>
                        </header>
                        <p align="justify">Se ofrece una gran variedad de actividades extracurriculares para todo tipo de disciplinas aportando
                            al desarrollo de los estudiantes para su integración a la vida universitaria y desarrollo de
                            nuevos talentos. Entre estas actividades podemos encontrar Cultura, Deportes, Bellas Artes, Voluntariado
                            Social, Pastoral, etc..
                        </p>
                    </div>
                    <!-- /.feature-box -->
                </div>
                <!-- /.col-md-4 -->
                <div class="col-md-4 col-sm-4">
                    <div class="feature-box">
                        <header>
                            <img class="center" src="lp-assets/img/icon-02.png" alt="">
                            <h4>Intercambio Académico</h4>
                        </header>
                        <p align="justify">La UCA se apoya en un proyecto curricular basado en la pedagogía ignaciana, un cuerpo académico altamente
                            calificado y en constante capacitación; en las tecnologías, la innovación y en el trabajo en
                            red con organizaciones nacionales e internacionales, instituciones públicas y privadas y más
                            de 200.
                        </p>
                    </div>
                    <!-- /.feature-box -->
                </div>
                <!-- /.col-md-4 -->
                <div class="col-md-4 col-sm-4">
                    <div class="feature-box">
                        <header>
                            <img class="center" src="lp-assets/img/icon-03.png" alt="">
                            <h4>Grupos de Interés</h4>
                        </header>
                        <p align="justify">Cada Facultad posee un sin numero de grupos de interés apoyados por las asignaturas que se imparten
                            en cada carrera. Muchos de estos grupos tienen como base a comunidades de innovadores con una
                            visión social para el desarrollo del país. Abriendo las puertas para debates, proyectos y capacitaciones
                            según lo ameriten las necesidades del estudiantado.
                        </p>
                    </div>
                    <!-- /.feature-box -->
                </div>
                <!-- /.col-md-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /#features-boxes .block -->
    <section id="boxes" class="block background-color-grey-light">
        <div class="container">
            <div class="box">
                <div class="image width-50 pull-left">
                    <img src="lp-assets/img/box-image-01.jpg" class="top-50" alt="">
                </div>
                <div class="description width-50">
                    <div class="description-wrapper">
                        <header>
                            <h3>Plenty of Room For Everyone
                                <br> Even For Your Car</h3>
                        </header>
                        <p>We understand your privacy. Every person should have his own space where he can live his own private
                            live. Even the smallest apartments are big and space enough to feel comfortable. And we think
                            that your car deserves the same in the garage.</p>
                        <a href="#home" class="btn roll">Be the first to know!</a>
                    </div>
                </div>
                <!-- /.description -->
            </div>
            <!-- /.box -->
            <div class="box">
                <div class="description width-30 pull-left">
                    <div class="description-wrapper top-180">
                        <header>
                            <h3>Spectacular Views</h3>
                        </header>
                        <p>Start your day by enjoying the unforgettable view from your home. Open the window and smell the fresh
                            air of the pure nature.</p>
                        <a href="#home" class="btn roll">Be the first to know!</a>
                    </div>
                </div>
                <!-- /.description -->
                <div class="image width-70">
                    <img src="lp-assets/img/box-image-02.jpg" alt="">
                </div>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /#boxes -->
    <!-- /#properties -->
    <section id="contact" class="block background-color-grey-dark has-dark-background">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <header>
                        <h3>Administrador</h3>
                    </header>
                    <section class="agent">
                        <div class="agent-id">
                            <img src="lp-assets/img/agent.png" alt="">
                            <div class="agent-wrapper">
                                <h4>Armando López</h4>
                                <p>Coordinador Carrera de Ingeniería en Sistemas de Información</p>
                            </div>
                        </div>
                        <div class="agent-contact">
                            <div>
                                <strong>Phone:</strong> (505) 456 789 000</div>
                            <div>
                                <strong>E-mail:</strong>
                                <a href="#">armandol@edu.uca.ni</a>
                            </div>
                            <div>
                                <strong>Skype:</strong> Armando_López</div>
                        </div>
                    </section>
                </div>
                <div class="col-md-8 col-sm-8">
                    <header>
                        <h3>Contáctenos</h3>
                    </header>
                    <form role="form" id="form-contact" method="post" action="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="contact-form-name">Nombre
                                        <em>*</em>
                                    </label>
                                    <input type="text" class="form-control" id="contact-form-name" name="contact-form-name" required>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col-md-6 -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="contact-form-email">Correo
                                        <em>*</em>
                                    </label>
                                    <input type="email" class="form-control" id="contact-form-email" name="contact-form-email" required>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col-md-6 -->
                        </div>
                        <!-- /.row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="contact-form-message">Mensaje
                                        <em>*</em>
                                    </label>
                                    <textarea class="form-control" id="contact-form-message" rows="6" name="contact-form-message" required></textarea>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col-md-12 -->
                        </div>
                        <!-- /.row -->
                        <div class="form-group">
                            <button type="submit" class="btn pull-right btn-large" id="form-contact-submit">Enviar</button>
                        </div>
                        <!-- /.form-group -->
                        <div id="form-status"></div>
                    </form>
                    <!-- /#form-contact -->
                </div>
            </div>
        </div>
    </section>
    <footer id="footer">
        <div class="container">
            <figure>Universidad Centroamericana © Todos los Derechos Reservados</figure>
        </div>
    </footer>
    </div>
    <!-- end Wrapper -->
    <script type="text/javascript" src="lp-assets/js/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="lp-assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="lp-assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="lp-assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="lp-assets/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="lp-assets/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="lp-assets/js/retina-1.1.0.min.js"></script>
    <script type="text/javascript" src="lp-assets/js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="lp-assets/js/jquery.fitvids.js"></script>
    <script type="text/javascript" src="lp-assets/js/scrollReveal.min.js"></script>

    <script type="text/javascript" src="lp-assets/js/custom.js"></script>
    
    <script>
        $(document).ready(function(){
            $("#form-slider-submit").click(function() {                
                $.post("login", $("#form-slider").serialize(), function(data){
                    $(document.body).css({'cursor' : 'wait'});
                    if (data === "1") {
                        window.location.replace("dashboard.jsp");
                    }
                    $(document.body).css({'cursor' : 'default'});
                });
            }); 
        });
    </script>
    
    <%
            try {
                if (null != request.getAttribute("message")) {
    %>
    <script>
        $(document).ready(function(){
            $('html, body').animate({
                scrollTop: $("#user").offset().top-150
            }, 2000);
            
            $("#user").focus();
        });
    </script>
    <%
                }
            } catch (Exception e) {
                System.out.println("Ha ocurrido un error en ./index.jsp: " + e);
            }
    %>

    <%
            } else {
                response.sendRedirect("dashboard.jsp");
            }
        } catch (Exception e) {
            System.out.println("Ha ocurrido un error en ./index.jsp: " + e);
        }
    %>
</body>