/**
 * Creates the necesary elements for Interest Groups
 * just before the element that triggers the function.
 * Most usually a button tag element.
 * @param {element} object to prepend elements.
 */
function appendInterestGroupFields(object) {
    object.prepend();
    /*<label for="projectinput6">Carrera</label>
    <select id=""  name="<%= CAREER %>" class="custom-form-control" style="width: 123%">
        <option value="<%= firstFaculty.getCareers().get(i).getID() %>"><%= firstFaculty.getCareers().get(i).getName() %></option>
    </select>*/
}

/**
 * Deletes the specified group of
 * elements necesary to create an
 * Intereest Group.
 * @param {type} object object to be deleted.
 * @returns {undefined}
 */
function deleteInterestGroupFields(object) {
    
}