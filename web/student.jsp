<%@page import="data.StudentData"%>
<%@page import="model.Student"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template.">
        <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
        <meta name="author" content="PIXINVENT">
        <title>Entorno de Seguimiento Académico</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/logo/logo.ico">
        <link href="app-assets/fonts/font-awesome-4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/icheck/icheck.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/forms/icheck/custom.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/js/gallery/photo-swipe/photoswipe.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/js/gallery/photo-swipe/default-skin/default-skin.css">
        <!-- END VENDOR CSS-->
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="app-assets/fonts/icomoon.css">
        <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
        <!-- BEGIN ROBUST CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
        <!-- END ROBUST CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/users.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/pages/timeline.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/calendars/clndr.css">
        <link rel="stylesheet" type="text/css" href="app-assets/fonts/meteocons/style.min.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <!-- TODO Incluir en un archivo CSS -->
        <style>
            .table tbody > tr:hover {
                cursor:hand;
                color:blue;
            }
        </style>
        <!-- END Custom CSS-->
    </head>

    <%
        /**
         * Validate session/access to page acoording to user in order to show/hide
         * some data, functionalities and parameters.
         */

        // [PROCESS][1]: Validate Session
        // If there's an active session.
        if (null != session.getAttribute("user")) {
            
    %>
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu"
          data-col="2-columns">

        <!-- fixed-top-->
        <!-- fixed-top-->
        <%--
        [PROCESS][2]: Load Navigation and Menu
        --%>
        <div id="nav-placeholder">
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <!-- main menu-->
        <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
            <!-- main menu header-->
            <div class="main-menu-header">
                <input type="text" placeholder="Buscar" class="menu-search form-control round" />
            </div>
            <!-- / main menu header-->
            <!-- main menu content-->
            <div class="main-menu-content">
                <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
                    <li class=" nav-item">
                        <a href="dashboard.jsp">
                            <i class="icon-home3"></i>
                            <span data-i18n="nav.dash.main" class="menu-title">Inicio</span>
                        </a>
                    </li>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-user-tie"></i>
                            <span data-i18n="nav.project.main" class="menu-title">Colaboradores</span>
                        </a>
                        <ul class="menu-content">
                            <li>
                                <a href="add-employee.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Colaborador</a>
                            </li>
                            <li>
                                <a href="employee.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Colaboradores</a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-group"></i>
                            <span data-i18n="nav.project.main" class="menu-title">Estudiantes</span>
                        </a>
                        <ul class="menu-content">
                            <li>
                                <a href="add-student.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Estudiante</a>
                            </li>
                            <li>
                                <a href="student.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Estudiantes</a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-ios-albums-outline"></i>
                            <span data-i18n="nav.cards.main" class="menu-title">Carreras</span>
                        </a>
                        <ul class="menu-content">
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Carrera</a>
                            </li>
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Carreras</a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-university2"></i>
                            <span data-i18n="nav.advance_cards.main" class="menu-title">Facultades</span>
                        </a>
                        <ul class="menu-content">
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Facultad</a>
                            </li>
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Facultades</a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-star6"></i>
                            <span data-i18n="nav.content.main" class="menu-title">Experiencias</span>
                        </a>
                        <ul class="menu-content">
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Agregar Experiencia</a>
                            </li>
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Experiencias</a>
                            </li>
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Ver Solicitudes</a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-list-alt"></i>
                            <span data-i18n="nav.components.main" class="menu-title">Encuestas</span>
                        </a>
                        <ul class="menu-content">
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Crear Encuesta</a>
                            </li>
                            <li>
                                <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Encuestas</a>
                            </li>
                        </ul>
                    </li>
                    <li class="navigation-header">
                        <span data-i18n="nav.category.support">Ajustes</span>
                        <i data-toggle="tooltip" data-placement="right" data-original-title="Ajustes"></i>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-cog2"></i>
                            <span data-i18n="nav.components.main" class="menu-title">Seguridad</span>
                        </a>
                        <ul class="menu-content">
                            <li class=" nav-item">
                                <a href="#">
                                    <span data-i18n="nav.components.main" class="menu-title">Usuarios</span>
                                </a>
                                <ul class="menu-content">
                                    <li>
                                        <a href="coming-soon-flat.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Registrar Usuario</a>
                                    </li>
                                    <li>
                                        <a href="user.jsp" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Gestionar Usuarios</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="jobs.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Cargos</a>
                            </li>
                            <li>
                                <a href="permissions.html" data-i18n="nav.other_pages.coming_soon.coming_soon_flat" class="menu-item">Permisos</a>
                            </li>
                        </ul>
                    </li>
                    <li class=" nav-item">
                        <a href="#">
                            <i class="icon-file-text3"></i>
                            <span data-i18n="nav.support_documentation.main" class="menu-title">Documentación</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /main menu content-->
            <!-- main menu footer-->
            <!-- include includes/menu-footer-->
            <!-- main menu footer-->
        </div>
        <!-- / main menu-->

        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row">
                    <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                        <h3 class="content-header-title mb-0 d-inline-block">Estudiantes</h3>
                        <div class="row breadcrumbs-top d-inline-block">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="dashboard.jsp">Inicio</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="student.jsp">Gestionar Estudiantes</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="content-header-right col-md-4 col-12">
                        <div class="btn-group float-md-right">
                            <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="icon-settings mr-1"></i>Opciones</button>
                            <div class="dropdown-menu arrow">
                                <a class="dropdown-item" href="#">
                                    <i class="icon-calendar-plus-o mr-1"></i> Calendario</a>
                                <a class="dropdown-item" href="#">
                                    <i class="icon-wrench4 mr-1"></i> Soporte</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">
                                    <i class="icon-file-text3 mr-1"></i> Documentación</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-body">
                    <!-- Column selectors table -->
                    <section id="column-selectors">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Todos los Estudiantes</h3>
                                        <a class="heading-elements-toggle">
                                            <i class="fa fa-ellipsis-v font-medium-3"></i>
                                        </a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li>
                                                    <a data-action="collapse">
                                                        <i class="ft-minus"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-action="reload">
                                                        <i class="ft-rotate-cw"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-action="expand">
                                                        <i class="ft-maximize"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <%--
                                            [PROCESS][3]: Load Students Table with all students.
                                            --%>
                                            <table class="table table-striped table-bordered dataex-html5-selectors">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre</th>
                                                        <!--<th>Usuario</th>-->
                                                        <th>Carnet</th>
                                                        <th>Cédula</th>
                                                        <th>Género</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%
                                                        StudentData studentData = new StudentData();
                                                        ArrayList<Student> students = new ArrayList<Student>();
                                                        
                                                        //  [PROCESS][3.1]: Get Data
                                                        students = studentData.getStudentsPage(1, 20);
                                                        for (int i = 0; i < students.size(); i++) {
                                                    %>
                                                    <tr class="table-row" data="<%= students.get(i).getStudentIdNumber() != null ? students.get(i).getStudentIdNumber() : students.get(i).getExStudentIdNumber() %>">
                                                        <td><%= students.get(i).getName() %></td>
                                                        <td><%= students.get(i).getStudentIdNumber() %></td>
                                                        <td><%= students.get(i).getIdCardNumber() %></td>
                                                        <td><%= students.get(i).getGender() %></td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Nombre</th>
                                                        <th>Carnet</th>
                                                        <th>Carnet Ex-Alumno</th>
                                                        <th>Género</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--/ Column selectors table -->
                </div>
            </div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <div class="customizer border-left-blue-grey border-left-lighten-4 d-none d-xl-block">
            <a class="customizer-close" href="#">
                <i class="ft-x font-medium-3"></i>
            </a>
            <a class="customizer-toggle bg-danger box-shadow-3" href="#">
                <i class="ft-settings font-medium-3 spinner white"></i>
            </a>
            <div class="customizer-content p-2">
                <h4 class="text-uppercase mb-0">Theme Customizer</h4>
                <hr>
                <p>Customize & Preview in Real Time</p>
                <h5 class="mt-1 mb-1 text-bold-500">Menu Color Options</h5>
                <div class="form-group">
                    <!-- Outline Button group -->
                    <div class="btn-group customizer-sidebar-options" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-outline-info" data-sidebar="menu-light">Light Menu</button>
                        <button type="button" class="btn btn-outline-info" data-sidebar="menu-dark">Dark Menu</button>
                    </div>
                </div>
                <hr>
                <h5 class="mt-1 text-bold-500">Layout Options</h5>
                <ul class="nav nav-tabs nav-underline nav-justified layout-options">
                    <li class="nav-item">
                        <a class="nav-link layouts active" id="baseIcon-tab21" data-toggle="tab" aria-controls="tabIcon21" href="#tabIcon21" aria-expanded="true">Layouts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navigation" id="baseIcon-tab22" data-toggle="tab" aria-controls="tabIcon22" href="#tabIcon22" aria-expanded="false">Navigation</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navbar" id="baseIcon-tab23" data-toggle="tab" aria-controls="tabIcon23" href="#tabIcon23" aria-expanded="false">Navbar</a>
                    </li>
                </ul>
                <div class="tab-content px-1 pt-1">
                    <div role="tabpanel" class="tab-pane active" id="tabIcon21" aria-expanded="true" aria-labelledby="baseIcon-tab21">
                        <p>
                        <div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="collapsed-sidebar" id="collapsed-sidebar">
                            <label class="custom-control-label" for="collapsed-sidebar">Collapsed Menu</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="fixed-layout" id="fixed-layout">
                            <label class="custom-control-label" for="fixed-layout">Fixed Layout</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="boxed-layout" id="boxed-layout">
                            <label class="custom-control-label" for="boxed-layout">Boxed Layout</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="static-layout" id="static-layout">
                            <label class="custom-control-label" for="static-layout">Static Layout</label>
                        </div>
                        </p>
                    </div>
                    <div class="tab-pane" id="tabIcon22" aria-labelledby="baseIcon-tab22">
                        <p>
                        <div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="native-scroll" id="native-scroll">
                            <label class="custom-control-label" for="native-scroll">Native Scroll</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="right-side-icons" id="right-side-icons">
                            <label class="custom-control-label" for="right-side-icons">Right Side Icons</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="bordered-navigation" id="bordered-navigation">
                            <label class="custom-control-label" for="bordered-navigation">Bordered Navigation</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="flipped-navigation" id="flipped-navigation">
                            <label class="custom-control-label" for="flipped-navigation">Flipped Navigation</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="collapsible-navigation" id="collapsible-navigation">
                            <label class="custom-control-label" for="collapsible-navigation">Collapsible Navigation</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="static-navigation" id="static-navigation">
                            <label class="custom-control-label" for="static-navigation">Static Navigation</label>
                        </div>
                        </p>
                    </div>
                    <div class="tab-pane" id="tabIcon23" aria-labelledby="baseIcon-tab23">
                        <p>
                        <div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="brand-center" id="brand-center">
                            <label class="custom-control-label" for="brand-center">Brand Center</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="navbar-static-top" id="navbar-static-top">
                            <label class="custom-control-label" for="navbar-static-top">Static Top</label>
                        </div>
                        </p>
                    </div>
                </div>
                <hr>
                <h5 class="mt-1 text-bold-500">Navigation Color Options</h5>
                <ul class="nav nav-tabs nav-underline nav-justified color-options">
                    <li class="nav-item">
                        <a class="nav-link nav-semi-light active" id="color-opt-1" data-toggle="tab" aria-controls="clrOpt1" href="#clrOpt1" aria-expanded="false">Semi Light</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-semi-dark" id="color-opt-2" data-toggle="tab" aria-controls="clrOpt2" href="#clrOpt2" aria-expanded="false">Semi Dark</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-dark" id="color-opt-3" data-toggle="tab" aria-controls="clrOpt3" href="#clrOpt3" aria-expanded="false">Dark</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-light" id="color-opt-4" data-toggle="tab" aria-controls="clrOpt4" href="#clrOpt4" aria-expanded="true">Light</a>
                    </li>
                </ul>
                <div class="tab-content px-1 pt-1">
                    <div role="tabpanel" class="tab-pane active" id="clrOpt1" aria-expanded="true" aria-labelledby="color-opt-1">
                        <div class="row">
                            <div class="col-6">
                                <h6>Solid</h6>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-blue-grey" data-bg="bg-blue-grey" id="default">
                                    <label class="custom-control-label" for="default">Default</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-primary" data-bg="bg-primary" id="primary">
                                    <label class="custom-control-label" for="primary">Primary</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-danger" data-bg="bg-danger" id="danger">
                                    <label class="custom-control-label" for="danger">Danger</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-success" data-bg="bg-success" id="success">
                                    <label class="custom-control-label" for="success">Success</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-blue" data-bg="bg-blue" id="blue">
                                    <label class="custom-control-label" for="blue">Blue</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan" id="cyan">
                                    <label class="custom-control-label" for="cyan">Cyan</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-pink" data-bg="bg-pink" id="pink">
                                    <label class="custom-control-label" for="pink">Pink</label>
                                </div>
                                </p>
                            </div>
                            <div class="col-6">
                                <h6>Gradient</h6>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" checked class="custom-control-input bg-blue-grey" data-bg="bg-gradient-x-grey-blue"
                                           id="bg-gradient-x-grey-blue">
                                    <label class="custom-control-label" for="bg-gradient-x-grey-blue">Default</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-primary" data-bg="bg-gradient-x-primary" id="bg-gradient-x-primary">
                                    <label class="custom-control-label" for="bg-gradient-x-primary">Primary</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-danger" data-bg="bg-gradient-x-danger" id="bg-gradient-x-danger">
                                    <label class="custom-control-label" for="bg-gradient-x-danger">Danger</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-success" data-bg="bg-gradient-x-success" id="bg-gradient-x-success">
                                    <label class="custom-control-label" for="bg-gradient-x-success">Success</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-blue" data-bg="bg-gradient-x-blue" id="bg-gradient-x-blue">
                                    <label class="custom-control-label" for="bg-gradient-x-blue">Blue</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-cyan" data-bg="bg-gradient-x-cyan" id="bg-gradient-x-cyan">
                                    <label class="custom-control-label" for="bg-gradient-x-cyan">Cyan</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-pink" data-bg="bg-gradient-x-pink" id="bg-gradient-x-pink">
                                    <label class="custom-control-label" for="bg-gradient-x-pink">Pink</label>
                                </div>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="clrOpt2" aria-labelledby="color-opt-2">
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" checked class="custom-control-input bg-default" data-bg="bg-default" id="opt-default">
                            <label class="custom-control-label" for="opt-default">Default</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-primary" data-bg="bg-primary" id="opt-primary">
                            <label class="custom-control-label" for="opt-primary">Primary</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-danger" data-bg="bg-danger" id="opt-danger">
                            <label class="custom-control-label" for="opt-danger">Danger</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-success" data-bg="bg-success" id="opt-success">
                            <label class="custom-control-label" for="opt-success">Success</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-blue" data-bg="bg-blue" id="opt-blue">
                            <label class="custom-control-label" for="opt-blue">Blue</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan" id="opt-cyan">
                            <label class="custom-control-label" for="opt-cyan">Cyan</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-pink" data-bg="bg-pink" id="opt-pink">
                            <label class="custom-control-label" for="opt-pink">Pink</label>
                        </div>
                        </p>
                    </div>
                    <div class="tab-pane" id="clrOpt3" aria-labelledby="color-opt-3">
                        <div class="row">
                            <div class="col-6">
                                <h3>Solid</h3>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-blue-grey" data-bg="bg-blue-grey" id="solid-blue-grey">
                                    <label class="custom-control-label" for="solid-blue-grey">Default</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-primary" data-bg="bg-primary" id="solid-primary">
                                    <label class="custom-control-label" for="solid-primary">Primary</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-danger" data-bg="bg-danger" id="solid-danger">
                                    <label class="custom-control-label" for="solid-danger">Danger</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-success" data-bg="bg-success" id="solid-success">
                                    <label class="custom-control-label" for="solid-success">Success</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-blue" data-bg="bg-blue" id="solid-blue">
                                    <label class="custom-control-label" for="solid-blue">Blue</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan" id="solid-cyan">
                                    <label class="custom-control-label" for="solid-cyan">Cyan</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-pink" data-bg="bg-pink" id="solid-pink">
                                    <label class="custom-control-label" for="solid-pink">Pink</label>
                                </div>
                                </p>
                            </div>

                            <div class="col-6">
                                <h3>Gradient</h3>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-blue-grey" data-bg="bg-gradient-x-grey-blue" id="bg-gradient-x-grey-blue1">
                                    <label class="custom-control-label" for="bg-gradient-x-grey-blue1">Default</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked class="custom-control-input bg-primary" data-bg="bg-gradient-x-primary" id="bg-gradient-x-primary1">
                                    <label class="custom-control-label" for="bg-gradient-x-primary1">Primary</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked class="custom-control-input bg-danger" data-bg="bg-gradient-x-danger" id="bg-gradient-x-danger1">
                                    <label class="custom-control-label" for="bg-gradient-x-danger1">Danger</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked class="custom-control-input bg-success" data-bg="bg-gradient-x-success" id="bg-gradient-x-success1">
                                    <label class="custom-control-label" for="bg-gradient-x-success1">Success</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked class="custom-control-input bg-blue" data-bg="bg-gradient-x-blue" id="bg-gradient-x-blue1">
                                    <label class="custom-control-label" for="bg-gradient-x-blue1">Blue</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked class="custom-control-input bg-cyan" data-bg="bg-gradient-x-cyan" id="bg-gradient-x-cyan1">
                                    <label class="custom-control-label" for="bg-gradient-x-cyan1">Cyan</label>
                                </div>
                                </p>
                                <p>
                                <div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked class="custom-control-input bg-pink" data-bg="bg-gradient-x-pink" id="bg-gradient-x-pink1">
                                    <label class="custom-control-label" for="bg-gradient-x-pink1">Pink</label>
                                </div>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="clrOpt4" aria-labelledby="color-opt-4">
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-blue-grey" data-bg="bg-blue-grey bg-lighten-4" id="light-blue-grey">
                            <label class="custom-control-label" for="light-blue-grey">Default</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-primary" data-bg="bg-primary bg-lighten-4" id="light-primary">
                            <label class="custom-control-label" for="light-primary">Primary</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-danger" data-bg="bg-danger bg-lighten-4" id="light-danger">
                            <label class="custom-control-label" for="light-danger">Danger</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-success" data-bg="bg-success bg-lighten-4" id="light-success">
                            <label class="custom-control-label" for="light-success">Success</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-blue" data-bg="bg-blue bg-lighten-4" id="light-blue">
                            <label class="custom-control-label" for="light-blue">Blue</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan bg-lighten-4" id="light-cyan">
                            <label class="custom-control-label" for="light-cyan">Cyan</label>
                        </div>
                        </p>
                        <p>
                        <div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-pink" data-bg="bg-pink bg-lighten-4" id="light-pink">
                            <label class="custom-control-label" for="light-pink">Pink</label>
                        </div>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                      <p>Some text in the modal.</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer footer-static footer-light navbar-border">
            <p class="clearfix text-muted text-sm-center mb-0 px-2">
                <span class="float-md-left d-xs-block d-md-inline-block">Copyright &copy; 2018
                    <a href="http://www.uca.edu.ni/" target="_blank" class="text-bold-800 grey darken-2">Universidad Centroamericana</a>, Todos los derechos reservados. </span>
                <span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted & Made with
                    <i class="icon-heart5 pink"></i>
                </span>
            </p>
        </footer>

        <!-- BEGIN VENDOR JS-->
        <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN ROBUST JS-->
        <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="app-assets/js/core/app.js" type="text/javascript"></script>
        <script src="app-assets/js/scripts/customizer.js" type="text/javascript"></script>
        <!-- END ROBUST JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="app-assets/js/scripts/pages/chat-application.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
        <script type="text/javascript" src="websocket.js"></script>
        <script type="text/javascript" src="chat.js"></script>
        <script>
            $(function () {
                $("#nav-placeholder").load("nav.jsp");
            });
            
            $(document).ready(function(){
                $(".table-row").click(function() {
                    $.post("getStudent", { student: $(this).attr("data")}, function(data){
                        <!-- FIXME Does not load content from servlet -->
                        $("#myModal").load(data);
                        $("#myModal").modal('show');
                    });
                });
            });
        </script>
    </body>
    <%
        } else {
            response.sendRedirect("index.jsp");
        }
    %>
</html>