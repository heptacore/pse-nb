<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="data.Student_Data" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pruebas de la capa de modelo</title>
</head>
<body>
		<%
		Student_Data data = Student_Data.getInstance();
		//data.createData(new ArrayList<Student>());
		data.readData();
		%>
	<table>
		<tr>
			<th>Primer Nombre</th>
			<th>Segundo Nombre</th>
			<th>Primer Apellido</th>
			<th>Segundo Apellido</th>
			<th>C�dula</th>
		</tr>
		<%
		for (int i = 0; i < data.getStudents().size(); i++) {
			out.println("<tr>");
			out.println("<td>"+data.getStudents().get(i).getFirstName()+"</td>");
			out.println("<td>"+data.getStudents().get(i).getLastName()+"</td>");
			out.println("<td>"+data.getStudents().get(i).getSurname()+"</td>");
			out.println("<td>"+data.getStudents().get(i).getSecondSurname()+"</td>");
			out.println("<td>"+data.getStudents().get(i).getIdCardNumber()+"</td>");
			out.println("</tr>");
		}
		%>
	</table>
</body>
</html>