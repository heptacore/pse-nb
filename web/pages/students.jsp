<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@page import="data.StudentData" %>
<%@page import="utilities.StringFormatter" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
        <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="PIXINVENT">
        <title>PSE - Estudiantes</title>
        <link rel="apple-touch-icon" sizes="60x60" href="../app-assets/images/ico/apple-icon-60.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../app-assets/images/ico/apple-icon-76.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../app-assets/images/ico/apple-icon-120.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../app-assets/images/ico/apple-icon-152.png">
        <link rel="shortcut icon" type="image/x-icon" href="../app-assets/images/ico/favicon.ico">
        <link rel="shortcut icon" type="image/png" href="../app-assets/images/ico/favicon-32.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.css">
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="../app-assets/fonts/icomoon.css">
        <link rel="stylesheet" type="text/css" href="../app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/pace.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN ROBUST CSS-->
        <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="../app-assets/css/app.css">
        <link rel="stylesheet" type="text/css" href="../app-assets/css/colors.css">
        <!-- END ROBUST CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-overlay-menu.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
        <!-- END Custom CSS-->
        <link rel="stylesheet" href="bootstrap-select.css" />

    </head>

    <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

        <!-- main menu-->
        <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
            <!-- main menu header-->
            <div class="main-menu-header">
                <input type="text" placeholder="Search" class="menu-search form-control round" />
            </div>
            <!-- / main menu header-->
            <!-- main menu content-->
            <div class="main-menu-content">
                <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
                    <li class=" nav-item"><a href="../index.html"><i class="icon-home3"></i><span data-i18n="nav.dash.main" class="menu-title">Inicio</span><span class="tag tag tag-primary tag-pill float-xs-right mr-2">2</span></a>
                    </li>
                    <li class=" nav-item"><a href="students.jsp"><i class="icon-head"></i><span data-i18n="nav.students" class="menu-title">Estudiantes</span></a>
                    </li>
                    <li class=" nav-item"><a href="employees.jsp"><i class="icon-head"></i><span data-i18n="nav.employees" class="menu-title">Colaboradores</span></a>
                    </li>
                </ul>
            </div>
            <!-- /main menu content-->
            <!-- main menu footer-->
            <!-- include includes/menu-footer-->
            <!-- main menu footer-->
        </div>
        <!-- / main menu-->

        <div class="app-content content container-fluid">
            <div class="content-wrapper">
                <div class="content-header row">
                    <div class="content-header-left col-md-6 col-xs-12 mb-1">
                        <h2 class="content-header-title">Registro Estudiantil</h2>
                    </div>
                    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
                        <div class="breadcrumb-wrapper col-xs-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="../index.html">Inicio</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Estudiantes</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="content-body">
                    <!-- Table head options start -->
                    <style>
                        /* bootstrap */

                        a {

                            color: #373a3c;
                        }

                        .pagination {
                            display: inline-block;
                            padding-left: 0;
                            margin-top: 1rem;
                            margin-bottom: 1rem;
                            margin: 1rem 3%;
                            border-radius: 0.18rem;
                            float: right;
                        }

                        .page-link {
                            border: none;
                            color: #373a3c;
                        }

                        .page-link:hover {
                            color: #373a3c;
                        }

                        .page-item.active .page-link,
                        .page-item.active .page-link:focus {
                            background-color: #373a3c;
                            border-color: #373a3c;
                        }

                        .mediumContainer {
                            position: relative;
                            float: left;

                            width: 50%;
                            margin: 0;
                            padding: 0;
                        }

                        .mediumContainer a:not([href]):not([tabindex]) {
                            margin: 0;
                            padding: 0;
                        }

                        /* color.css */

                        .border-primary {
                            border: 1px solid #373a3c !important;
                        }

                        .btn-primary {
                            background: #373a3c !important;

                        }

                        .popupEdit {
                            z-index: 9999;
                        }

                    </style>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Estudiantes</h4>
                                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                            <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body collapse in">


                                    <div class="table-responsive">
                                        <form class="navbar-form navbar-left mt-1" role="search">

                                            <div class="col-lg-6">
                                                <div class="col-lg-8 col-sm-6 col-xs-8">
                                                    <div class="form-group">
                                                        <input type="text" id="userinput1" class="form-control border-primary" placeholder="Name" name="name">
                                                    </div>
                                                </div>


                                                <div class="btn-group mr-1 mb-1">
                                                    <button type="button" class="btn btn-primary border-primary">Filtrar</button>
                                                    <button type="button" class="btn btn-primary dropdown-toggle border-primary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="#">ID</a>
                                                        <a class="dropdown-item" href="#">Nombre</a>
                                                        <a class="dropdown-item" href="#">Carrera</a>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                        <table class="table table-hover">
                                            <thead class="thead-inverse">
                                                <tr>
                                                    <th>#</th>
                                                    <th>ID</th>
                                                    <th>Nombre</th>
                                                    <th>Género</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%
                                                    StudentData data = StudentData.getInstance();
                                                    int studentsAmmount = 0;
                                                    int pages;
                                                    StringFormatter stringHelper = StringFormatter.getInstance();
                                                    data.readData();
                                                    studentsAmmount = data.getStudents().size();
                                                    int studentCareers;
                                                    int results = 0;
                                                    String studentName;
                                                    for (int i = 0; i < studentsAmmount; i++) {
                                                        if (data.getStudents().get(i).getCareers().isEmpty() == false) {
                                                            studentCareers = data.getStudents().get(i).getCareers().size();
                                                            for (int j = 0; j < studentCareers; j++) {
                                                                if (data.getStudents().get(i).getCareers().get(j).getID() == 1) {
                                                                    results++;
                                                                    studentName = stringHelper.validateString(data.getStudents().get(i).getFirstName(), "");
                                                                    studentName = studentName + " " + stringHelper.validateString(data.getStudents().get(i).getLastName(), "");
                                                                    studentName = studentName + " " + stringHelper.validateString(data.getStudents().get(i).getSurname(), "");
                                                                    studentName = studentName + " " + stringHelper.validateString(data.getStudents().get(i).getLastSurname(), "");
                                                %>
                                                <tr>
                                                    <th scope="row"><%=i + 1%></th>
                                                    <td><%= stringHelper.validateString(data.getStudents().get(i).getStudentIdNumber(), "-") %></td>
                                                    <td><%= studentName %></td>
                                                    <td><%= stringHelper.validateString(data.getStudents().get(i).getGender(), "-") %></td>
                                                    <td>
                                                        <div class="mediumContainer">
                                                            <a class="btn-block btn-lg view-modal" data-toggle="modal" data-target="#Previewregistry" title="Visualizer">
                                                                <i class="icon-eye6"></i>
                                                            </a>
                                                        </div>
                                                        <div class="mediumContainer">
                                                            <a class="btn-block btn-lg" data-toggle="modal" data-target="#mi-modal" title="Eliminar">
                                                                <i class="icon-cross2"></i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <%
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (results == 0) {
                                                        %>
                                                        <tr>
                                                            <th>No se encontraron resultados.<th>
                                                        </tr>
                                                        <%
                                                    }
                                                %>
                                            </tbody>
                                            <!--Pagination-->
                                        </table>
                                        <%
                                            if (results > 20) {
                                                pages = (int) results / 20;
                                            %>
                                        <nav aria-label="pagination example">
                                            <ul class="pagination .text-xs-right">

                                                <!--Arrow left-->
                                                <li class="page-item disabled">
                                                    <a class="page-link" href="#" aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>

                                                <li class="page-item active">
                                                    <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
                                                </li>
                                                <%
                                                    for (int i = 0; i < pages; i++) {
                                                        
                                                    %>
                                                <li class="page-item"><a class="page-link" href="#"><%= i + 1 %></a></li>
                                                <%
                                                    }
                                                    %>
                                                <li class="page-item">
                                                    <a class="page-link" href="#" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                            <%
                                                }
                                                %>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Table head options end -->

                    <!-- Modal Eliminar-->
                    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
                        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Desea eliminar el regitro</h4>
                                </div>
                                <div class="modal-footer">
                                    <div class="mediumContainer">
                                        <button type="button" class="btn btn-default btn-min-width mr-1 mb-1" id="modal-btn-si">Si</button>
                                    </div>
                                    <div class="mediumContainer">
                                        <button type="button" class="btn btn-primary btn-min-width ml-1 mb-1" id="modal-btn-no">No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Modal-->

                    <!-- Modal Edit -->
                    <div class="modal fade  popupEdit" id="academiclnfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3323" aria-hidden="true">
                        <div class="modal-dialog modal-md" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <label class="modal-title text-text-bold-600" id="myModalLabel3323">Modificar Informacion Academica</label>
                                </div>
                                <div class="card-block">

                                    <form class="form">
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="icon-head"></i>Informacion Academica<i class="icon-cog3"></i></h4>

                                            <div class="row">
                                                <div class="col-sm-12 col-md-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                                            <div class="form-group">
                                                                <h4>Carrera</h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                                            <div class="col-sm-6 col-md-6 col-xs-6">

                                                                <div class="form-group">
                                                                    <select class="selectpicker form-control border-primary" data-live-search="true">
                                                                        <option >Arquitectura</option>
                                                                        <option >Marketing</option>
                                                                        <option >Ing. en sistemas de informacion</option>
                                                                        <option >Mario Flores</option>
                                                                        <option >Don Young</option>
                                                                        <option disabled="disabled">Marvin Martinez</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3 col-xs-3">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control border-primary" placeholder="Generacion">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3 col-xs-3">

                                                                <div class="form-group">
                                                                    <a class="ml-1 mt-1" title="Agregar Carrera"><i class="icon-plus3"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                                            <div class="col-sm-6 col-md-6 col-xs-6">
                                                                <div class="form-group">
                                                                    <label>Ing. en sistemas de Informacion</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3 col-xs-3">
                                                                <div class="form-group">
                                                                    <label>2018</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3 col-xs-3">
                                                                <div class="form-group">
                                                                    <a class="ml-1" title="Editar Carrera"><i class="icon-edit"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Cancelar">
                                            <input type="submit" class="btn btn-primary btn-lg" value="Actualizar">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade  popupEdit" id="personalnfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                        <div class="modal-dialog modal-md" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <label class="modal-title text-text-bold-600" id="myModalLabel33">Modificar Informacion Personal</label>
                                </div>
                                <div class="card-block">

                                    <form class="form">
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="icon-head"></i>Informacion personal<i class="icon-cog3"></i></h4>

                                            <div class="row">
                                                <div class="col-sm-12 col-md-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                                            <div class="form-group">
                                                                <h4>Nombre</h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                                            <div class="col-sm-9 col-md-9 col-xs-9">
                                                                <div class="form-group">
                                                                    <label>Byron Rocha Rodriguez</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3 col-xs-3">
                                                                <div class="form-group">
                                                                    <a class="ml-1" title="Editar Nombre"><i class="icon-edit"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                                            <div class="form-group">
                                                                <h4>Identificación</h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                                            <div class="form-group">
                                                                <div class="col-sm-12 col-md-12 col-xs-12">
                                                                    <div class="col-sm-3 col-md-3 col-xs-3">

                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control border-primary" placeholder="Tipo Identificación">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6 col-md-6 col-xs-6">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control border-primary" placeholder="Identificación">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3 col-md-3 col-xs-3">

                                                                        <div class="form-group">
                                                                            <a class="ml-1 mt-1" title="Agregar Identificación"><i class="icon-plus3"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 col-md-12 col-xs-12">
                                                                    <div class="col-sm-3 col-md-3 col-xs-3">
                                                                        <div class="form-group">
                                                                            <strong>ID: </strong>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6 col-md-6 col-xs-6">
                                                                        <div class="form-group">
                                                                            <label>000003243</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3 col-md-3 col-xs-3">

                                                                        <div class="form-group">
                                                                            <a class="ml-1" title="Editar Identificación"><i class="icon-edit"></i></a>
                                                                            <a class="ml-1" title="Eliminar Identificación"><i class="icon-close-round"></i></a>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 col-md-12 col-xs-12">
                                                                    <div class="col-sm-3 col-md-3 col-xs-3">

                                                                        <div class="form-group">
                                                                            <strong>Cedula: </strong>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6 col-md-6 col-xs-6">
                                                                        <div class="form-group">
                                                                            <label>489-280897-0000Q</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3 col-md-3 col-xs-3">

                                                                        <div class="form-group">
                                                                            <a class="ml-1" title="Editar Identificación"><i class="icon-edit"></i></a>
                                                                            <a class="ml-1" title="Eliminar Identificación"><i class="icon-close-round"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-xs-12">
                                                    <div class="col-sm-12 col-md-12 col-xs-12">
                                                        <div class="form-group">
                                                            <h4>Correo</h4>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-12 col-xs-12">
                                                        <div class="col-sm-9 col-md-9 col-xs-9">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control border-primary" placeholder="Correo">

                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 col-xs-3">
                                                            <div class="form-group">
                                                                <a class="ml-1 mt-1" title="Agregar Correo"><i class="icon-plus3"></i></a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-12 col-xs-12">
                                                        <div class="col-sm-9 col-md-9 col-xs-9">
                                                            <div class="form-group">
                                                                <label>byronrochaxxx@gmail.com</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 col-xs-3">
                                                            <div class="form-group">
                                                                <a class="ml-1" title="Editar Correo"><i class="icon-edit"></i></a>
                                                                <a class="ml-1" title="Eliminar Correo"><i class="icon-close-round"></i></a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-12 col-xs-12">
                                                        <div class="col-sm-9 col-md-9 col-xs-9">
                                                            <div class="form-group">
                                                                <label>byron.roha815@est.uca.edu.ni</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 col-xs-3">
                                                            <div class="form-group">
                                                                <a class="ml-1" title="Editar Correo"><i class="icon-edit"></i></a>
                                                                <a class="ml-1" title="Eliminar Correo"><i class="icon-close-round"></i></a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-xs-12">
                                                    <div class="col-sm-12 col-md-12 col-xs-12">
                                                        <div class="form-group">
                                                            <h4>Celular</h4>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-12 col-xs-12">
                                                        <div class="col-sm-9 col-md-9 col-xs-9">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control border-primary" placeholder="Celular">

                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 col-xs-3">
                                                            <div class="form-group">
                                                                <a class="ml-1 mt-1" title="Agregar Celular"><i class="icon-plus3"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-12 col-xs-12">
                                                        <div class="col-sm-9 col-md-9 col-xs-9">
                                                            <div class="form-group">
                                                                <label>43546534</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 col-xs-3">
                                                            <div class="form-group">
                                                                <a class="ml-1" title="Editar Celular"><i class="icon-edit"></i></a>
                                                                <a class="ml-1" title="Eliminar Celular"><i class="icon-close-round"></i></a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-12 col-xs-12">
                                                        <div class="col-sm-9 col-md-9 col-xs-9">
                                                            <div class="form-group">
                                                                <label>87029392</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 col-xs-3">
                                                            <div class="form-group">
                                                                <a class="ml-1" title="Editar Celular"><i class="icon-edit"></i></a>
                                                                <a class="ml-1" title="Eliminar Celular"><i class="icon-close-round"></i></a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Cancelar">
                                            <input type="submit" class="btn btn-primary btn-lg" value="Actualizar">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal Visualizar Registro -->
                    <div class="modal fade  text-xs-left" id="Previewregistry" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h2 class="modal-title text-text-bold-800" id="myModalLabel34">Estudiante</h2>
                                </div>
                                <div class="card-block">
                                    <form class="form">
                                        <div class="form-body">
                                            <h4 class="form-section"><i class="icon-head"></i>Informacion personal
                                                <a class="btn" data-toggle="modal" data-target="#personalnfo" title="Editar"><i class="icon-pencil"></i></a>
                                            </h4>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <div class="col-sm-12 col-md-12 col-xs-12">
                                                            <div class="form-group">
                                                                <img class="" src="../app-assets/images/profileplaceholder.jpg" alt=" Generic placeholder image " style="width: 100%;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                            <div class="form-group ">
                                                                <h3>Byron Rocha Rodriguez</h3>
                                                                <label>Masculino</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                            <div class="form-group ">
                                                                <h4>Identificación</h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                            <div class="form-group ">
                                                                <label><strong>ID: </strong>000003243</label><br>
                                                                <label><strong>cedula: </strong>488-332323-0000E</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 ">
                                                    <div class="form-group ">
                                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                            <div class="form-group ">
                                                                <h4>Correo</h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                            <div class="form-group ">
                                                                <a>byronrochaxxx@gmail.com</a><br>
                                                                <a>byron.roha815@est.uca.edu.ni</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                            <div class="form-group ">
                                                                <h4>Celular</h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                            <div class="form-group ">
                                                                <a>87029392</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                            <h4 class="form-section "><i class="icon-clipboard4 "></i>Informacion Academica
                                                <a class="btn" data-toggle="modal" data-target="#academiclnfo" title="Editar"><i class="icon-pencil"></i></a>
                                            </h4>
                                            <div class="row ">
                                                <div class="col-md-6 ">
                                                    <div class="form-group ">
                                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                            <div class="form-group ">
                                                                <h4>Carrera</h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                            <div class="form-group ">
                                                                <label>Ing. en sistemas de informacion</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 ">
                                                    <div class="form-group ">
                                                        <div class="col-md-12 ">
                                                            <div class="col-sm-6 col-md-6 col-xs-6 ">
                                                                <div class="form-group ">
                                                                    <h4>Carrera</h4>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-xs-6 ">
                                                                <div class="form-group ">
                                                                    <h4>Generación </h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 ">
                                                            <div class="col-sm-6 col-md-6 col-xs-6 ">
                                                                <div class="form-group ">
                                                                    <label>Ing. en sistemas de informacion</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-xs-6 ">
                                                                <div class="form-group ">
                                                                    <label>2018 </label><br>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="form-section "><i class="icon-clipboard4 "></i>Informacion Extracurricular
                                                <a class="btn" data-toggle="modal" data-target="#academiclnfo" title="Editar"><i class="icon-pencil"></i></a></h4>


                                            <div class="row ">
                                                <div class="col-md-6 ">
                                                    <div class="form-group ">
                                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                            <div class="form-group ">
                                                                <h4>Deportes</h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                            <div class="form-group ">
                                                                <label>Basketball</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 ">
                                                    <div class="form-group ">
                                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                            <div class="form-group ">
                                                                <h4>Palmáres</h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                                                            <div class="form-group ">
                                                                <label>Basketball</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


<footer class="footer footer-static footer-light navbar-border ">
    <p class="clearfix text-muted text-sm-center mb-0 px-2 "><span class="float-md-left d-xs-block d-md-inline-block ">Copyright  © 2017 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent " target="_blank " class="text-bold-800
                                                                                                                                            grey darken-2 ">PIXINVENT </a>, All rights reserved. </span>
        <span class="float-md-right d-xs-block d-md-inline-block ">Hand-crafted &amp; Made with <i class="icon-heart5 pink "></i></span>
    </p>
</footer>

<!-- BEGIN VENDOR JS-->
<script src="../app-assets/js/core/libraries/jquery.min.js " type="text/javascript "></script>
<script src="../app-assets/vendors/js/ui/tether.min.js " type="text/javascript "></script>
<script src="../app-assets/js/core/libraries/bootstrap.min.js " type="text/javascript "></script>
<script src="bootstrap-select.js"></script>
<script src="../app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js " type="text/javascript "></script>
<script src="../app-assets/vendors/js/ui/unison.min.js " type="text/javascript "></script>
<script src="../app-assets/vendors/js/ui/blockUI.min.js " type="text/javascript "></script>
<script src="../app-assets/vendors/js/ui/jquery.matchHeight-min.js " type="text/javascript "></script>
<script src="../app-assets/vendors/js/ui/screenfull.min.js " type="text/javascript "></script>
<script src="../app-assets/vendors/js/extensions/pace.min.js " type="text/javascript "></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN ROBUST JS-->
<script src="../app-assets/js/core/app-menu.js " type="text/javascript "></script>
<script src="../app-assets/js/core/app.js " type="text/javascript "></script>

<script type="text/javascript ">
    $('.selectpicker').selectpicker({
    style: 'btn-info',
    size: 4
    });

    var modalConfirm = function(callback) {
    $("#modal-btn-si ").on("click ", function() {
    callback(true);
    $("#mi-modal ").modal('hide');
    });

    $("#modal-btn-no ").on("click ", function() {
    callback(false);
    $("#mi-modal ").modal('hide');
    });
    };

    modalConfirm(function(confirm) {
    if (confirm) {
    alert("Eliminado ");
    } else {
    alert("Accon cancelada ");

    }
    });

</script>
<!-- END ROBUST JS-->
<!-- BEGIN PAGE LEVEL JS-->
<!-- END PAGE LEVEL JS-->
<!-- BEGIN JQUERY -->
<script>
    $(document).ready(function(){
        $(".view-modal").click(function(){
            $.get('../login', function(data){
                alert(data);
            });
        });
    });
</script>
<!-- END JQUERY -->
</body>

</html>