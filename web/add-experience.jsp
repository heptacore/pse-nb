<%-- 
    Document   : add-experience
    Created on : 07-may-2018, 14:41:42
    Author     : Oliver
--%>

<%@page import="data.ExperienceData"%>
<%@page import="java.sql.SQLException"%>
<%@page import="data.JobData"%>
<%@page import="model.Job"%>
<%@page import="model.Stakeholder"%>
<%@page import="data.StakeholderData"%>
<%@page import="data.EmployeeData"%>
<%@page import="data.StudentData"%>
<%@page import="model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="loaded" lang="en" data-textdirection="ltr"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template.">
        <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
        <meta name="author" content="PIXINVENT">
        <title>Entorno de Seguimiento Académico</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/logo/logo.ico">
        <link href="app-assets/fonts/font-awesome-4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN ROBUST CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
        <!-- END ROBUST CSS-->
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="app-assets/fonts/icomoon.css">
        <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <!-- END Custom CSS-->
        <style>
            tr.table-row:hover {
                color: blue;
                cursor: pointer;
            }
        </style>
    </head>

    <%--
        Validate that the steps are in order and there is a session
    --%>
    <%
        try {
            //  There is an active session
            if (null != session.getAttribute("user")) {
                User myUser = (User) session.getAttribute("user");
                System.out.println("[DEBUG]: Se ha superado el objeto user");
                StudentData studentData = new StudentData();
                StakeholderData stakeholderData = new StakeholderData();
                ExperienceData experienceData = new ExperienceData();
                JobData jobData = new JobData();
                EmployeeData employeeData = new EmployeeData();
                // 0 = User/Person, 1 = Student, 2 = Coordinator (Stakeholder)
                int userType = 0;
                //  User is a person
                if (null != myUser.getPerson()) {
                    //  User is a student
                    if (null != studentData.readData(myUser.getPerson().getID())) {
                        userType = 1;
                        System.out.println("[DEBUG]: el usuario es estudiante");
                    }
                    System.out.println("Se ha superado la condicion de estudiante");
                    //  User is a stakeholder
                    if (null != stakeholderData.readData(myUser.getPerson().getID())) {
                        Stakeholder stakeholder = stakeholderData.readData(myUser.getPerson().getID());
                        int coordinatorID = 1;
                        boolean isCoordinator = false;
                        System.out.println("[DEBUG]: El usuario es stakeholder");
                        for (Job job : stakeholder.getJobs()) {
                            if (job.getID() == coordinatorID) {
                                isCoordinator = true;
                                System.out.println(job.getID() + " = " + coordinatorID);
                            }
                        }
                        Job o = jobData.fetchJob(coordinatorID);

                        //  Job Object is not NULL
                        if (isCoordinator == true) {
                            userType = 2;
                            System.out.println("[DEBUG]: El usuario es Coordinador");
                        }
                    }
                    System.out.println("Se ha superado la condicion de stakeholder/coordinador");
                }

                //  User is a student or a Coordinator (stakeholder)
                if (userType == 1 || userType == 2) {
                    //  There are experiences
                    if (experienceData.getAllExperiences().size() > 0) {
    %>
    <body class="vertical-layout vertical-menu 2-columns fixed-navbar  menu-expanded pace-done" data-open="click" data-menu="vertical-menu" data-col="2-columns"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div>

        <!-- fixed-top-->
        <div id="nav-placeholder">
        </div>
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row">
                    <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                        <h3 class="content-header-title mb-0 d-inline-block">Experiencias</h3>
                        <div class="row breadcrumbs-top d-inline-block">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="dashboard.jsp">Inicio</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="#">Registrar Experiencia</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="content-header-right col-md-4 col-12">
                        <div class="btn-group float-md-right">
                            <button class="btn btn-info" id="experiencesButton" type="button" >
                                <i class="icon-settings mr-1"></i>Experiencias
                            </button>
                        </div>
                    </div>
                </div>
                <div class="content-body">
                    <!-- Basic form layout section start -->
                    <section id="basic-form-layouts">
                        <div class="row match-height">
                            <div class="custom-col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <a class="heading-elements-toggle">
                                            <i class="fa fa-ellipsis-v font-medium-3"></i>
                                        </a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li>
                                                    <a data-action="collapse">
                                                        <i class="ft-minus"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-action="reload">
                                                        <i class="ft-rotate-cw"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-action="expand">
                                                        <i class="ft-maximize"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show" style="">
                                        <div class="custom-card-body">
                                            <form class="form" action="./addStudent" method="POST">
                                                <div id="form-body" class="form-body">
                                                    <h4 class="form-section">
                                                        <i class="ft-user"></i>Experiencia
                                                    </h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group" data-ac-repeater="is" data-ac-repeater="Add Who is" data-ac-repeater-text="Agregar Cohorte" buttonStyle="style='margin-top: 10px;' type='button' class='btn btn-primary'">
                                                                <label for="projectinput1">Primer Nombre</label>
                                                                <select id="projectinput1" name="gender" class="custom-form-control">
                                                                    <option value="masculino">Masculino</option>
                                                                    <option value="femenino">Femenino</option>
                                                                </select>
                                                            </div>
                                                            <button type="button" data-repeater-create="" class="btn btn-primary">
                                                                    <i class="ft-plus"></i> Agregar Nuevo Correo
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Primer Nombre</label>
                                                                <input type="text" id="projectinput1" class="custom-form-control" placeholder="Ingrese su Nombre" name="firstName">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput2">Segundo Nombre</label>
                                                                <input type="text" id="projectinput2" class="custom-form-control" placeholder="Ingrese su Nombre" name="lastName">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Primer Apellido</label>
                                                                <input type="text" id="projectinput1" class="custom-form-control" placeholder="Ingrese su Apellido" name="surname">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput2">Segundo Apellido</label>
                                                                <input type="text" id="projectinput2" class="custom-form-control" placeholder="Ingrese su Apellido" name="lastSurname">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput5">Género</label>
                                                                <select id="projectinput5" name="gender" class="custom-form-control">
                                                                    <option value="masculino">Masculino</option>
                                                                    <option value="femenino">Femenino</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput6">Cédula</label>
                                                                <input type="text" id="projectinput6" class="custom-form-control" placeholder="Ingrese su cédula" name="idCardNumber">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="projectinput3">Correo Electrónico</label>
                                                            <div class="form-group custom-col-12 mb-2 contact-repeater">

                                                                <div data-repeater-list="repeater-group">
                                                                    <div class="input-group mb-1" data-repeater-item="">
                                                                        <input name="repeater-group[0][email]" type="email" placeholder="Ingrese su Correo" class="custom-form-control" id="email">
                                                                        <span class="input-group-append" id="button-addon2">
                                                                            <button class="btn btn-danger" type="button" data-repeater-delete="">
                                                                                <i class="ft-x"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                                <button type="button" data-repeater-create="" class="btn btn-primary">
                                                                    <i class="ft-plus"></i> Agregar Nuevo Correo
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="projectinput3">Teléfono</label>
                                                            <div class="form-group custom-col-12 mb-2 contact-repeater">

                                                                <div data-repeater-list="repeater-group">
                                                                    <div class="input-group mb-1" data-repeater-item="">
                                                                        <input name="repeater-group[0][phone]" type="tel" placeholder="Ingrese su Teléfono" class="custom-form-control" id="example-tel-input">
                                                                        <span class="input-group-append" id="button-addon2">
                                                                            <button class="btn btn-danger" type="button" data-repeater-delete="">
                                                                                <i class="ft-x"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                                <button type="button" data-repeater-create="" class="btn btn-primary">
                                                                    <i class="ft-plus"></i> Agregar Nuevo Teléfono
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <button id="formCancel" type="button" class="btn btn-warning mr-1">
                                                            <i class=" ft-x"></i> Cancelar
                                                        </button>
                                                        <button id="formSave" type="button" class="btn btn-primary">
                                                            <i class="icon-check"></i> Guardar
                                                        </button>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div></section>
                    <!-- // Basic form layout section end -->
                </div>
            </div>
        </div>
        
        <!-- Begin Modals -->
        <div class="modal fade" role="dialog" id="experiencesModal">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body" id="experiencesModalBody">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="studentsModal">
        </div>
        <!-- End Modals -->

        <footer class="footer footer-static footer-light navbar-border">
            <p class="clearfix text-muted text-sm-center mb-0 px-2">
                <span class="float-md-left d-xs-block d-md-inline-block">Copyright © 2018
                    <a href="http://www.uca.edu.ni/" target="_blank" class="text-bold-800 grey darken-2">Universidad Centroamericana</a>, Todos los derechos reservados. </span>
                <span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted &amp; Made with
                    <i class="icon-heart5 pink"></i>
                </span>
            </p>
        </footer>

        <!-- BEGIN VENDOR JS-->
        <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="app-assets/vendors/js/forms/repeater/jquery.repeater.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN ROBUST JS-->
        <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="app-assets/js/core/app.js" type="text/javascript"></script>
        <script src="app-assets/js/scripts/customizer.js" type="text/javascript"></script>
        <!-- END ROBUST JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="app-assets/js/scripts/forms/form-repeater.js" type="text/javascript"></script>
        <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
        <!-- Begin Form Repeater -->
        <script src="lp-assets/js/jquery.ac-form-field-repeater.js" type="text/javascript"></script>
        <!-- End Form Repeater -->
        <script>
            $(document).ready(function () {
                $("#nav-placeholder").load("nav.jsp");

                //  Begin Modal Functions
                $("#experiencesButton").click(function(){
                    $("#experiencesModalBody").load("getExperiences");
                    $("#experiencesModal").modal('show');
                });
            });
        </script>
        <script>
            function loadForm(evt) {
                $("#form-body").load("getExperienceForm", {experience : evt});
            }
        </script>

        <!-- For file input
        <script>
            $("#file-1").fileinput({
                theme: 'fa',
                uploadUrl: '#', // you must set a valid URL here else you will get an error
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                overwriteInitial: false,
                maxFileSize: 1000,
                maxFilesNum: 10,
                //allowedFileTypes: ['image', 'video', 'flash'],
                slugCallback: function (filename) {
                    return filename.replace('(', '_').replace(']', '_');
                }
            });
        </script>
        -->

        <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/form-layout-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 17:46:42 GMT -->



        <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/form-layout-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 17:46:42 GMT -->



        <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/form-layout-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 17:46:42 GMT -->



        <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/form-layout-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 17:46:42 GMT -->



        <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/form-layout-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 17:46:42 GMT -->




        <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/form-layout-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 17:46:42 GMT -->

        <div id="lumio-bubble-anchor">

        </div>
    </body>
    <%
                    } else {
                        response.sendRedirect("dashboard.jsp");
                    }
                } else {
                    response.sendRedirect("dashboard.jsp");
                }
            } else {
                response.sendRedirect("index.jsp");
            }
        } catch (Exception e) {
            System.out.println("[ERROR] [add-experience.jsp] The following was thrown:" + e);
            response.sendRedirect("dashboard.jsp");
        }
    %>
</html>