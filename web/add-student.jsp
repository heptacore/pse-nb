<%@page import="model.Faculty"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="data.CareerData"%>
<%@page import="data.FacultyData"%>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template.">
        <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
        <meta name="author" content="PIXINVENT">
        <title>Entorno de Seguimiento Académico</title>
        <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/logo/logo.ico">
        <link href="app-assets/fonts/font-awesome-4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/daterange/daterangepicker.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/pickers/pickadate/pickadate.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN ROBUST CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
        <!-- END ROBUST CSS-->
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="app-assets/fonts/icomoon.css">
        <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
        <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/pickers/daterange/daterange.css">
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <!-- END Custom CSS-->
    </head>

    <%
        /*
        Code executed before html body tag is loaded. Use this block of code
        in order to perform validations x
        */
        //  There's an active session
        if (null != session.getAttribute("user")) {
            FacultyData facultyData = new FacultyData();
            //  Form field's name variables
            String FIRST_NAME = "firstName";
            String LAST_NAME = "lastName";
            String SURNAME = "surname";
            String LAST_SURNAME = "lastSurname";
            String GENDER = "gender";
            String PHONE = "phone";
            String EMAIL = "email";
            String FACULTY = "faculty";
            String CAREER = "career";
            String COHORT = "cohort";
            String ID_STUDENT = "idStudent";
            String ID_EX_STUDENT = "idExStudent";
            String ID_CARD = "idCardNumber";
            String NICKNAME = "nickname";
            String PASSWORD = "password";
            String SERVLET_ADD_STUDENT = "./addStudent";
            
    %>
    <body class="vertical-layout vertical-menu 2-columns fixed-navbar  menu-expanded pace-done" data-open="click" data-menu="vertical-menu" data-col="2-columns"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div></div>

        <!-- fixed-top-->
        <div id="nav-placeholder">
        </div>

        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row">
                    <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                        <h3 class="content-header-title mb-0 d-inline-block">Estudiantes</h3>
                        <div class="row breadcrumbs-top d-inline-block">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="dashboard.jsp">Inicio</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="#">Agregar Estudiante</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="content-header-right col-md-4 col-12">
                        <div class="btn-group float-md-right">
                            <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="icon-settings mr-1"></i>Opciones</button>
                            <div class="dropdown-menu arrow">
                                <a class="dropdown-item" href="#">
                                    <i class="icon-calendar-plus-o mr-1"></i> Calendario</a>
                                <a class="dropdown-item" href="#">
                                    <i class="icon-wrench4 mr-1"></i> Soporte</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">
                                    <i class="icon-file-text3 mr-1"></i> Documentación</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-body">
                    <!-- Basic form layout section start -->
                    <section id="basic-form-layouts">
                        <div class="row match-height">
                            <div class="custom-col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <a class="heading-elements-toggle">
                                            <i class="fa fa-ellipsis-v font-medium-3"></i>
                                        </a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li>
                                                    <a data-action="collapse">
                                                        <i class="ft-minus"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-action="reload">
                                                        <i class="ft-rotate-cw"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-action="expand">
                                                        <i class="ft-maximize"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="custom-card-body">
                                            <form class="form" action="<%= SERVLET_ADD_STUDENT %>" method="POST">
                                                <div class="form-body">
                                                    <h4 class="form-section">
                                                        <i class="ft-user"></i>Información Personal</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Primer Nombre</label>
                                                                <input type="text" id="projectinput1" class="custom-form-control" placeholder="Ingrese su Nombre" name="<%= FIRST_NAME %>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput2">Segundo Nombre</label>
                                                                <input type="text" id="projectinput2" class="custom-form-control" placeholder="Ingrese su Nombre" name="<%= LAST_NAME %>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Primer Apellido</label>
                                                                <input type="text" id="projectinput1" class="custom-form-control" placeholder="Ingrese su Apellido" name="<%= SURNAME %>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput2">Segundo Apellido</label>
                                                                <input type="text" id="projectinput2" class="custom-form-control" placeholder="Ingrese su Apellido" name="<%= LAST_SURNAME %>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput5">Género</label>
                                                                <select id="projectinput5" name="<%= GENDER %>" class="custom-form-control">
                                                                    <option value="masculino">Masculino</option>
                                                                    <option value="femenino">Femenino</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput6">Cédula</label>
                                                                <input type="text" id="projectinput6" class="custom-form-control" placeholder="Ingrese su cédula" name="<%= ID_CARD %>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label for="projectinput3">Correo Electrónico</label>
                                                            <div class="form-group custom-col-12 mb-2 contact-repeater">

                                                                <div data-repeater-list="repeater-group">
                                                                    <div class="input-group mb-1" data-repeater-item="">
                                                                        <input name="<%= EMAIL %>" type="email" placeholder="Ingrese su Correo" class="custom-form-control" id="email">
                                                                        <span class="input-group-append" id="button-addon2">
                                                                            <button class="btn btn-danger" type="button" data-repeater-delete="">
                                                                                <i class="ft-x"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                                <button type="button" data-repeater-create="" class="btn btn-primary">
                                                                    <i class="ft-plus"></i> Agregar Nuevo Correo
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for="projectinput3">Teléfono</label>
                                                            <div class="form-group custom-col-12 mb-2 contact-repeater">

                                                                <div data-repeater-list="repeater-group">
                                                                    <div class="input-group mb-1" data-repeater-item="">
                                                                        <input name="<%= PHONE %>" type="tel" placeholder="Ingrese su Teléfono" class="custom-form-control" id="example-tel-input">
                                                                        <span class="input-group-append" id="button-addon2">
                                                                            <button class="btn btn-danger" type="button" data-repeater-delete="">
                                                                                <i class="ft-x"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                                <button type="button" data-repeater-create="" class="btn btn-primary">
                                                                    <i class="ft-plus"></i> Agregar Nuevo Teléfono
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4 class="form-section">
                                                        <i class="icon-graduation-cap"></i>Información Académica</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput4">ID Estudiante</label>
                                                                <input type="text" id="projectinput4" class="custom-form-control" placeholder="Ingrese su ID" name="<%= ID_STUDENT %>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput4">ID ExEstudiante</label>
                                                                <input type="text" id="projectinput4" class="custom-form-control" placeholder="Ingrese su ID" name="<%= ID_EX_STUDENT %>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group custom-col-12 mb-2 contact-repeater">

                                                        <div data-repeater-list="repeater-group">
                                                            <div class="input-group mb-1" data-repeater-item style="border-top: 1px solid #dadada;padding-top:5px">
                                                                <div class="row custom-col-md-12 col-sm-12">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="projectinput2">Cohorte</label>
                                                                            <input type="month" class="custom-form-control" placeholder="Seleccione una fecha..." name="<%= COHORT %>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <%
                                                                    /*
                                                                    Show faculties and careers per faculty in combos
                                                                    wether there are both in database.
                                                                    */
                                                                    ArrayList<Faculty> faculties = new ArrayList<Faculty>();
                                                                    faculties = facultyData.getFaculties();
                                                                    Faculty firstFaculty = new Faculty();
                                                                    
                                                                    //  At least a faculty was fetch from database.
                                                                    if (faculties.size() > 0) {
                                                                %>
                                                                <div class="row custom-col-md-12 col-sm-12">
                                                                    <div class="col-md-5">
                                                                        <div class="form-group">
                                                                            <label for="projectinput5">Facultad</label>
                                                                            <select  name="<%= FACULTY %>" class="custom-form-control" style="width:88%">                                                                
                                                                <%
                                                                        firstFaculty = faculties.get(0);
                                                                        for (int i = 0; i < faculties.size(); i ++) {
                                                                %>
                                                                                <option value="<%= faculties.get(i).getID() %>"><%= faculties.get(i).getName() %></option>
                                                                <%
                                                                        }
                                                                %>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-5" style="padding-left: 110px">
                                                                        <div class="form-group">
                                                                            <label for="projectinput6">Carrera</label>
                                                                            <select id=""  name="<%= CAREER %>" class="custom-form-control" style="width: 123%">
                                                                <%
                                                                        for (int i = 0; i < firstFaculty.getCareers().size(); i++) {
                                                                %>
                                                                                <option value="<%= firstFaculty.getCareers().get(i).getID() %>"><%= firstFaculty.getCareers().get(i).getName() %></option>
                                                                <%
                                                                        }
                                                                %>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <label for="Buttom Delete">&af;</label>

                                                                        <span class="input-group-append" id="button-addon2" style="padding-left: 45px; padding-top: 2px;">
                                                                            <button class="btn btn-danger" type="button" data-repeater-delete>
                                                                                <i class="ft-x"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>                                                                
                                                                <%                                                                    
                                                                    }
                                                                %>
                                                            </div>
                                                        </div>
                                                        <button type="button" data-repeater-create class="btn btn-primary">
                                                            <i class="ft-plus"></i> Agregar Nuevo Cohorte
                                                        </button>
                                                    </div>
                                                    <h4 class="form-section">
                                                        <i class="icon-graduation-cap"></i>Información de Cuenta</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput7">Usuario</label>
                                                                <input type="text" id="projectinput7" class="custom-form-control" placeholder="Ingrese su ID" name="<%= NICKNAME %>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput8">Contraseña</label>
                                                                <input type="text" id="projectinput8" class="custom-form-control" placeholder="Ingrese su ID" name="<%= PASSWORD %>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <button type="button" class="btn btn-warning mr-1">
                                                            <i class=" ft-x"></i> Cancelar
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">
                                                            <i class="icon-check"></i> Guardar
                                                        </button>
                                                    </div>

                                                </div> </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div></section>
                    <!-- // Basic form layout section end -->
                </div>
            </div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <div class="customizer border-left-blue-grey border-left-lighten-4 d-none d-xl-block">
            <a class="customizer-close" href="#">
                <i class="ft-x font-medium-3"></i>
            </a>
            <a class="customizer-toggle bg-danger box-shadow-3" href="#">
                <i class="ft-settings font-medium-3 spinner white"></i>
            </a>
            <div class="customizer-content p-2 ps-container ps-theme-dark ps-active-y" data-ps-id="8b34b0d6-60d9-6cd1-d9fa-37ee56e4de22">
                <h4 class="text-uppercase mb-0">Theme Customizer</h4>
                <hr>
                <p>Customize &amp; Preview in Real Time</p>
                <h5 class="mt-1 mb-1 text-bold-500">Menu Color Options</h5>
                <div class="form-group">
                    <!-- Outline Button group -->
                    <div class="btn-group customizer-sidebar-options" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-outline-info active" data-sidebar="menu-light">Light Menu</button>
                        <button type="button" class="btn btn-outline-info active" data-sidebar="menu-dark">Dark Menu</button>
                    </div>
                </div>
                <hr>
                <h5 class="mt-1 text-bold-500">Layout Options</h5>
                <ul class="nav nav-tabs nav-underline nav-justified layout-options">
                    <li class="nav-item">
                        <a class="nav-link layouts active" id="baseIcon-tab21" data-toggle="tab" aria-controls="tabIcon21" href="#tabIcon21" aria-expanded="true">Layouts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navigation" id="baseIcon-tab22" data-toggle="tab" aria-controls="tabIcon22" href="#tabIcon22" aria-expanded="false">Navigation</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navbar" id="baseIcon-tab23" data-toggle="tab" aria-controls="tabIcon23" href="#tabIcon23" aria-expanded="false">Navbar</a>
                    </li>
                </ul>
                <div class="tab-content px-1 pt-1">
                    <div role="tabpanel" class="tab-pane active" id="tabIcon21" aria-expanded="true" aria-labelledby="baseIcon-tab21">
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="collapsed-sidebar" id="collapsed-sidebar">
                            <label class="custom-control-label" for="collapsed-sidebar">Collapsed Menu</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="fixed-layout" id="fixed-layout">
                            <label class="custom-control-label" for="fixed-layout">Fixed Layout</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="boxed-layout" id="boxed-layout">
                            <label class="custom-control-label" for="boxed-layout">Boxed Layout</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="static-layout" id="static-layout">
                            <label class="custom-control-label" for="static-layout">Static Layout</label>
                        </div>
                        <p></p>
                    </div>
                    <div class="tab-pane" id="tabIcon22" aria-labelledby="baseIcon-tab22">
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="native-scroll" id="native-scroll">
                            <label class="custom-control-label" for="native-scroll">Native Scroll</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="right-side-icons" id="right-side-icons">
                            <label class="custom-control-label" for="right-side-icons">Right Side Icons</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="bordered-navigation" id="bordered-navigation">
                            <label class="custom-control-label" for="bordered-navigation">Bordered Navigation</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="flipped-navigation" id="flipped-navigation">
                            <label class="custom-control-label" for="flipped-navigation">Flipped Navigation</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="collapsible-navigation" id="collapsible-navigation">
                            <label class="custom-control-label" for="collapsible-navigation">Collapsible Navigation</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="static-navigation" id="static-navigation">
                            <label class="custom-control-label" for="static-navigation">Static Navigation</label>
                        </div>
                        <p></p>
                    </div>
                    <div class="tab-pane" id="tabIcon23" aria-labelledby="baseIcon-tab23">
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="brand-center" id="brand-center">
                            <label class="custom-control-label" for="brand-center">Brand Center</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="navbar-static-top" id="navbar-static-top">
                            <label class="custom-control-label" for="navbar-static-top">Static Top</label>
                        </div>
                        <p></p>
                    </div>
                </div>
                <hr>
                <h5 class="mt-1 text-bold-500">Navigation Color Options</h5>
                <ul class="nav nav-tabs nav-underline nav-justified color-options">
                    <li class="nav-item">
                        <a class="nav-link nav-semi-light active" id="color-opt-1" data-toggle="tab" aria-controls="clrOpt1" href="#clrOpt1" aria-expanded="false">Semi Light</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-semi-dark" id="color-opt-2" data-toggle="tab" aria-controls="clrOpt2" href="#clrOpt2" aria-expanded="false">Semi Dark</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-dark" id="color-opt-3" data-toggle="tab" aria-controls="clrOpt3" href="#clrOpt3" aria-expanded="false">Dark</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-light" id="color-opt-4" data-toggle="tab" aria-controls="clrOpt4" href="#clrOpt4" aria-expanded="true">Light</a>
                    </li>
                </ul>
                <div class="tab-content px-1 pt-1">
                    <div role="tabpanel" class="tab-pane active" id="clrOpt1" aria-expanded="true" aria-labelledby="color-opt-1">
                        <div class="row">
                            <div class="col-6">
                                <h6>Solid</h6>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-blue-grey" data-bg="bg-blue-grey" id="default">
                                    <label class="custom-control-label" for="default">Default</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-primary" data-bg="bg-primary" id="primary">
                                    <label class="custom-control-label" for="primary">Primary</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-danger" data-bg="bg-danger" id="danger">
                                    <label class="custom-control-label" for="danger">Danger</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-success" data-bg="bg-success" id="success">
                                    <label class="custom-control-label" for="success">Success</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-blue" data-bg="bg-blue" id="blue">
                                    <label class="custom-control-label" for="blue">Blue</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan" id="cyan">
                                    <label class="custom-control-label" for="cyan">Cyan</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-pink" data-bg="bg-pink" id="pink">
                                    <label class="custom-control-label" for="pink">Pink</label>
                                </div>
                                <p></p>
                            </div>
                            <div class="col-6">
                                <h6>Gradient</h6>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" checked="" class="custom-control-input bg-blue-grey" data-bg="bg-gradient-x-grey-blue" id="bg-gradient-x-grey-blue">
                                    <label class="custom-control-label" for="bg-gradient-x-grey-blue">Default</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-primary" data-bg="bg-gradient-x-primary" id="bg-gradient-x-primary">
                                    <label class="custom-control-label" for="bg-gradient-x-primary">Primary</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-danger" data-bg="bg-gradient-x-danger" id="bg-gradient-x-danger">
                                    <label class="custom-control-label" for="bg-gradient-x-danger">Danger</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-success" data-bg="bg-gradient-x-success" id="bg-gradient-x-success">
                                    <label class="custom-control-label" for="bg-gradient-x-success">Success</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-blue" data-bg="bg-gradient-x-blue" id="bg-gradient-x-blue">
                                    <label class="custom-control-label" for="bg-gradient-x-blue">Blue</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-cyan" data-bg="bg-gradient-x-cyan" id="bg-gradient-x-cyan">
                                    <label class="custom-control-label" for="bg-gradient-x-cyan">Cyan</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-slight-clr" class="custom-control-input bg-pink" data-bg="bg-gradient-x-pink" id="bg-gradient-x-pink">
                                    <label class="custom-control-label" for="bg-gradient-x-pink">Pink</label>
                                </div>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="clrOpt2" aria-labelledby="color-opt-2">
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" checked="" class="custom-control-input bg-default" data-bg="bg-default" id="opt-default">
                            <label class="custom-control-label" for="opt-default">Default</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-primary" data-bg="bg-primary" id="opt-primary">
                            <label class="custom-control-label" for="opt-primary">Primary</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-danger" data-bg="bg-danger" id="opt-danger">
                            <label class="custom-control-label" for="opt-danger">Danger</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-success" data-bg="bg-success" id="opt-success">
                            <label class="custom-control-label" for="opt-success">Success</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-blue" data-bg="bg-blue" id="opt-blue">
                            <label class="custom-control-label" for="opt-blue">Blue</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan" id="opt-cyan">
                            <label class="custom-control-label" for="opt-cyan">Cyan</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-sdark-clr" class="custom-control-input bg-pink" data-bg="bg-pink" id="opt-pink">
                            <label class="custom-control-label" for="opt-pink">Pink</label>
                        </div>
                        <p></p>
                    </div>
                    <div class="tab-pane" id="clrOpt3" aria-labelledby="color-opt-3">
                        <div class="row">
                            <div class="col-6">
                                <h3>Solid</h3>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-blue-grey" data-bg="bg-blue-grey" id="solid-blue-grey">
                                    <label class="custom-control-label" for="solid-blue-grey">Default</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-primary" data-bg="bg-primary" id="solid-primary">
                                    <label class="custom-control-label" for="solid-primary">Primary</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-danger" data-bg="bg-danger" id="solid-danger">
                                    <label class="custom-control-label" for="solid-danger">Danger</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-success" data-bg="bg-success" id="solid-success">
                                    <label class="custom-control-label" for="solid-success">Success</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-blue" data-bg="bg-blue" id="solid-blue">
                                    <label class="custom-control-label" for="solid-blue">Blue</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan" id="solid-cyan">
                                    <label class="custom-control-label" for="solid-cyan">Cyan</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-pink" data-bg="bg-pink" id="solid-pink">
                                    <label class="custom-control-label" for="solid-pink">Pink</label>
                                </div>
                                <p></p>
                            </div>

                            <div class="col-6">
                                <h3>Gradient</h3>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" class="custom-control-input bg-blue-grey" data-bg="bg-gradient-x-grey-blue" id="bg-gradient-x-grey-blue1">
                                    <label class="custom-control-label" for="bg-gradient-x-grey-blue1">Default</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked="" class="custom-control-input bg-primary" data-bg="bg-gradient-x-primary" id="bg-gradient-x-primary1">
                                    <label class="custom-control-label" for="bg-gradient-x-primary1">Primary</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked="" class="custom-control-input bg-danger" data-bg="bg-gradient-x-danger" id="bg-gradient-x-danger1">
                                    <label class="custom-control-label" for="bg-gradient-x-danger1">Danger</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked="" class="custom-control-input bg-success" data-bg="bg-gradient-x-success" id="bg-gradient-x-success1">
                                    <label class="custom-control-label" for="bg-gradient-x-success1">Success</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked="" class="custom-control-input bg-blue" data-bg="bg-gradient-x-blue" id="bg-gradient-x-blue1">
                                    <label class="custom-control-label" for="bg-gradient-x-blue1">Blue</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked="" class="custom-control-input bg-cyan" data-bg="bg-gradient-x-cyan" id="bg-gradient-x-cyan1">
                                    <label class="custom-control-label" for="bg-gradient-x-cyan1">Cyan</label>
                                </div>
                                <p></p>
                                <p>
                                </p><div class="d-inline-block custom-control custom-radio">
                                    <input type="radio" name="nav-dark-clr" checked="" class="custom-control-input bg-pink" data-bg="bg-gradient-x-pink" id="bg-gradient-x-pink1">
                                    <label class="custom-control-label" for="bg-gradient-x-pink1">Pink</label>
                                </div>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="clrOpt4" aria-labelledby="color-opt-4">
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-blue-grey" data-bg="bg-blue-grey bg-lighten-4" id="light-blue-grey">
                            <label class="custom-control-label" for="light-blue-grey">Default</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-primary" data-bg="bg-primary bg-lighten-4" id="light-primary">
                            <label class="custom-control-label" for="light-primary">Primary</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-danger" data-bg="bg-danger bg-lighten-4" id="light-danger">
                            <label class="custom-control-label" for="light-danger">Danger</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-success" data-bg="bg-success bg-lighten-4" id="light-success">
                            <label class="custom-control-label" for="light-success">Success</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-blue" data-bg="bg-blue bg-lighten-4" id="light-blue">
                            <label class="custom-control-label" for="light-blue">Blue</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-cyan" data-bg="bg-cyan bg-lighten-4" id="light-cyan">
                            <label class="custom-control-label" for="light-cyan">Cyan</label>
                        </div>
                        <p></p>
                        <p>
                        </p><div class="d-inline-block custom-control custom-radio">
                            <input type="radio" name="nav-light-clr" class="custom-control-input bg-pink" data-bg="bg-pink bg-lighten-4" id="light-pink">
                            <label class="custom-control-label" for="light-pink">Pink</label>
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 658px; right: 3px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 522px;"></div></div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 658px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 522px;"></div></div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 658px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 522px;"></div></div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 658px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 522px;"></div></div><div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 658px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 522px;"></div></div></div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <footer class="footer footer-static footer-light navbar-border">
            <p class="clearfix text-muted text-sm-center mb-0 px-2">
                <span class="float-md-left d-xs-block d-md-inline-block">Copyright © 2018
                    <a href="http://www.uca.edu.ni/" target="_blank" class="text-bold-800 grey darken-2">Universidad Centroamericana</a>, Todos los derechos reservados. </span>
                <span class="float-md-right d-xs-block d-md-inline-block">Hand-crafted &amp; Made with
                    <i class="icon-heart5 pink"></i>
                </span>
            </p>
        </footer>

        <!-- BEGIN VENDOR JS-->
        <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
        <!-- BEGIN VENDOR JS-->
        <!-- BEGIN PAGE VENDOR JS-->
        <script src="app-assets/vendors/js/forms/repeater/jquery.repeater.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js" type="text/javascript"></script>
        <script src="app-assets/vendors/js/pickers/daterange/daterangepicker.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN ROBUST JS-->
        <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="app-assets/js/core/app.js" type="text/javascript"></script>
        <script src="app-assets/js/scripts/customizer.js" type="text/javascript"></script>
        <!-- END ROBUST JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script src="app-assets/js/scripts/forms/form-repeater.js" type="text/javascript"></script>
        <script src="app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL JS-->
        <script>
            $(document).ready(function () {
                $("#nav-placeholder").load("nav.jsp");
            });
        </script>

        <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/form-layout-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 17:46:42 GMT -->



        <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/form-layout-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 17:46:42 GMT -->



        <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/form-layout-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 17:46:42 GMT -->



        <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/form-layout-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 17:46:42 GMT -->



        <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/form-layout-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 17:46:42 GMT -->

    </body>
    <%
        }
        //  There's no active session
        else {
            response.sendRedirect("index.jsp");
        }
    %>

    <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/form-layout-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 17:46:42 GMT -->

</html>